<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservedPickup extends Model
{
    protected $fillable = [
        'store_id',
        'date',
        'time',
        'user_id',
    ];
}
