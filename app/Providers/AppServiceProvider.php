<?php

namespace App\Providers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $brands = [
            'https://shopping.buyforlessok.com' => 1,
            'http://shopping-bfl.test:8000' => 1,
            'https://shopping.uptowngroceryco.com' => 2,
            'http://shopping.test:8000' => 2,
            'http://shopping.test' => 2,
            'https://shopping.smartsaverok.com' => 3,
            'http://shopping-ss.test:8000' => 3,
            'https://supermercado.buyforlessok.com' => 4,
            'http://shopping-sm.test:8000' => 4,
            'https://staging.uptowngroceryco.com' => 2,
            'http://shopping.ngrok.io' => 2,
            'https://shopping.ngrok.io' => 2,
        ];

        $domain = URL::to('');

        if (! session()->has('brand')) {
            try
            {
                $client = new Client();

                $response = $client->get('https://sara.buyforlessok.com/api/brand/'.$brands[$domain]);

                $brand = $response->getBody();

                $brand = json_decode($brand);

                session()->put('brand', $brand);
            } catch (\Exception $e)
            {
                return $e->getMessage();
            }
        } else {
            $brand = session()->get('brand');
        }

        view()->share('brand', $brand);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);
        $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);
    }
}
