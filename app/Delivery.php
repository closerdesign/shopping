<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Delivery extends Model
{
    protected $fillable = [
        'order_id',
        'first_name_delivery',
        'last_name_delivery',
        'delivery_address',
        'phone_delivery',
        'zip_code_delivery',
        'delivery_instructions',
        'city_delivery',
        'state_delivery',
        'delivery_time',
        'time_slot_id',
        'delivery_reference_id',
    ];

    public function order()
    {
        $this->hasOne(\App\Order::class);
    }
}
