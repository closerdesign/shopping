<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = [
        'note',
    ];

    /**
     *  Order Relationship.
     */
    public function order()
    {
        return $this->belongsTo(\App\Order::class);
    }
}
