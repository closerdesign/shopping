<?php

namespace App\Console\Commands;

use App\Mail\OrderSurvey;
use App\Order;
use Carbon\Carbon;
use Dompdf\Exception;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendOrdersSurvey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:surveys';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::where('pickup_date', date('Y-m-d', strtotime(Carbon::yesterday())))
            ->where('status', 'DELIVERED')
            ->get();

        foreach ($orders as $order) {
            try {
                $client = new Client();

                $response = $client->get('https://sara.buyforlessok.com/api/store-by-code/'.$order->store);

                $store = \GuzzleHttp\json_decode($response->getBody());
            } catch (Exception $e) {
                Log::alert($e->getMessage());
            }

            $order->store = $store;

            Mail::to($order->email)
                ->send(new OrderSurvey($order));
        }
    }
}
