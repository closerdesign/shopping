<?php

namespace App\Console\Commands;

use App\Mail\DailyReport;
use App\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class DailySummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reports:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It sends a daily movements report with our Online Shopping movement.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stores = [1230, 3501, 9515, 1006, 2701];

        $date = date('Y-m-d', strtotime('-1 days'));

        $cash = [
            3501 => 'cash3501@buyforlessok.com',
            1230 => 'cash1230@uptowngroceryco.com',
            9515 => 'cash9515@uptowngroceryco.com',
        ];

        foreach ($stores as $store_code) {
            $transactions = Order::where('store', $store_code)
                ->where('pickup_date', $date)
                ->whereIn('status', ['COMPLETED', 'DELIVERED'])
                ->get();

            $report = View::make('reports.daily-report', ['transactions' => $transactions]);

            if (count($transactions) > 1) {
                Mail::to([
                    'tmiller@buyforlessok.com',
                    'jroby@buyforlessok.com',
                    $cash[$store_code],
                    'juanc@closerdesign.co'
                ])
                    ->send(new DailyReport($transactions, $store_code, $date, $report));
            }
        }
    }
}
