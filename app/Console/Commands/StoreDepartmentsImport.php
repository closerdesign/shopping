<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class StoreDepartmentsImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'departments:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('store_departments')->truncate();

        try{
            $client = new Client();
            $response = $client->get('https://sara.buyforlessok.com/api/departments');
            $departments = $response->getBody();
            $departments = json_decode($departments);

            foreach ($departments as $department)
            {
                DB::table('store_departments')->insert([
                    'store_department' => $department->code,
                    'description' => $department->name
                ]);
            }
        } catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }
}
