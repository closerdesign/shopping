<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class fixUserNameAndLastName extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:names';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();

        $count = 0;

        foreach ($users as $user) {
            $names = explode(' ', $user->name);

            User::where('id', $user->id)->update([
                'name' => current($names),
                'last_name' => end($names),
            ]);

            $count++;
        }

        echo $count;
    }
}
