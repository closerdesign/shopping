<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class ThanxPayPal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thanx:paypal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Last Week PayPal Transactions to the Thanx Team';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = date('Y-m-d');

        $yesterday = date('Y-m-d', strtotime('-1 day', strtotime($today)));

        $date_to = $yesterday;

        $date_from = date('Y-m-d', strtotime('-6 day', strtotime($yesterday)));

        $transactions = Order::whereBetween('pickup_date', [$date_from, $date_to])
            ->whereIn('status', ['COMPLETED', 'DELIVERED'])
            ->whereIn('store', [1230, 9515])
            ->get();

        $report = View::make('reports.thanx-report', ['transactions' => $transactions]);

        Mail::to('keaton.renta@thanx.com')
            ->cc('digital@buyforlessok.com')
            ->send(new \App\Mail\ThanxPayPal($transactions, $date_from, $date_to, $report));

        dd('Task finished successfully. '.count($transactions).' records found.');
    }
}
