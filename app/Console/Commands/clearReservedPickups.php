<?php

namespace App\Console\Commands;

use App\ReservedPickup;
use Illuminate\Console\Command;

class clearReservedPickups extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickups:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $limit = date('Y-m-d h:i:s', strtotime('-3 hours'));

        ReservedPickup::where('created_at', '<', $limit)->delete();
    }
}
