<?php

namespace App\Console\Commands;

use App\Order;
use App\User;
use Illuminate\Console\Command;

class fixLoyaltyMemberId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:loyalty';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::where('loyalty_number', '!=', '')
            ->groupBy('email')
            ->select('email', 'loyalty_number')
            ->get();

        $count = 0;

        foreach ($orders as $order) {
            $users = User::where('email', $order->email)->get();

            foreach ($users as $user) {
                if ($order->loyalty_number > 0) {
                    $user->loyalty_member_id = $order->loyalty_number;
                    $user->save();
                }
            }

            $count++;
        }

        echo $count;
    }
}
