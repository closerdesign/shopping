<?php

namespace App\Console\Commands;

use App\Order;
use App\User;
use Illuminate\Console\Command;

class fixOrdersUserId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:userid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::where('user_id', null)
            ->take(5000)
            ->get();

        $count = 0;

        foreach ($orders as $order) {
            $user = User::where('email', $order->email)->first();

            if ($user) {
                $order->user_id = $user->id;
                $order->save();

                $count++;
            }
        }

        echo $count;
    }
}
