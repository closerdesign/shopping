<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;

class fixOrdersNames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:ordersnames';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::orderBy('created_at', 'desc')->take(1000)->get();

        $count = 0;

        foreach ($orders as $order) {
            $names = explode(' ', $order->name);

            $order->update([
                'name' => current($names).' '.end($names),
            ]);

            $count++;
        }

        echo $count;
    }
}
