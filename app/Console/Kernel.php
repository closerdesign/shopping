<?php

namespace App\Console;

use App\Console\Commands\clearReservedPickups;
use App\Console\Commands\DailySummary;
use App\Console\Commands\DBBackup;
use App\Console\Commands\fixLoyaltyMemberId;
use App\Console\Commands\fixOrdersNames;
use App\Console\Commands\fixOrdersUserId;
use App\Console\Commands\fixUserNameAndLastName;
use App\Console\Commands\SendOrdersSurvey;
use App\Console\Commands\ThanxPayPal;
use App\Http\Controllers\OrdersController;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ThanxPayPal::class,
        DailySummary::class,
        fixUserNameAndLastName::class,
        fixOrdersNames::class,
        fixOrdersUserId::class,
        fixLoyaltyMemberId::class,
        SendOrdersSurvey::class,
        clearReservedPickups::class,
        DBBackup::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('pickups:clear')->dailyAt('1:00');
        $schedule->command('reports:daily')->dailyAt('6:00');
        $schedule->command('send:surveys')->dailyAt('16:30');
        $schedule->command('db:backup')->dailyAt('2:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
