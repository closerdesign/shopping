<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class isApproved
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Auth::user()->isApproved()) {
            Session::flash('message', [
                'type'    => 'danger',
                'message' => 'Your account hasn\'t been approved yet.',
            ]);

            Auth::logout();

            return redirect('');
        }

        return $next($request);
    }
}
