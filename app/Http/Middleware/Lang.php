<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = 'en';

        if ((URL::to('') == 'https://supermercado.buyforlessok.com') && (! Session::has('lang'))) {
            $locale = 'es';

            App::setLocale('es');

            Session::put('store', 2701);
        }

        if (URL::to('') == 'https://shopping.buyforlessok.com') {
            Session::put('store', 3501);
        }

        if (URL::to('') == 'https://shopping.smartsaverok.com') {
            Session::put('store', 1006);
        }

        if (session()->has('lang')) {
            $locale = session()->get('lang');
        }

        App::setLocale($locale);

        return $next($request);
    }
}
