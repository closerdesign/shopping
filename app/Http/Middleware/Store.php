<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class Store
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! session()->has('store')) {
            if ((auth()->user()->billing_address == null) || (auth()->user()->preferred_store == null) || (auth()->user()->zip_code == null) || (auth()->user()->phone == null) || (auth()->user()->preferred_store == null) || (auth()->user()->city == null)) {
                return redirect()->action('AccountsController@personal_info')->with('message', ['type' => 'danger', 'message' => \Illuminate\Support\Facades\Lang::get('account.complete-personal-info')]);
            }

            if (! session()->has('store') && (auth()->user()->preferred_store !== null)) {
                session()->put('store', auth()->user()->preferred_store);
            }

            if( !session()->has('store_data') )
            {
                $client = new Client();
                $response = $client->get('https://sara.buyforlessok.com/api/store-by-code/' . session()->get('store'));
                $store_data = json_decode($response->getBody());
                session()->put('store_data', $store_data);
            }
        }

        return $next($request);
    }
}
