<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class personalInfo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Auth::user()) {
            return redirect()
                ->route('home')
                ->with([
                    'message' => [
                        'type' => 'success',
                        'message' => \Illuminate\Support\Facades\Lang::get('general.please-enter-your-credentials'),
                    ],
                ]);
        }

        if (
            auth()->user()->name == null ||
            auth()->user()->email == null ||
            auth()->user()->billing_address == null ||
            auth()->user()->city == null ||
            auth()->user()->zip_code == null ||
            auth()->user()->phone == null ||
            auth()->user()->preferred_store == null
        ) {
            return redirect()
                ->action('AccountsController@personal_info')
                ->with(['message' => [
                    'type'    => 'danger',
                    'message' => \Illuminate\Support\Facades\Lang::get('account.complete-personal-info'),
                ]]);
        }

        return $next($request);
    }
}
