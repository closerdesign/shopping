<?php

namespace App\Http\Traits;

use Dompdf\Exception;
use GuzzleHttp\Client;

trait StoreTrait
{
    public function store_data($id)
    {
        try {
            $client = new Client();

            $response = $client->get('https://sara.buyforlessok.com/api/store-by-code/'.$id);

            $store = $response->getBody();

            $store = json_decode($store);

            return $store;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
