<?php

namespace App\Http\Controllers;

use App\Mail\SmsReply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class NexmoController extends Controller
{
    public function reply(Request $request)
    {
        $this->validate($request, [
            'msisdn' => 'required',
            'text'   => 'required',
        ]);

        Mail::to('digital@buyforlessok.com')
            ->send(new SmsReply($request));

        return response('success', 200);
    }
}
