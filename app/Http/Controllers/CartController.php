<?php

namespace App\Http\Controllers;

use App\Delivery;
use App\Item;
use App\Mail\OrderCompleted;
use App\Mail\OrderConfirmation;
use App\Order;
use App\ReservedPickup;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Meta;
use Nexmo\Laravel\Facade\Nexmo;

class CartController extends Controller
{
    private $stripe_data;
    public $categories;

    public function __construct()
    {
        $this->middleware(
            'store', ['except' => ['payment_do', 'cancel', 'delivery_zip']]
        );

        try {
            $client = new Client();

            $url = env('BFL_API_URL').'/shopping-categories-list/'.Session::get('store');

            $response = $client->get($url);

            $this->categories = json_decode($response->getBody());

            Session::put('categories', $this->categories);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id'    => 'required',
            'name'  => 'required',
            'qty'   => 'required',
            'price' => 'required',
        ]);

        if (! is_numeric($request->price)) {
            Session::flash('message', [
                'type'    => 'danger',
                'message' => 'The price entered was not a valid number.',
            ]);

            return back();
        }

        Cart::add($request->id, $request->name, $request->qty, $request->price, [
            'allow_substitutions' => 1,
            'dept_code' => $request->dept_code,
            'image' => $request->image
        ]);

        $this->cartstore();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Product added successfully',
            'cart'    => 1,
        ]);

        if ($request->back == 1) {
            return back();
        }
    }

    public function add_item(Request $request)
    {
        $item = Cart::add($request->id, $request->name, $request->qty, $request->price, [
            'allow_substitutions' => 1,
            'dept_code' => $request->dept_code,
            'image' => $request->image,
            'scale_flag' => (int) request()->scale_flag
            ]
        );

        return response($item, 200);
    }

    /**
     * Adds search string and item to search table for analytics and improving search.
     * @param int $id
     * @return void
     */
    public function add_search_term($id)
    {
        if (Session::get('search_string') != '') {
            $url = env('BFL_API_URL').'/shopping-product-search-add/'.$this->store.'/'.urlencode(trim(Session::get('search_string'))).'/'.$id;

            $client = new \GuzzleHttp\Client();
            $response = $client->get($url);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Item removed successfully from your cart',
        ]);

        return back();
    }

    /**
     * Display the Shopping Cart.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkout()
    {
        if( auth()->user()->suspended )
        {
            return back()
                ->with([
                    'message' => [
                        'type' => 'danger',
                        'message' => 'Your account has been suspended.'
                    ]
                ]);
        }

        $categories = $this->categories;

        try {
            $client = new Client();
            $response = $client->get('https://sara.buyforlessok.com/api/store-by-code/'.session()->get('store'));
            $store = json_decode($response->getBody());
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return view('pages.checkout', compact('categories', 'store'));
    }

    /**
     *  Index.
     */
    public function index()
    {
        return $this->checkout();
    }

    /**
     * returns to the checkout page.
     */
    public function show()
    {
        return $this->checkout();
    }

    public function add_delivery_tip(Request $request)
    {
        if ($request->delivery_tip != '' && $request->delivery_tip >= 0 && is_numeric($request->delivery_tip)) {
            Session::put('delivery_tip', $request->delivery_tip);
        }

        return redirect('checkout');
    }

    /**
     *  Adding Item Comments.
     */
    public function comments($id, $comment)
    {
        $cart = Cart::get($id);
        if (empty($cart)) {
            return 1;
        } else {
            Cart::update($id, [
                'options' => [
                    'comments' => $comment,
                    'allow_substitutions' => $cart->options->allow_substitutions,
                    'dept_code' => $cart->options->dept_code,
                    'image' => $cart->options->image,
                ],
            ]);
        }

        return 1;
    }

    /**
     *  Update Qty.
     */
    public function update_qty($id, $qty)
    {
        try {
            $item = Cart::update($id, [
                'qty' => $qty,
            ]);

            $this->cartstore();

            return response($item, 200);
        } catch (\Exception $e) {
        }
    }

    /**
     *  Payment Process.
     */
    public function payment(Request $request)
    {
        if( !session()->has('pickup_date') )
        {
            return back()->with(['message' => ['type' => 'danger', 'message' => 'Please select Pickup Date & Time']]);
        }

        $order = new Order([
            'pickup_date' => session()->get('pickup_date'),
            'pickup_time' => session()->get('pickup_time'),
            'name' => auth()->user()->name . ' ' . auth()->user()->last_name,
            'phone' => auth()->user()->phone,
            'email' => auth()->user()->email,
            'comments' => request()->comments,
            'payment_method' => 'Stripe',
            'total_order' => Cart::total(),
            'status' => 'PLACED',
            'store' => session()->get('store'),
            'is_complex' => session()->get('is_complex'),
            'complex' => session()->get('complex'),
            'substitution_preference' => '',
            'billing_address' => auth()->user()->billing_address,
            'zip_code' => auth()->user()->zip_code,
            'paypal_token' => session()->get('payment_intent')
        ]);

        $order->user_id = auth()->user()->id;
        $order->loyalty_number = auth()->user()->loyalty_member_id;

        $order->save();

        foreach (Cart::content() as $product) {
            $item = new Item([
                'upc'                 => $product->id,
                'name'                => $product->name,
                'price'               => $product->price,
                'qty'                 => $product->qty,
                'comments'            => $product->options->comments,
                'allow_substitutions' => $product->options->allow_substitutions,
                'dept_code'           => $product->options->dept_code,
                'image'               => $product->options->image,
                'scale_flag'          => $product->options->scale_flag,
                'custom_item'         => $product->options->custom_item,
            ]);

            $item->order_id = $order->id;

            $item->save();
        }

        $bcc = User::where('store', Session::get('store'))->pluck('email')->toArray();

        try
        {
            Mail::to($order->email)
                ->bcc($bcc)
                ->send(new OrderConfirmation($order));
        }

        catch (\Exception $e)
        {
            Log::alert('Email Confirmation for Order ' . $order->id . ' has not been sent.');
        }

        ReservedPickup::destroy(session()->get('reserved_pickup'));

        Session::forget('pickup_time');
        Session::forget('pickup_date');
        session()->forget('reserved_pickup');

        Cart::destroy();

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Your Order Has Been Placed Successfully!',
        ]);

        return response($order, 200);
    }

    /**
     *  Paypal Do Payment.
     */
    public function payment_do(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        try {

//            $status = PosController::order_status($order->id);
//
//            $collected = 0;
//
//            if (! $status[0]->orderEntryTenders) {
//                return back()
//                    ->with(['message' => [
//                        'type' => 'danger',
//                        'message' => 'Order has not been totaled at the POS level.',
//                    ]]);
//            }
//
//            foreach ($status[0]->orderEntryTenders as $tender) {
//                $collected += $tender->posTenderAmount;
//            }
//
//            if ($collected == 0) {
//                return back()
//                    ->with(['message' => [
//                        'type' => 'danger',
//                        'message' => 'Tender amount cannot be zero.',
//                    ]]);
//            }

            $collected = request()->final_amount;

            try {

                $payment = StripeController::capture_payment($order, $collected);

                $order->completed_by = auth()->user()->email;
                $order->completion_date = date('Y-m-d');
                $order->final_amount = $collected;
                $order->shopper_end_time = date('Y-m-d H:i:s');
                $order->status = 'COMPLETED';
                $order->save();

            } catch (\Exception $e) {
                Session::flash('message', [
                    'type' => 'danger',
                    'message' => 'Oops! Stripe: '.$e->getMessage(),
                ]);

                return back()
                    ->with([
                        'message', [
                            'type' => 'danger',
                            'message' => 'Oops! Stripe: '.$e->getMessage(),
                        ]
                    ]);
            }


            Mail::to($order->email)
                ->send(new OrderCompleted($order));

            $text = "[DO NOT REPLY] Hello " . $order->first_name . "! Your Online Shopping Order has been completed and is ready to be picked up. Give us a call when you arrive and we will bring your groceries to your car!";

            try
            {
                $basic  = new \Nexmo\Client\Credentials\Basic('1751edb0', '6282f76ad02b5989');
                $client = new \Nexmo\Client($basic);

                $message = $client->message()->send([
                    'to' => '1' . $order->phone,
                    'from' => '14058967925',
                    'text' => $text
                ]);

            } catch (\Exception $e)
            {
                Log::error('Issues when sending SMS message for order ' . $order->id . ': ' . $e->getMessage());
            }

            return redirect()
                ->action('AdminController@index')
                ->with('message', [
                    'type' => 'success',
                    'message' => 'Great! The Payment Went Through And Now The Order Has Been Completed',
                ]);

        } catch (\Exception $e) {
            return redirect()
                ->action('AdminController@index')
                ->with('message', [
                    'type' => 'danger',
                    'message' => $e->getMessage(),
                ]);
        }
    }

    /**
     *  Substitutions.
     */
    public function substitutions($row, $value)
    {
        $cart = Cart::get($row);
        $comment = $cart->options->comments;

        if ($cart->options->comments == null) {
            $comment = '';
        }

        Cart::update($row, [
            'options' => [
                'comments' => $comment,
                'allow_substitutions' => $value,
                'dept_code' => $cart->options->dept_code,
                'image' => $cart->options->image,
            ],
        ]);

        return $value;
    }

    /**
     *  Disallow Substitutions.
     */
    public function disallow_substitutions()
    {
        $items = Cart::content();

        foreach ($items as $item) {
            $comment = $item->options->comments;

            if ($item->options->comments == null) {
                $comment = '';
            }

            Cart::update($item->rowId, [
                'options' => [
                    'comments' => $comment,
                    'allow_substitutions' => 0,
                    'dept_code' => $item->options->dept_code,
                    'image' => $item->options->image,
                ],
            ]);
        }

        return 'Substitutions disallowed.';
    }

    /**
     *  Refresh.
     */
    public function refresh()
    {
        return view('partials._cart');
    }

    public function refresh_top()
    {
        return view('partials._nav');
    }

    public function refresh_checkout()
    {
        return view('partials._edit');
    }

    public function refresh_sidebar()
    {
        return view('partials._very-top');
    }

    /**
     *  Delete.
     */
    public function delete($id)
    {
        try {
            Cart::remove($id);

            $this->cartstore();

            return 'Ok';
        } catch (\Exception $e) {
        }
    }

    /**
     *  Saves The Cart Into The Database.
     */
    public function cartstore()
    {
        if (! empty(Auth::user())) {
            $id = Auth::user()->id;

            DB::select(DB::raw("DELETE FROM shoppingcart WHERE identifier = $id"));

            Cart::store(Auth::user()->id);
        }
    }

    /***
     *  Restores The Order When You Log In
     *
     *
     */

    public function restore()
    {
        if (! empty(Auth::user())) {
            Cart::restore(Auth::user()->id);

            $items = Cart::content();

            foreach ($items as $item) {
                $price = $item->price;
                $id = $item->id;

                //  $product = file_get_contents('http://bfl-marketing.test:8000/api/shopping-products/product/' . Session::get('store') ."/". $id);

                $product = file_get_contents(env('BFL_API_URL').'/shopping-products/product/'.Session::get('store').'/'.$id);

                $payload = json_decode($product, true);

                //  print_r($payload);

                if (! $payload) {
                    $missing_items[] = $item->name;

                    Cart::remove($item->rowId);
                } else {
                    if ($payload[0]['promo_tag'] != '') {
                        $item->price = $payload[0]['sale_price'];
                    } else {
                        if ($payload[0]['regular_price'] != '') {
                            $item->price = $payload[0]['regular_price'];
                        }
                    }

                    if ($item->price == null) {
                        $price = 0.00;
                    } else {
                        $price = $item->price;
                    }

                    Cart::update($item->rowId, [
                        'price' => $price,
                    ]);
                }
            }
            // Add the items back to the shopping cart table so if they log right back out it's still saved
            Cart::store(Auth::user()->id);

            if (! empty($missing_items)) {
                return $missing_items;
            }
        }
    }

    /**
     *  Removing The Cart From The Database.
     */
    public function clear()
    {
        Cart::destroy();

        if (isset(Auth::user()->id)) {
            $id = Auth::user()->id;

            DB::select(DB::raw("DELETE FROM shoppingcart WHERE identifier = $id"));
        }

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Your cart has been cleared.',
        ]);

        return back();
    }

    public function delivery_zip($zip_code)
    {
        $zip_code = trim($zip_code);

        $store = Session::get('store');

        if ($store == 1230) {
            $array = [73003, 73007, 73012, 73013, 73025, 73034];
        } elseif ($store == 9515) {
            $array = [73008, 73099, 73102, 73103, 73104, 73105, 73106, 73107, 73108, 73109, 73111, 73112, 73114, 73116, 73118, 73119, 73120, 73122, 73127, 73128, 73131, 73132, 73134, 73142, 73151, 73162];
        } elseif ($store == 1124) {
            $array = [73111];
        } else {
            $zip = 'false';
        }

        if (in_array($zip_code, $array)) {
            $zip = 'true'; //$this->delivery_times();
        } else {
            $zip = 'false';
        }

        if ($zip == 'true') {
            Session::put('delivery_zip_code', $zip_code);
        }

        return $zip;
    }

    /**
     * Returns an array of two elements [zip code valid, alcohol].
     */
    public function delivery_eligible($zip)
    {
        $zip = $this->delivery_zip($zip);
        $alcohol = $this->alcohol_check();

        if ($alcohol) {
            $a = 'false';
        } else {
            $a = 'true';
        }

        return [$zip, $a];
    }

    public function set_delivery_time(Request $request)
    {
        Session::put('delivery_date', $request->delivery_date);
        Session::put('delivery_time', $request->delivery_time);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Your delivery time has been reserved.',
        ]);

        return back();
    }

    public function set_delivery(Request $request, $order_id)
    {
        $delivery = new Delivery($request->all());

        $del_date = date('Y-m-d', strtotime(Session::get('delivery_date'))).' '.date('H:i', strtotime(Session::get('delivery_time')));

        $delivery->order_id = $order_id;
        $delivery->first_name_delivery = $request->first_name_delivery;
        $delivery->last_name_delivery = $request->last_name_delivery;
        $delivery->delivery_address = $request->delivery_address;
        $delivery->phone_delivery = $request->phone_delivery;
        $delivery->delivery_instructions = $request->delivery_instructions;
        $delivery->city_delivery = $request->delivery_city;
        $delivery->state_delivery = 'OK';
        $delivery->zip_code_delivery = $request->zip_code_delivery;
        $delivery->delivery_time = $del_date;
        $delivery->save();
    }

    public function post_delivery_to_shipt(Request $request, $order_id)
    {
        if (Session::get('store') == '1230') {
            $pickup_city = 'Edmond';
            $pickup_address = '1230 W. Covell Rd';
            $pickup_state = 'OK';
            $pickup_postal_code = '73003';
        }
        if (Session::get('store') == '9515') {
            $pickup_city = 'OKC';
            $pickup_address = '9515 N. May Ave.';
            $pickup_state = 'OK';
            $pickup_postal_code = '73120';
        }
        if (Session::get('store') == '1124') {
            $pickup_city = 'OKC';
            $pickup_address = '1124 NE 36th Street';
            $pickup_state = 'OK';
            $pickup_postal_code = '73111';
        }

        $order = Order::findOrFail($order_id);

        $del_date = date('Y-m-d', strtotime(Session::get('delivery_date'))).' '.date('H:i', strtotime(Session::get('delivery_time')));

        $shipt_date = date(DATE_ISO8601, strtotime($del_date));

        $items = count($order->items);

        $data = '
        
        {

          "pickup": {
        
            "location": {
        
              "address": "'.$pickup_address.'",
        
              "address_2": null,
        
              "city": "'.$pickup_city.'",
        
              "state": "'.$pickup_state.'",
        
              "postal_code": " '.$pickup_postal_code.' "
        
            },
        
            "special_instructions": ""
        
          },
        
          "dropoff": {
        
            "location": {
        
              "address": "'.$request->delivery_address.'",
        
              "address_2": "",
        
              "city": "'.$request->city_delivery.'",
        
              "state": "OK",
        
              "postal_code": "'.$request->zip_code_delivery.'"
        
            },
        
            "contact": {
        
              "first_name": "'.$request->first_name_delivery.'",
        
              "last_name": "'.$request->last_name_delivery.'",
        
              "email": "'.$request->email.'",
        
              "phone_number": "'.$request->phone_delivery.'"},
        
            "delivery_instructions": "'.$request->delivery_instructions.'",
        
            "contains_alcohol": false
        
          },
        
          "external_reference_id": '.$order->id.',
        
          "deliver_by": "'.$shipt_date.'",
        
          "requested_items_summary": "",
        
         "tip": '.Session::get('delivery_tip').',
         
        "order_amount": '.$order->total_order.',
        
        "total_items": '.$items.'
        
        }
        
        ';

        try {
            $client = new Client();

            $response = $client->post(env('SHIPT_URL').'/deliveries', [

                'header' => [
                    'Content-Type: application/json',
                    'Content-Length: '.strlen($data),
                    'Authorization: OAuth '.env('SHIPT_ACCESS_TOKEN'),
                ],

                'body' => $data,

            ]);

            $json = json_decode($response->getBody());

            try {
                Delivery::where('order_id', $order_id)->update(['delivery_reference_id' => $json->id]);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function cancel_delivery($shipt_order_id)
    {
        $data = ['_method'=> 'DELETE'];

        $access_token = env('SHIPT_ACCESS_TOKEN');

        $data = json_encode($data);

        $curl = curl_init(env('SHIPT_URL').'/deliveries/'.$shipt_order_id);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: '.strlen($data),
            'Authorization: OAuth '.$access_token,
        ]);

        $response = curl_exec($curl);
        curl_close($curl);
    }

    /**
     * @return bool
     * if order has alcohol return true
     */
    public function alcohol_check()
    {
        $items = Cart::content();

        $alcohol = [055, 005, 85, 86, 87];

        foreach ($items as $item) {
            if (in_array($item->options->dept_code, $alcohol)) {
                return true;
            }
        }

        return false;
    }

    /**
     *  Content
     */

    public function content()
    {
        return Cart::content();
    }
}
