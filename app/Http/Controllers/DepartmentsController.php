<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DepartmentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('store');

        $this->middleware(function ($request, $next) {
            $this->store = Session::get('store');

            $this->categories = json_decode(file_get_contents(env('BFL_API_URL').'/shopping-categories-list/'.Session::get('store')));

            return $next($request);
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = $this->categories;

        $items = file_get_contents('https://marketing.buyforlessok.com/shopping-products/products/'.$id.'/'.Session::get('store'));

        $items = json_decode($items);

        return view('pages.department', compact('items', 'categories'));
    }

    /**
     *  Departments List.
     */
    public static function departments_list()
    {
        $departments = file_get_contents(env('BFL_API_URL').'/shopping-products/departments/'.Session::get('store'));

        $departments = json_decode($departments);

        return $departments;
    }
}
