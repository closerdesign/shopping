<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $brands = [
            'https://shopping.buyforlessok.com' => 1,
            'http://shopping-bfl.test:8000' => 1,
            'https://shopping.uptowngroceryco.com' => 2,
            'http://shopping.test:8000' => 2,
            'https://shopping.smartsaverok.com' => 3,
            'http://shopping-ss.test:8000' => 3,
            'https://supermercado.buyforlessok.com' => 4,
            'http://shopping-sm.test:8000' => 4,
        ];

        $domain = URL::to('');

        try {
            if (session()->has('brand')) {
                $brand = session()->get('brand');
            } else {
                $client = new Client();

                $response = $client->get('https://sara.buyforlessok.com/api/brand/'.$brands[$domain]);

                $brand = $response->getBody();

                $brand = json_decode($brand);

                session()->put('brand', $brand);
            }

            view()->share('brand', $brand);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
