<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    /**
     *  Language.
     */
    public function language($id)
    {
        Session::put('lang', $id);

        return back();
    }
}
