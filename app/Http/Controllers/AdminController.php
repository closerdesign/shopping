<?php

namespace App\Http\Controllers;

use App\Item;
use App\Mail\ImprovementRequest;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'approved']);
    }

    /**
     *  Home.
     */
    public function index()
    {
        $orders = Order::orderByRaw("FIELD(status, 'PLACED', 'In Progress', 'POS', 'COMPLETED', 'DELIVERED', 'CANCELLED', 'PAYMENT')")
            ->orderBy('pickup_date')
            ->orderBy('pickup_time');

        if (auth()->user()->store != null) {
            $orders = $orders
                ->where('store', auth()->user()->store);
        }

        if (! auth()->user()->isAdmin()) {

            $orders = $orders->where(function($q){
                $q->where('status', 'PLACED')
                    ->orWhere('status', 'In Progress')
                    ->orWhere('status', 'POS')
                    ->orWhere('status', 'COMPLETED');
            });
        }

        if (isset($_GET['keyword'])) {
            $orders = $orders->where(function ($q) {
                $q->where('name', 'LIKE', '%'.$_GET['keyword'].'%')
                    ->orWhere('email', 'LIKE', '%'.$_GET['keyword'].'%')
                    ->orWhere('phone', 'LIKE', '%'.$_GET['keyword'].'%')
                    ->orWhere('total_order', 'LIKE', '%'.$_GET['keyword'].'%')
                    ->orWhere('final_amount', 'LIKE', '%'.$_GET['keyword'].'%')
                    ->orWhere('paypal_token', 'LIKE', '%'.$_GET['keyword'].'%')
                    ->orWhere('id', 'LIKE', '%'.$_GET['keyword'].'%');
            });
        }

        if (isset($_GET['store'])) {
            $orders = $orders->where('store', $_GET['store']);
        }

        if (isset($_GET['pickup-date'])) {
            $orders = $orders->where('pickup_date', $_GET['pickup-date']);
        }

        if (isset($_GET['status'])) {
            $orders = $orders->where('status', $_GET['status']);
        }

        $orders = $orders->paginate(24);

        return view('admin.orders', compact('orders'));
    }

    /**
     *  Improvement Request.
     */
    public function improvement_request()
    {
        return view('admin.improvement-request');
    }

    public function improvement_request_form(Request $request)
    {
        $this->validate($request, [
            'subject'    => 'required',
            'details'   => 'required',
        ]);

        Mail::to('digital@buyforlessok.com')->send(new ImprovementRequest($request));

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Thank you! Your request has been submitted.',
        ]);

        return back();
    }

    /**
     *  Excel Report.
     */
    public function excel_full_report()
    {
        $orders = Order::all();

        if (Auth::user()->store != null) {
            $orders = Order::where('store', Auth::user()->store)->get();
        }

        Excel::create('Orders', function ($excel) use ($orders) {
            $excel->sheet('ORDERS', function ($sheet) use ($orders) {
                $sheet->loadView('admin.excel-full-report')->with('orders', $orders);
            });
        })->download('xlsx');
    }

    /**
     *  Top Sellers Report.
     */
    public function top_sellers_report()
    {
        $items = DB::table('items')
            ->join('orders', 'items.order_id', '=', 'orders.id')
            ->where('orders.status', 'COMPLETED')
            ->groupBy('upc')
            ->select(DB::raw('items.upc,items.name,SUM(items.qty) as count'))
            ->orderBy('count', 'desc')
            ->paginate(50);

        return view('admin.top-sellers-report', compact('items'));
    }

    /**
     *  Top Sellers Report Excel.
     */
    public function top_sellers_report_excel()
    {
        $items = DB::table('items')
            ->join('orders', 'items.order_id', '=', 'orders.id')
            ->where('orders.status', 'COMPLETED')
            ->groupBy('upc')
            ->select(DB::raw('items.upc,items.name,SUM(items.qty) as count'))
            ->orderBy('count', 'desc')
            ->get();

        Excel::create('TopSellersReport', function ($excel) use ($items) {
            $excel->sheet('TOP SELLERS', function ($sheet) use ($items) {
                $sheet->loadView('admin.top-sellers-report-excel')->with('items', $items);
            });
        })->download('xlsx');
    }

    /**
     *  Thanx Transactions.
     */
    public function thanx()
    {
        $yesterday = date('Y-m-d', strtotime('-1 days'));

        $transactions = Order::where(function ($q) use ($yesterday) {
            $q->where('payment_method', 'Paypal')
                ->where('status', 'COMPLETED')
                ->where('pickup_date', $yesterday);
        })->get();

        Excel::create('PaypalTransactions-'.$yesterday, function ($excel) use ($transactions) {
            $excel->sheet('TRANSACTIONS', function ($sheet) use ($transactions) {
                $sheet->loadView('admin.thanx')->with('transactions', $transactions);
            });
        })->download('xlsx');
    }

    public function top_online_shoppers()
    {
        $numbers = DB::select(DB::raw('select count(email) as num_email, email, max(pickup_date) as last_date, SUM(final_amount) as sum_orders from orders group by email order by num_email DESC '));

        return view('admin.customers', compact('numbers'));
    }

    public function customer_orders(Request $request)
    {
        $orders = Order::where('email', $request->email)->orderBy('pickup_date', 'DESC')->get();

        return view('admin.customer-orders', compact('orders'));
    }

    /**
     *  Login using ID.
     */
    public function login_using_id($id)
    {
        $user = User::find($id);

        Auth::loginUsingId($user->id);

        return redirect('');
    }

    /**
     *  Duplicate an order
     */

    public function duplicate(Order $order)
    {
        $new = $order->replicate();
        $new->status = 'In Progress';
        $new->save();

        $order->status = 'CANCELLED';
        $order->save();

        foreach ($order->items as $item)
        {
            $new_item = $item->replicate();
            $new_item->order_id = $new->id;
            $new_item->save();
        }

        return redirect()
            ->action('AdminController@index')
            ->with([
                'message' => [
                    'type' => 'success',
                    'message' => 'Order has been replicated. New ID is ' . $new->id
                ]
            ]);
    }
}
