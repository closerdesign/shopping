<?php

namespace App\Http\Controllers;

use App\PaymentMethod;
use App\User;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Void_;
use Stripe\Stripe;

class PaymentMethodController extends Controller
{
    public function __construct()
    {
        $this->stripe = Stripe::class;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create($customer)
    {
        //

        $user = User::where('email', $customer->email)->first();

        $payment_method = new PaymentMethod();
        $payment_method->user_id = $user->id;
        $payment_method->card_id = $customer->default_source;
        $payment_method->last_four = $customer->sources->last4;
        $payment_method->brand = $customer->brand;

        $payment_method->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string customer stripe $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cards = $this->stripe->cards()->all($id);

        return $cards;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->stripe->cards()->delete($request->stripe_id, $request->card_id);
    }
}
