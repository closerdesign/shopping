<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class PagesController extends Controller
{
    public $brand;

    public function __construct()
    {
        $this->middleware(['auth', 'store']);
    }

    /**
     *  Homepage.
     */
    public function homepage()
    {
        return view('pages.homepage');
    }
}
