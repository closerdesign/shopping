<?php

namespace App\Http\Controllers;

use App\Order;
use Dompdf\Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

class PosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *  Add Order Attended.
     */
    public static function add_order($id, $type)
    {
        // Types: 1. Unattended. 2. Attended.

        try {
            $order = Order::find($id);

            $client = new Client();

            $products = [];

            $i = 1;

            foreach ($order->items->where('picked_qty', '>', 0)->where('custom_item', 0) as $item) {

                $qty = (int) $item->picked_qty;
                $weight = (float) 0;

                if ($item->scale_flag === 1) {
                    $qty = (int) 0;
                    $weight = (float) $item->picked_qty;
                }

                $product = [
                    'upc' => $item->upc,
                    'originalUpc' => $item->upc,
                    'department' => $item->dept_code,
                    'price' => $item->price,
                    'quantity' => $qty,
                    'weight' => $weight,
                    'amount' => $item->picked_qty * $item->price,
                    'itemSequence' => $i,
                ];

                array_push($products, $product);

                $i++;
            }

            if(count($products) === 0)
            {
                return back()
                    ->with(['message' => [
                        'type' => 'danger',
                        'message' => 'The order has no items to send.'
                    ]]);
            }

            $products = json_encode($products);

            $order_totals = [];
            $order_totals['total'] = $order->total;
            $order_totals['subTotal'] = $order->subtotal;
            $order_totals['taxAmount'] = $order->tax;
            $order_totals['amountTendered'] = $order->total;

            $order_entry_taxes = [];
            $order_entry_taxes['taxCode'] = '1';
            $order_entry_taxes['taxableAmount'] = $order->subtotal;
            $order_entry_taxes['taxAmount'] = $order->tax;

            $order_entry_tenders = [];
            $order_entry_tenders['posTenderType'] = $order->payment_method;
            $order_entry_tenders['posTenderAmount'] = $order->total;
            $order_entry_tenders['accountNumber'] = '';

            $body = '
                {
                    "status": "1",
                    "type": "'.$type.'",
                    "processorOrderNumber": "'.$order->id.'",
                    "posLoyaltyNumber": "'.$order->loyalty_number.'",
                    "orderEntryPOSs": '.$products.',
                    "memberOffers": ""
                }
            ';

            Storage::put('trucommerce/online-shopping/'.$order->id.'.json', $body);

            $response = $client->post(env('TRUCOMMERCE_SERVER_URL').'/v2/orderService', [
                'headers' => ['Content-type' => 'application/json'],
                'auth' => [
                    env($order->store.'_TRUCOMMERCE_USER'),
                    env($order->store.'_TRUCOMMERCE_PASSWORD'),
                ],
                'body' => $body,
            ]);

            $pos = json_decode($response->getBody());

            $order->pos_id = $pos->id;

            $order->pos_received = $pos->posReceived;

            $order->status = 'POS';

            $order->save();

            return redirect()
                ->action('AdminController@index')
                ->with([
                    'message' => [
                        'type' => 'success',
                        'message' => 'The order has been sent to the POS successfully! ID:' . $order->id,
                    ],
                ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     *  Check Order Status.
     */
    public static function order_status($id)
    {
        $order = Order::findOrFail($id);

        try {
            $url = env('TRUCOMMERCE_SERVER_URL').'/v2/orderService?processorOrderNumber='.$order->id;

            $client = new Client();

            $response = $client->get($url, [
                'headers' => ['Content-type' => 'application/json'],
                'auth' => [
                    env($order->store.'_TRUCOMMERCE_USER'),
                    env($order->store.'_TRUCOMMERCE_PASSWORD'),
                ],
            ]);

            $status = $response->getBody();

            $status = json_decode($status);

            return $status;
        } catch (\Exception $e) {
            return back()
                ->with([
                    'message' => [
                        'type' => 'danger',
                        'message' => $e->getMessage(),
                    ],
                ]);
        }
    }

    /**
     *  Receipt
     */

    public function receipt(Order $order)
    {
        try
        {
            $url = env('TRUCOMMERCE_SERVER_URL').'/v2/orderService?processorOrderNumber='.$order->id;

            $client = new Client();

            $response = $client->get($url, [
                'headers' => ['Content-type' => 'application/json'],
                'auth' => [
                    env($order->store.'_TRUCOMMERCE_USER'),
                    env($order->store.'_TRUCOMMERCE_PASSWORD'),
                ],
            ]);

            $status = $response->getBody();

            $status = json_decode($status);

            return response($status, 200);
        }

        catch (\Exception $e)
        {
            return back()
                ->with([
                    'message' => [
                        'type' => 'danger',
                        'message' => $e->getMessage(),
                    ],
                ]);
        }
    }
}
