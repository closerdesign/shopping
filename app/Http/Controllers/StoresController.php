<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Meta;

class StoresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *  Store Selector.
     */
    public function store_selector()
    {
        Meta::set('title', 'Skip the Line. Order Online. - Please select a store to proceed.');
        Meta::set('description', 'Order your groceries for pick-up and we will have them waiting for you when you arrive!');

        return view('pages.store-selector');
    }

    /**
     *  Store Select.
     */
    public function store_select(Request $request)
    {
        $this->validate($request, [
            'store' => 'required|numeric',
        ]);

        Session::put('store', $request->store);

        return redirect(URL::to('/'));
    }
}
