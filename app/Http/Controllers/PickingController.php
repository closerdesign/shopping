<?php

namespace App\Http\Controllers;

use App\Item;
use App\Order;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PickingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *  Pull the product data out of the Online Database.
     */
    public function pull_data_from_online_db($store, $upc)
    {
        $client = new Client();

        $url = 'https://sara.buyforlessok.com/api/shopping-product-favorites/'.$store.'/'.$upc;

        $response = $client->get($url);

        $product = $response->getBody();

        $product = json_decode($product);

        if (empty($product->data)) {
            return null;
        }

        $product->image = urlencode($product->image);
        $product->source = 'OS';

        return $product;
    }

    /**
     *  Pull the product data from the POS database.
     */
    public function pull_data_from_pos($store, $upc)
    {
        $product = null;

        $client = new Client();
        $url = 'https://sara.buyforlessok.com/api/retalix-price/price-check/'.$store.'/'.$upc;
        $response = $client->get($url);
        $product = json_decode($response->getBody());

        if ($product !== null) {
            $product->upc = substr($product->upc_ean, 0, 13);
            $product->name = $product->product_description;
            $product->price = $product->ip_unit_price;
            $product->source = 'POS';
            $product->image = null;
        }

        return $product;
    }

    /**
     *  Pull Data.
     */
    public function pull_data($store, $upc)
    {
        $upc = format_upc($upc);

        $product = $this->pull_data_from_online_db($store, $upc);

        if ($product !== null) {
            return $product;
        }

        $product = $this->pull_data_from_pos($store, $upc);

        if ($product !== null) {
            return $product;
        }

        return null;
    }

    /**
     *  Product Insert.
     */
    public function insert($product, $order, $qty = 1)
    {
        try{
            $item = Item::where('order_id', $order)->where('upc', $product->upc);

            if ($item->count() > 0) {
                $item = $item->first();
                $item->qty = $item->qty + 1;
                $item->picked_qty = $item->picked_qty + 1;
                $item->save();

                return $item;
            }

            $item = new Item([
                'upc' => $product->upc,
                'name' => $product->name,
                'price' => $product->price,
                'qty' => $qty,
                'picked_qty' => 1,
                'dept_code' => $product->dept_code,
                'image' => $product->image,
                'picked' => 1,
            ]);

            $item->order_id = $order;
            $item->save();

            return $item;
        }

        catch (\Exception $e)
        {
            return back()
                ->with([
                    'message' => [
                        'type' => 'danger',
                        'message' => $e->getMessage()
                    ]
                ]);
        }
    }

    /**
     *  Picked Items.
     */
    public function picking_items(Order $order)
    {
        if ($order->status == 'CANCELLED' || $order->status == 'COMPLETED' || $order->status == 'DELIVERED' || $order->status == 'PAYMENT') {
            return redirect()
                ->action('OrdersController@show', $order->id)
                ->with([
                    'message' => [
                        'type' => 'danger',
                        'message' => 'Order cannot be picked in the current status',
                    ],
                ]);
        }

        if ($order->status !== 'In Progress') {
            $order->status = 'In Progress';
            $order->shopper = auth()->user()->email;
            $order->shopper_start_time = date('Y-m-d h:i:s');
            $order->save();
        }

        try
        {
            $client = new Client();
            $response = $client->get('https://sara.buyforlessok.com/api/departments');
            $departments = $response->getBody();
            $departments = json_decode($departments, true);

        } catch (\Exception $e)
        {
            return $e->getMessage();
        }

        return view('picking.picking-items', compact('order', 'departments'));
    }

    /**
     *  Picked Item.
     */
    public function picked(Request $request, Order $order)
    {
        try
        {
            // Formatting the UPC so its format matches the Online Database
            $upc = format_upc(request()->upc);

            // First, we will validate if the item is in the list
            $item = Item::where([
                'upc' => $upc,
                'order_id' => $order->id
            ]);

            // This is what we're going to do if the item is in the list
            if( $item->count() == 1 )
            {
                $item = $item->first();
                $item->picked_qty = $item->picked_qty + 1;
                $item->save();

                return back()->with(['message' => ['type' => 'success', 'message' => 'Item has been picked. ' . $item->picked_qty . ' of ' . $item->qty]]);
            }

            // If not, we will look up for this product information into Retalix database
            $client = new Client();
            $url = 'https://sara.buyforlessok.com/api/retalix-price/price-check/' . auth()->user()->store . '/' . request()->upc;
            $response = $client->get($url);
            $product = $response->getBody();
            $product = json_decode($product, true);

            if( !$product['upc'] || ($product['upc'] == null) || ($product['upc'] == "") )
            {
                // If the product data is not available
                return back()
                    ->with(['message' => [
                        'type' => 'danger',
                        'message' => 'Product not available in the online database. Please set apart and add at the POS level.'
                    ]]);
            }else{
                // Then, we will add the product to the list.
                $added = new Item([
                    'upc' => $product['upc'],
                    'name' => $product['product_description'],
                    'price' => round($product['online_price'], 2),
                    'dept_code' => $product['dept_code'],
                    'qty' => $product['qty'],
                    'picked_qty' => $product['qty'],
                ]);
                $added->order_id = $order->id;
                $added->save();

                return back()->with(['message' => ['type' => 'success', 'message' => 'The product is not part of the original list, but it has been added to the order.']]);
            }
        }

        catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }

    /**
     *  Is out of stock.
     */
    public function is_out_of_stock($id)
    {
        $item = Item::find($id);
        $item->out_of_stock = true;
        $item->save();

        return back()->with([
            'message' => [
                'type'    => 'success',
                'message' => 'Out of stock has been reported successfully',
            ],
        ]);
    }

    /**
     *  Substitute
     *  - Displays the form to confirm a substitution.
     */
    public function substitute(Order $order, $upc)
    {
        try {
            $product = $this->pull_data($order->store, $upc);

            if ($product !== null) {
                return view('picking.substitute', compact('order', 'product'));
            }

            return back()
                ->with([
                    'message' => [
                        'type'    => 'danger',
                        'message' => 'Product data not found. Please avoid including the item to the basket. (substitute)',
                    ],
                ]);
        } catch (\Exception $e) {
            return back()->with([
                'message' => [
                    'type' => 'danger',
                    'message' => $e->getMessage() . ': substitute',
                ],
            ]);
        }
    }

    /**
     *  Add Substitute.
     */
    public function add_substitute(Order $order, Item $old, $upc)
    {
        try {
            $product = $this->pull_data($order->store, $upc);

            if ($product !== null) {
                $item = $this->insert($product, $order->id, $old->qty);

                if (! $item) {
                    return back()
                        ->with([
                            'message' => [
                                'type' => 'danger',
                                'message' => 'There was a problem adding the product to the order.',
                            ],
                        ]);
                }

                $old->out_of_stock = 1;
                $old->substitute = $product->upc;
                $old->save();

                return redirect()
                    ->route('picking-order', $order->id)
                    ->with([
                        'message' => [
                            'type' => 'success',
                            'message' => 'The substitution has been processed. 1 of '.$old->qty,
                        ],
                    ]);
            } else {
                return back()->with([
                    'message' => [
                        'type' => 'Danger',
                        'message' => 'No product data. This product cannot be used to substitute any item.',
                    ],
                ]);
            }
        } catch (\Exception $e) {
            return back()->with([
                'message' => [
                    'type' => 'danger',
                    'message' => $e->getMessage(),
                ],
            ]);
        }
    }

    /**
     *  Add Item.
     */
    public function add_item(Order $order, $upc)
    {
        return "Ok";
    }

    /**
     *  Price Check.
     */
    public function price_check()
    {
        try {
            if (auth()->user()->store == null) {
                return back()
                    ->with([
                        'message' => [
                            'type' => 'danger',
                            'message' => 'Only users assigned to a store can use the Price Check functionality',
                        ],
                    ]);
            }

            $product = null;

            if (isset($_GET['upc'])) {
                $client = new Client();

                $response = $client->get('https://sara.buyforlessok.com/api/retalix-price/price-check/'.auth()->user()->store.'/'.$_GET['upc']);

                $product = json_decode($response->getBody());
            }

            return view('picking.price-check', compact('product'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
