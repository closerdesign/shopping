<?php

namespace App\Http\Controllers;

use App\ReservedPickup;
use Illuminate\Http\Request;

class ReservedPickupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReservedPickup  $reservedPickup
     * @return \Illuminate\Http\Response
     */
    public function show(ReservedPickup $reservedPickup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReservedPickup  $reservedPickup
     * @return \Illuminate\Http\Response
     */
    public function edit(ReservedPickup $reservedPickup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReservedPickup  $reservedPickup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReservedPickup $reservedPickup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReservedPickup  $reservedPickup
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReservedPickup $reservedPickup)
    {
        //
    }
}
