<?php

namespace App\Http\Controllers;

use App\Mail\ContactForm;
use App\Mail\FeedbackForm;
use App\Mail\NotesForm;
use App\Note;
use App\Order;
use App\ReservedPickup;
use App\User;
use Dompdf\Exception;
use Faker\Provider\DateTime;
use Gloudemans\Shoppingcart\Facades\Cart;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Meta;

class ShoppingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('store', ['except' => ['notes', 'notes_form']]);

        $this->middleware(function ($request, \Closure $next) {
            $this->store = Session::get('store');

            if (empty(Session::get('categories'))) {
                try {
                    $client = new Client();

                    $url = env('BFL_API_URL').'/shopping-categories-list/'.Session::get('store');

                    $response = $client->get($url);

                    $this->categories = json_decode($response->getBody());

                    Session::put('categories', $this->categories);
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            } else {
                $this->categories = Session::get('categories');
            }

            $brand = [
                '3501' => 'Buy For Less',
                '7957' => 'Buy For Less',
                '2701' => 'SuperMercado',
                '3701' => 'SuperMercado',
                '3713' => 'SuperMercado',
                '4150' => 'SuperMercado',
                '1230' => 'Uptown Grocery Co.',
                '9515' => 'Uptown Grocery Co.',
                '1124' => 'Uptown Grocery Co.',
                '1006' => 'Smart Saver',
                '1205' => 'Smart Saver',
                '1201' => 'Smart Saver',
                '2001' => 'Smart Saver',
                '4424' => 'Smart Saver',
            ];

            $response = $next($request);

            if (isset($response)) {
                return $response;
            }
        }, ['except' => ['notes', 'notes_form']]);
    }

    /**
     *  Home View.
     */
    public function index()
    {
//        return Cart::content();

        $categories = $this->categories();

        $recent = [];

        $upcs = DB::table('items')
            ->join('orders', 'items.order_id', '=', 'orders.id')
            ->where('user_id', auth()->user()->id)
            ->distinct('upc')
            ->select('upc', 'qty')
            ->orderBy('qty', 'desc')
            ->take(24)
            ->get()
            ->pluck('upc')
            ->implode(',');

        if (count(explode(',', $upcs)) > 1) {
            try {
                $client = new Client();

                $response = $client->get('https://sara.buyforlessok.com/api/shopping-product-favorites/'.session()->get('store').'/'.$upcs);

                $recent = json_decode($response->getBody());
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }

        $favorites = [];

        if (auth()->user()->favorites->count() > 1) {
            try {
                $client = new Client();

                $upcs = $favorites = auth()->user()->favorites()->pluck('upc')->implode(',');

                $response = $client->get('https://sara.buyforlessok.com/api/shopping-product-favorites/'.session()->get('store').'/'.$upcs);

                $favorites = json_decode($response->getBody());
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }

        return view('pages.home', compact(['categories', 'favorites', 'recent']));
    }

    /**
     * categories.
     */
    public function categories()
    {
        if (empty(Session::get('categories'))) {
            try {
                $client = new Client();

                $url = env('BFL_API_URL').'/shopping-categories-list/'.Session::get('store');

                $response = $client->get($url);

                $categories = json_decode($response->getBody());

                Session::put('categories', $categories);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            $categories = Session::get('categories');
        }

        return $categories;
    }

    /**
     *  Category View.
     */
    public function category($id)
    {
        $categories = $this->categories();

        $id = explode('-', $id)[0];

        $url = env('BFL_API_URL').'/shopping-category-view/'.$this->store.'/'.$id;

        if (isset($_GET['page'])) {
            $url .= '?page='.$_GET['page'];
        }

        try {
            $client = new Client();

            $products = $client->get($url);

            $products = json_decode($products->getBody());
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return view('pages.category', compact('products', 'categories'));
    }

    /**
     *  Product View.
     */
    public function product($id)
    {
        $categories = $this->categories();

        $id = explode('-', $id)[0];

        $product = file_get_contents(env('BFL_API_URL').'/shopping-product-view/'.$this->store.'/'.$id);

        $product = json_decode($product)[0];

        return view('pages.product', compact('categories', 'product'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * AWS Search
     */
    public function search_aws()
    {
        $categories = $this->categories;

        $products = [];

        $page_size = 24;

        if (isset($_GET['page'])) {
            $current_page = $_GET['page'];
        } else {
            $current_page = 1;
        }

        $start = (($current_page * $page_size) - $page_size);

        if (isset($_GET['keyword'])) {
            $keyword = $_GET['keyword'];

            $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
            $keyword = preg_replace($regexEmoticons, '', $keyword);
            $keyword = str_replace(['\'', '"', ',', ';', '<', '>', '*', '-', '&'], '', $keyword);

            Session::forget('search_string');

            Session::put('search_string', $keyword);

            $url[1230] = 'https://search-store-1230-yuidmmfr7a53ecxlwa6j2moafm.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.$keyword.'&size='.$page_size.'&start='.$start;
            $url[1124] = 'https://search-store-1230-yuidmmfr7a53ecxlwa6j2moafm.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.$keyword.'&size='.$page_size.'&start='.$start;
            $url[9515] = 'https://search-store-9515-5lzbu7736t3sjqnuvi4a5m5xje.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.$keyword.'&size='.$page_size.'&start='.$start;
            $url[3501] = 'https://search-store-3501-ngeuq4bh7ihwwfwusextktqw2u.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.$keyword.'&size='.$page_size.'&start='.$start;
            $url[1006] = 'https://search-store-1006-abkghiu3osli562jxhevzs52w4.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.$keyword.'&size='.$page_size.'&start='.$start;
            $url[2701] = 'https://search-store-2701-nsp7r26btfm5bmj7pdunqksdhq.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.$keyword.'&size='.$page_size.'&start='.$start;
            $url['test'] = 'https://search-aws-test-5frdmdrifkfk4fv6bffljyjd5m.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.$keyword.'&size='.$page_size.'&start='.$start;

            $client = new \GuzzleHttp\Client();
            $response = $client->get($url[Session::get('store')]);
            $products = $response->getBody()->getContents();

            $products = json_decode($products);

            $vars['found'] = $products->hits->found;

            $vars['pages'] = ceil(($vars['found'] / $page_size));

            if ($current_page < $vars['pages']) {
                $vars['next'] = $current_page + 1;
            } else {
                $vars['next'] = 0;
            }

            if ($current_page > 1) {
                $vars['previous'] = $current_page - 1;
            }

            $vars['showing_start'] = ((($current_page * $page_size) - ($page_size)) + 1);

            $vars['showing_end'] = (($page_size * $current_page));

            if ($vars['showing_end'] > $vars['found']) {
                $vars['showing_end'] = $vars['found'];
            }

            $vars['current_page'] = $current_page;

            $products = $products->hits->hit;

            return view('pages.search', compact('products', 'categories', 'vars'));
        }
    }

    /**
     *  Search View.
     */
    public function search()
    {
        $categories = $this->categories();

        $products = [];

        if (isset($_GET['keyword'])) {
            $keyword = $_GET['keyword'];

            $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
            $keyword = preg_replace($regexEmoticons, '', $keyword);
            $keyword = str_replace(['\'', '"', ',', ';', '<', '>', '*', '-'], '', $keyword);

            if (trim($keyword) !== '' && strstr($keyword, '/') == false) {
                Session::forget('search_string');

                Session::put('search_string', $keyword);

                $url = env('BFL_API_URL').'/shopping-product-search/'.$this->store.'/'.urlencode(trim($keyword));

                //  $url = 'http://bfl-marketing.test:8000/api/shopping-product-search/'.$this->store.'/'.urlencode(trim($keyword));

                if (isset($_GET['page'])) {
                    $url .= '?page='.$_GET['page'];
                }

                try {
                    $client = new \GuzzleHttp\Client();
                    $response = $client->get($url);
                    $answer = $response->getBody()->getContents();
                    $products = json_decode($answer);
                } catch (\Exception $e) {
                    $products = '';
                }

                return view('pages.search', compact('products', 'categories'));
            } else {
                Session::flash('message', [
                    'type'    => 'danger',
                    'message' => 'Your search contained characters that are not allowed or the field was left blank. Please try again.',
                ]);

                return back();
            }
        }
    }

    public function autocomplete(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');

            if (strlen($query) > 2) {
                $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
                $query = preg_replace($regexEmoticons, '', $query);
                $query = str_replace(['\'', '"', ',', ';', '<', '>', '*', '-'], '', $query);

                $url = env('BFL_API_URL').'/shopping-product-search/'.$this->store.'/'.urlencode(trim($query));
                //$products = file_get_contents($url);

                try {
                    $client = new \GuzzleHttp\Client();
                    $response = $client->get($url);
                    $answer = $response->getBody()->getContents();
                    $products = json_decode($answer);
                } catch (\Exception $e) {
                    $products = '';
                }

                $output = "<div class='search_container' style='border: #3c763d solid 1px;'>";
                if (! empty($products)) {
                    foreach ($products as $p) {
                        //die(print_r($p->data));
                        foreach ($p->data as $d) {
                            $name = '';
                            if (substr($d->brand_name, 0, 13) == '1 LB is about') {
                                $name = '<a href="/shopping/product/'.$d->upc.'"><b>'.$d->name.'</b></a> <br>'.$d->size.' | ';
                            } else {
                                $name = '<a href="/shopping/product/'.$d->upc.'"><b>'.$d->brand_name.' '.$d->name.'</b></a> <br>'.$d->size.' | ';
                            }

                            $output .= '<div class="product_container" style="background-color: #ffffff; padding:10px; border-bottom: 1px solid #dddddd">';
                            $output .= $name;
                            $output .= ($d->sale_price != '' ? '<small><span class="label label-danger">On Sale!</span></small> $'.$d->sale_price : '$'.$d->regular_price);
                            $output .= '</div>';
                        }
                    }

                    $output .= '</div>';
                    echo $output;

                    if (strlen($query) > 1) {
                        echo '';
                    }
                }
            }
        }
    }

    /**
     *  Contact View.
     */
    public function contact()
    {
        $categories = $this->categories();

        return view('pages.contact', compact('categories'));
    }

    /**
     *  Contact Form.
     */
    public function contact_form(Request $request)
    {
        try{
            $client = new Client();
            $response = $client->get('https://www.google.com/recaptcha/api/siteverify?response=' . request()->recaptcha . '&secret=' . env('RECAPTCHA_V3_SECRET'));
            $response = $response->getBody();
            $response = json_decode($response);

            if( $response->success == true && $response->score > 0.5 )
            {
                $this->validate($request, [
                    'name'    => 'required',
                    'email'   => 'required|email',
                    'phone'   => 'required',
                    'contact_preference' => 'required',
                    'message' => 'required',
                ]);

                Mail::to('help@buyforlessok.com')
                    ->bcc('juanc@closerdesign.co')
                    ->send(new ContactForm($request));

                return back()->with(['message' => ['type' => 'success', 'message' => 'Thank you! Your message has been received. We\'ll be in contact soon.']]);
            }
        }catch (\Exception $e)
        {
            return back()
                ->with(['message' => [
                    'type' => 'danger',
                    'message' => $e->getMessage()
                ]]);
        }
    }

    /**
     *  Feedback View.
     */
    public function feedback()
    {
        $categories = $this->categories();

        return view('pages.feedback', compact('categories'));
    }

    /**
     *  Feedback Form.
     */
    public function feedback_form(Request $request)
    {
        $this->validate($request, [
            'name'    => 'required',
            'email'   => 'required|email',
            'experience' => 'required',
            'message' => 'required',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);

        Mail::to('digital@buyforlessok.com')->send(new FeedbackForm($request));

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Thank you! Your feedback has been received.',
        ]);

        return back();
    }

    /**
     *  Reset Location.
     */
    public function reset_location()
    {
        Cart::destroy();

        Session::forget('pickup_time');
        Session::forget('pickup_date');
        Session::forget('is_complex');
        Session::forget('store');
        Session::forget('categories');
        Session::forget('delivery_date');
        Session::forget('delivery_time');

        return back();
    }

    public function delivery_times($date, $location)
    {
        $hours = [
            '10:00',
            '10:30',
            '11:00',
            '11:30',
            '12:00',
            '12:30',
            '13:00',
            '13:30',
            '14:00',
            '14:30',
            '15:00',
            '15:30',
            '16:00',
            '16:30',
            '17:00',
            '17:30',
            '18:00',
            '18:30',
            '19:00',
            '19:30'
        ];

        $slots = '<option value="">Select...</option>';

        foreach ($hours as $hour) {
            $count = count(Order::where('pickup_date', $date)->where('pickup_time', $hour)->where('store', $location)->get());

            if ($count < 2) {
                if ($date == date('Y-m-d')) {
                    $min = strtotime('+4 hours');

                    if (strtotime($hour) > $min) {
                        $slots .= "<option value='".date('H:i', strtotime($hour.'+1 hour'))."'>".date('g:i A', strtotime($hour)).' - '.date('g:i A', strtotime($hour.' + 1 hour ')).'</option>';
                    }
                }

                if ($date > date('Y-m-d')) {
                    $slots .= "\"<option value='".date('H:i', strtotime($hour.'+1 hour'))."'>".date('g:i A', strtotime($hour)).' - '.date('g:i A', strtotime($hour.' + 1 hour ')).'</option>';
                }
            }
        }

        return $slots;
    }

    /**
     *  Pickup Times.
     */
    public function pickup_times($date, $location)
    {
        $location = session()->get('store');

        $options = ['10:00','11:00','12:00','13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00'];

        $min = date('Y-m-d H:i:s', strtotime('+4 hours'));

        $hours = [];

        foreach ($options as $option) {
            $order_date = date('Y-m-d H:i:s', strtotime($date.' '.$option));

            if ($order_date > $min) {
                $count = 0;

                $reserved = ReservedPickup::where('date', $date)
                    ->where('store_id', $location)
                    ->where('time', $option)
                    ->count();

                $count += $reserved;

                $orders = Order::where('store', $location)
                    ->where('pickup_date', $date)
                    ->where('pickup_time', $option)
                    ->where('status', '!=', 'PAYMENT')
                    ->count();

                $count += $orders;

                if ($count <= 3) {
                    array_push($hours, $option);
                }
            }
        }

        if ( count($hours) < 1 ) {
            $slots = '<option value="">Sorry, no slots available. Please select another date.</option>';
        } else {
            $slots = "<option value=''>Select...</option>";

            foreach ($hours as $hour) {
                $slots .= "<option value='$hour'>".date('g:i A', strtotime($hour))."</option>";
            }
        }

        if( (date('Ymd', strtotime($date)) == '20201225') )
        {
            $slots = '<option value="">Sorry, no slots available. Please select another date.</option>';
        }

        // Uncomment to block offering all pickup spots
//        $slots = '<option value="">Sorry, no slots available. Please select another date.</option>';

        return $slots;
    }

    /**
     *  Pickup Settings.
     */
    public function pickup_settings(Request $request)
    {
        if ($request->is_complex == 1) {
            $this->validate($request, [
                'complex'    => 'required',
            ]);

            $complex = $request->complex;

            $next = strtotime('next thursday');

            if (date('Y-m-d') < '2018-11-22') {
                $next = strtotime('next wednesday');
            }

            Session::put('is_complex', 1);
            Session::put('complex', $request->complex);

            if ($complex == 'J Marshall Square' || $complex == 'The Classen' || $complex == 'The Montgomery') {
                Session::put('pickup_date', date('Y-m-d', $next));
                Session::put('pickup_time', '14:00');
            } elseif ($complex = 'Park Harvey Apartments') {
                Session::put('pickup_date', date('Y-m-d', $next));
                Session::put('pickup_time', '15:00');
            }
        } else {
            if ($request->pickup_time !== 'Select...') {
                $this->validate($request, [
                    'pickup_time' => 'required',
                    'pickup_date' => 'required',
                ]);

                $reserved_pickup = ReservedPickup::updateOrCreate([
                    'date' => request()->pickup_date,
                    'time' => request()->pickup_time,
                ], [
                    'store_id' => (int) session()->get('store'),
                    'user_id' => (int) auth()->user()->id,
                ]);

                session()->put('reserved_pickup', $reserved_pickup->id);

                Session::put('pickup_time', $request->pickup_time);
                Session::put('pickup_date', $request->pickup_date);

                Session::flash('message', [
                    'type' => 'success',
                    'message' => 'Your time slot has been reserved.',
                ]);
            } else {
                Session::flash('message', [
                    'type'    => 'danger',
                    'message' => 'You did not specify a pickup time. Please try again.',
                ]);
            }
        }

        return back();
    }

    /**
     *  Pickup Reset.
     */
    public function pickup_reset()
    {
        Session::forget('pickup_time');
        Session::forget('pickup_date');
        Session::forget('is_complex');

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Your time slot has been reset. Please select a new one before completing your order.',
        ]);

        return back();
    }

    public function delivery_reset()
    {
        Session::forget('delivery_date');
        Session::forget('delivery_time');

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Your delivery time has been reset. Please select a new one before completing your order.',
        ]);

        return redirect('checkout');
    }

    /**
     *  Terms.
     */
    public function terms()
    {
        $categories = $this->categories();

        return view('pages.terms', compact('categories'));
    }

    /**
     *  Add Notes to Order.
     */
    public function notes($id)
    {
        $order = Order::findOrFail($id);

        $categories = json_decode(file_get_contents(env('BFL_API_URL').'/shopping-categories-list/'.$order->store));

        return view('pages.notes', compact('order', 'categories'));
    }

    /**
     *  Note Form.
     */
    public function notes_form(Request $request, $id)
    {
        try{
            $client = new Client();
            $response = $client->get('https://www.google.com/recaptcha/api/siteverify?secret=' . env('RECAPTCHA_V3_SECRET') . '&response=' . request()->recaptcha);
            $response = $response->getBody();
            $response = json_decode($response);

            if( $response->success == true && $response->score > 0.5 )
            {
                $this->validate($request, [
                    'message' => 'required',
                ]);

                $order = Order::findOrFail($id);

                if ($order->status !== 'PLACED') {
                    Session::flash('message', [
                        'type'    => 'danger',
                        'message' => 'Sorry. You can not add any message to this order.',
                    ]);

                    return back();
                }

                $note = new Note();
                $note->note = $request->message;
                $note->order_id = $order->id;
                $note->save();

                Mail::to(User::where('store', $order->store)->select('email')->get()->map(function ($email) {
                    return $email->email;
                }))
                    ->bcc($order->email)
                    ->send(new NotesForm($request, $order));

                return back()->with(['message' => ['type' => 'success', 'message' => 'Thank you! Your note has been added to your order.']]);
            }

        }catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }

    /**
     *  Thank you / Confirmation page.
     */
    public function thankyou($id)
    {
        $order = Order::findOrFail($id);

        $categories = $this->categories();

        return view('pages.thankyou', compact('categories', 'order'));
    }
}
