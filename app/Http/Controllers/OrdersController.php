<?php

namespace App\Http\Controllers;

use App\Delivery;
use App\Item;
use App\Mail\CallNotification;
use App\Mail\CancelledOrder;
use App\Mail\GuestCheck;
use App\Mail\OrderScoreComments;
use App\Mail\OverTotalNotification;
use App\Mail\Reminder;
use App\Mail\ReportForm;
use App\Mail\TextNotification;
use App\Order;
use App\OutOfStock;
use App\User;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Nexmo\Laravel\Facade\Nexmo;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['month_report', 'day_report', 'score_page', 'score_comments']]);

        $this->departments = [
            ''    => 'UNDEFINED',
            '00'  => 'CUSTOM ITEMS',
            '001' => 'GROCERY',
            '002' => 'MEAT',
            '003' => 'PRODUCE',
            '004' => 'BAKERY',
            '005' => 'BEER',
            '006' => 'HBC',
            '007' => 'DAIRY',
            '009' => 'FROZEN',
            '011' => 'NON-FOOD',
            '013' => 'GENERAL',
            '015' => 'BAKERY(PRE-PACKAGED)',
            '018' => 'FLORAL',
            '019' => 'DELI',
            '020' => 'DELI',
            '021' => 'WEDGERY',
            '022' => 'PHARMACY',
            '023' => 'DELI',
            '024' => 'SEAFOOD',
            '026' => 'FROZEN',
            '029' => 'PRODUCE',
            '031' => 'HBC',
            '038' => 'BAKERY',
            '039' => 'SUSHI',
            '041' => 'GM FOOD STAMPABLE',
            '201' => 'SERVICE',
            '055' => 'WINE',
            '070' => 'GIFTS AND SPECIALTIES',
            '999' => 'CUSTOM ITEMS'
        ];

        $this->brand = [
            '3501' => 'Buy For Less',
            '7957' => 'Buy For Less',
            '2701' => 'SuperMercado',
            '3701' => 'SuperMercado',
            '3713' => 'SuperMercado',
            '4150' => 'SuperMercado',
            '1230' => 'Uptown Grocery Co.',
            '9515' => 'Uptown Grocery Co.',
            '1124' => 'Uptown Grocery Co.',
            '1006' => 'Smart Saver',
            '1205' => 'Smart Saver',
            '1201' => 'Smart Saver',
            '2001' => 'Smart Saver',
            '4424' => 'Smart Saver',
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $departments = $this->departments;

        $order = Order::findOrFail($id);

        return view('orders.show', compact('order', 'departments'));
    }

    /**
     *  Summary PDF.
     */
    public function summary($images, $id)
    {
        $order = Order::findOrFail($id);

        $orders = Order::all();

        $count = 0;

        foreach ($orders as $key) {
            if ($key->email == $order->email) {
                $count = $count + 1;
            }
        }

        $departments = $this->departments;

        $pdf = App::make('dompdf.wrapper');

        $pdf->loadView('orders.summary', compact('order', 'departments', 'images', 'count'));

        return $pdf->stream($id.'-order-summary.pdf');
    }

    /**
     *  Packaging PDF.
     */
    public function packaging($id)
    {
        $order = Order::findOrFail($id);

        $departments = $this->departments;

        $pdf = App::make('dompdf.wrapper');

        $pdf->loadView('orders.packaging', compact('order', 'departments'));

        return $pdf->stream($id.'-order-packaging.pdf');
    }

    /**
     *  Report Item We Don't Carry.
     */
    public function report($id, $store)
    {
        Mail::to('mellis@buyforlessok.com')->send(new ReportForm($id, $store));

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Thank you. Item has been reported as an item we don\'t carry.',
        ]);

        return back();
    }

    /**
     *  Report Out of Stock Item.
     */
    public function out_of_stock($id, $store, $userid)
    {
        $item = Item::findOrFail($id);

        $outofstock = new OutOfStock();
        $outofstock->upc = $item->upc;
        $outofstock->name = $item->name;
        $outofstock->store = $store;
        $outofstock->user_id = $userid;
        $outofstock->save();

        $director = 'mgr'.$store.'@buyforlessok.com';

        Mail::send('email.report-oos', ['item' => $item, 'director' => $director], function ($m) use ($item, $director) {
            $m->from('media@buyforlessok.com', 'BFL Media');

            $m->to($director)
                ->subject('[Online Shopping] Item reported as out of stock');
        });

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Thank you. Item has been reported as out of stock.',
        ]);

        return back();
    }

    /**
     *  I Called You Notification.
     */
    public function call_notification($id)
    {
        $order = Order::findOrFail($id);

        Mail::to($order->email)->send(new CallNotification($order));

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Thank you. A call notification has been emailed to the guest.',
        ]);

        return back();
    }

    /**
     *  I Texted You Notification.
     */
    public function text_notification($id)
    {
        $order = Order::findOrFail($id);

        Mail::to($order->email)->send(new TextNotification($order));

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Thank you. A text notification has been emailed to the guest.',
        ]);

        return back();
    }

    /**
     *  Order is over authorized total.
     */
    public function over_total($id)
    {
        $order = Order::findOrFail($id);

        Mail::to($order->email)->send(new OverTotalNotification($order));

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Thank you. A notification has been emailed to the guest letting them know their order is complete but over the authorized total.',
        ]);

        return back();
    }

    /**
     *  Did Something Go Wrong? Notification.
     */
    public function guest_check($id)
    {
        $order = Order::findOrFail($id);

        Mail::to($order->email)->send(new GuestCheck($order));

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Thank you. An email has been sent to the guest to check for any issues with the platform.',
        ]);

        return back();
    }

    /**
     * Save Signature.
     */
    public function save_signature(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        $order->signature = $request['data'];
        $order->status = 'DELIVERED';

        $order->save();

        if ($order->delivery_fee > 0) {
            $this->notify_delivery_customer($id);
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Order has been DELIVERED successfully.',
        ]);
    }

    /**
     * Mark as Picked.
     */
    public function picked($id)
    {
        $item = Item::findOrFail($id);

        if ($item->picked == 0) {
            $item->picked = 1;
        } else {
            $item->picked = 0;
        }

        $item->save();
    }

    /**
     * Picker Comments.
     */
    public function picker_comment($id, $comment)
    {
        $item = Item::findOrFail($id);

        $item->picker_comments = $comment;

        $item->save();
    }

    /**
     * Reminder.
     */
    public static function reminder()
    {
        $orders = Order::where('pickup_date', date('Y-m-d'))->get();

        foreach ($orders as $order) {
            if ($order->is_complex == '' || $order->is_complex == '0' && $order->status != 'PAYMENT') {
                Mail::to($order->email)->send(new Reminder($order));
            }
        }
    }

    public function month_report()
    {
        $order = DB::select(DB::raw("SELECT SUM(final_amount), MONTH(pickup_date), YEAR(pickup_date), store FROM orders
                                      WHERE pickup_date BETWEEN '2018-01-01' AND '2018-12-31'
                                      AND status = 'DELIVERED'
                                      GROUP BY MONTH(`pickup_date`), YEAR(pickup_date), store;"));

        return response($order, 200);
    }

    public function notify_delivery_customer($id)
    {
        $order = Order::findOrFail($id);

        if ((preg_match('/^\d{10}$/', $order->phone)) && ($order->is_complex != 1)) {
            Nexmo::message()->send([
                'to' => '1'.$order->phone,
                'from' => '14058967925',
                'text' => '['.$this->brand[$order->store].'] Hello '.explode(' ', $order->name)[0].'! Your online shopping order has been picked up and will be delivered shortly. (This is an automated message, please do not reply)',
            ]);
        }
    }

    /**
     *  Score Page.
     */
    public function score_page($order, $score)
    {
        $order = Order::find($order);
        $order->score = $score;
        $order->save();

        return view('orders.score-page', compact('order'));
    }

    /**
     *  Score Comments.
     */
    public function  score_comments(Request $request, $id)
    {
        $this->validate($request, [
            'score_comments' => 'required'
        ]);

        $order = Order::find($id);
        $order->score_comments = request()->score_comments;
        $order->save();

        Mail::to(['mmagill@buyforlessok.com','juanc@closerdesign.co'])
            ->cc($order->shopper)
            ->send(new OrderScoreComments($order));

        return back()
            ->with([
                'message' => [
                    'type'    => 'success',
                    'message' => 'Thank you for your comments!',
                ],
            ]);
    }

    /**
     *  Cancel Order.
     */
    public function cancel_order(Request $request, $id)
    {
        $order = Order::find($id);

        $this->validate($request, [
            'cancel_reason' => 'required',
        ]);

        $order->status = 'CANCELLED';
        $order->cancel_reason = request()->cancel_reason;
        $order->save();

        Mail::to($order->email)
            ->cc(['digital@buyforlessok.com', 'juanc@closerdesign.co'])
            ->bcc(User::where('store', $order->store)->select('email')->get()->map(function ($email) {
                return $email->email;
            }))
            ->send(new CancelledOrder($order, $request));

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Order Cancelled Successfully',
        ]);

        return redirect()->action('AdminController@index');
    }
}
