<?php

namespace App\Http\Controllers;

use App\CustomItem;
use App\Order;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class CustomItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Order $order)
    {
        $image = URL::to('img/custom-product.png');

        $item = Cart::add(uniqid(), request()->name, request()->qty, request()->price, ['allow_substitutions' => 1, 'dept_code' => 999, 'image' => $image, 'custom_item' => 1]);

        return response($item, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomItem  $customItem
     * @return \Illuminate\Http\Response
     */
    public function show(CustomItem $customItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomItem  $customItem
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomItem $customItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomItem  $customItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomItem $customItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomItem  $customItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomItem $customItem)
    {
        //
    }
}
