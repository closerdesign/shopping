<?php

namespace App\Http\Controllers;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class ShoppingCartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *  Content
     */

    public function content()
    {
        $cart = [];

        foreach (Cart::content() as $item)
        {
            array_push($cart, [
                'id' => $item->id,
                'qty' => $item->qty
            ]);
        }

        array_push($cart, [
            'subtotal' => Cart::subtotal()
        ]);

        return response($cart, 200);
    }

    /**
     *  Update
     */

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required_unless:custom_item,1',
            'name' => 'required',
            'qty' => 'required',
            'price' => 'required',
            'dept_code' => 'required_unless:custom_item,1',
            'image' => 'required_unless:custom_item,1',
            'scale_flag' => 'required_unless:custom_item,1'
        ]);

        if( request()->custom_item == 1 )
        {
            request()->id = request()->id ?? uniqid();
            request()->dept_code = 999;
            request()->image = URL::to('img/custom-product.png');
            request()->scale_flag = 0;
        }

        $i = Cart::content()->filter(function($row){ return $row->id == request()->id; });

        if( !isset($i->first()->rowId) )
        {
            $item = Cart::add($request->id, $request->name, $request->qty, $request->price, [
                    'allow_substitutions' => 1,
                    'dept_code' => $request->dept_code,
                    'image' => $request->image,
                    'scale_flag' => (int) request()->scale_flag,
                    'comments' => request()->comments,
                    'custom_item' => request()->custom_item ?? 0,
                ]
            );
        }

        else
        {
            $subs = 1;

            if( request()->allow_substitutions === false )
            {
                $subs = 0;
            }

            $item = Cart::update($i->first()->rowId, [
                'qty' => request()->qty,
                'options' => [
                    'allow_substitutions' => $subs,
                    'dept_code' => $request->dept_code,
                    'image' => $request->image,
                    'scale_flag' => (int) request()->scale_flag,
                    'comments' => request()->comments,
                    'custom_item' => request()->custom_item ?? 0,
                ]
            ]);
        }

        return response($item, 200);
    }

    /**
     *  Subtotal
     */

    public function subtotal()
    {
        $count = 0;

        foreach (Cart::content() as $item)
        {
            $count += $item->qty;
        }

        $subtotal['subtotal'] = Cart::subtotal();
        $subtotal['count'] = $count;

        return $subtotal;
    }
}
