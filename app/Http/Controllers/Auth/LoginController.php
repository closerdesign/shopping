<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->store != null || $user->is_admin == 1) {
            return redirect()
                ->action('AdminController@index');
        }

        return redirect('/');
    }

    public function facebook()
    {
        // Send the user's request to Facebook
        return Socialite::driver('facebook')->redirect();
    }

    public function facebook_redirect()
    {
        // Get OAuth request back from Facebook to authenticate user
        $user = Socialite::driver('facebook')->user();

        // If this user doesn't exist, add them
        // If they do, get the model
        // Either way, authenticate the user into the application and redirect afterwards
        $user = User::firstOrCreate([
            'email' => $user->email
        ], [
            'name' => $user->name,
            'password' => Hash::make(Str::random(24)),
            'preferred_store' => 1230
        ]);

        auth()->login($user, true);

        return redirect('');
    }
}
