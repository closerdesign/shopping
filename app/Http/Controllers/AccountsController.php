<?php

namespace App\Http\Controllers;

use App\Item;
use App\Order;
use Gloudemans\Shoppingcart\Facades\Cart;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Meta;

class AccountsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            if (empty(Session::get('categories'))) {
                try {
                    $client = new Client();

                    $url = env('BFL_API_URL').'/shopping-categories-list/'.Session::get('store');

                    $response = $client->get($url);

                    $this->categories = json_decode($response->getBody());

                    Session::put('categories', $this->categories);
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            } else {
                $this->categories = Session::get('categories');
            }

            $brand = [
                '3501' => 'Buy For Less',
                '7957' => 'Buy For Less',
                '2701' => 'SuperMercado',
                '3701' => 'SuperMercado',
                '3713' => 'SuperMercado',
                '4150' => 'SuperMercado',
                '1230' => 'Uptown Grocery Co.',
                '9515' => 'Uptown Grocery Co.',
                '1124' => 'Uptown Grocery Co.',
                '1006' => 'Smart Saver',
                '1205' => 'Smart Saver',
                '1201' => 'Smart Saver',
                '2001' => 'Smart Saver',
                '4424' => 'Smart Saver',
            ];

            return $next($request);
        }, ['except' => ['notes', 'notes_form', 'my_account', 'personal_info', 'update_user']]);
    }

    public function my_account()
    {
        if (! empty(Auth::user())) {
            $missing_items = $this->restore();
        }

        return view('account.my-account');
    }

    public function change_password()
    {
        $categories = $this->categories;

        return view('account.change-password', compact('categories'));
    }

    public function personal_info()
    {
        return view('account.personal-info');
    }

    public function update_pass(Request $request)
    {
        $current = $request->password;
        $new = $request->new_pass;
        $confirm = $request->confirm_pass;

        if (Hash::check($current, Auth::user()->getAuthPassword())) {
            if ($new !== $confirm) {
                Session::flash('message', [
                        'type'    => 'danger',
                        'message' => 'Your new password did not match the confirmation. Please try again.',
                    ]);

                return back();
            } else {
                $user = Auth::user();
                $user->password = Hash::make($new);
                $user->save();

                Session::flash('message', [
                        'type'    => 'success',
                        'message' => 'Your password has been updated successfully!',
                    ]);

                $categories = $this->categories;

                return redirect()->action('AccountsController@my_account');
            }
        } else {
            Session::flash('message', [
                    'type'    => 'danger',
                    'message' => 'Your current password was incorrect. Please try again.',
                ]);

            return back();
        }
    }

    public function update_user(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required',
            'email' => 'required|email',
            'billing_address' => 'required',
            'city' => 'required',
            'zip_code' => 'required|regex:/\b\d{5}\b/',
            'phone' => 'required',
            'preferred_store' => 'required',
        ]);

        $user = Auth::user();

        $user->fill($request->all());

        $user->save();

        session()->forget('store');
        session()->put('store', $user->preferred_store);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Your information has been updated.',
        ]);

        return redirect()->action('ShoppingController@index');
    }

    public function previous_orders()
    {
        $categories = $this->categories;

        $placed = Order::user()
            ->whereIn('status', [
                'PLACED',
                'In Progress',
                'COMPLETED',
                'DELIVERED',
            ])
            ->orderBy('pickup_date', 'desc')
            ->paginate(15);

        return view('account.previous-orders', compact('categories', 'placed'));
    }

    public function order_details($order)
    {
        $categories = $this->categories;

        $order = Order::user()->findOrFail($order);

        return view('account.order-details', compact('categories', 'order'));
    }

    public function reorder_items($order)
    {
        $order = Order::findOrFail($order);

        $items = $order->items;

        $upcs = [];

        foreach ($order->items as $item) {
            array_push($upcs, $item->upc);
        }

        $upcs = implode(',', $upcs);

        $prices = env('BFL_API_URL').'/shopping-products/reorder-list/'.Session::get('store').'/'.$upcs;

        try {
            $client = new Client();

            $response = $client->get($prices);

            $prices = json_decode($response->getBody());
        } catch (\Exception $e) {
            return back()
                ->with([
                    'message' => [
                        'type' => 'danger',
                        'message' => $e->getMessage(),
                    ],
                ]);
        }

        Cart::destroy();

        foreach ($prices as $price) {
            $item = Item::where('upc', $price->sku)
                ->where('order_id', $order->id)
                ->where('custom_item', 0)
                ->first();

            if ($item != null) {
                $current_price = $price->regular_price;

                if ($price->sale_price > 0) {
                    $current_price = $price->sale_price;
                }

                $subs = '0';

                if ($item->allow_substitutions == 'Yes') {
                    $subs = '1';
                }

                Cart::add($item->upc, $item->name, $item->qty, $current_price, [
                    'dept_code' => $item->dept_code,
                    'image' => $item->image,
                    'allow_substitutions' => $subs,
                    'scale_flag' => $item->scale_flag,
                ]);
            }
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Items Have Been Added To Your Cart Successfully',
        ]);

        return redirect()
            ->route('checkout')
            ->with(['message' => ['type' => 'success', 'message' => 'Your cart has been restored successfully.']]);
    }

    /**
     *  Favorites.
     */
    public function favorites()
    {
        if (count(Auth::user()->favorites) < 1) {
            Session::flash('message', [
                'type' => 'info',
                'message' => 'In order to access your Favorites you need to favorite some items first. Let\'s start now!',
            ]);

            return back();
        }

        $categories = $this->categories;

        $favorites = Auth::user()->favorites()->select('upc')->get();

        $upcs = [];

        foreach ($favorites as $favorite) {
            array_push($upcs, $favorite->upc);
        }

        $upcs = implode(',', $upcs);

        $url = env('BFL_API_URL').'/shopping-product-favorites/'.Session::get('store').'/'.$upcs;

        if (isset($_GET['page'])) {
            $url .= '?page='.$_GET['page'];
        }

        $products = file_get_contents($url);

        $products = json_decode($products);

        return view('pages.favorites', compact('products', 'categories'));
    }

    public function restore()
    {
        if (! empty(Auth::user())) {
            Cart::restore(Auth::user()->id);

            $items = Cart::content();

            foreach ($items as $item) {
                $price = $item->price;
                $id = $item->id;

                $product = file_get_contents(env('BFL_API_URL').'/shopping-products/product/'.Session::get('store').'/'.$id);

                $payload = json_decode($product, true);

                //  print_r($payload);

                if (! $payload) {
                    $missing_items[] = $item->name;
                    Cart::remove($item->rowId);
                } else {
                    if ($payload[0]['promo_tag'] != '') {
                        $item->price = $payload[0]['sale_price'];
                    } else {
                        if ($payload[0]['regular_price'] != '') {
                            $item->price = $payload[0]['regular_price'];
                        }
                    }

                    if ($item->price == null) {
                        $price = 0.00;
                    } else {
                        $price = $item->price;
                    }

                    Cart::update($item->rowId, [
                        'price' => $price,
                    ]);
                }
            }
            // Add the items back to the shopping cart table so if they log right back out it's still saved
            try {
                Cart::store(Auth::user()->id);
            } catch (\Exception $e) {
                $l = 'already loaded';
            }

            if (! empty($missing_items)) {
                return $missing_items;
            }
        }
    }

    public function loyalty(Request $request)
    {

        // dd($request);

        $url = env('BFL_API_URL').'/loyalty-program-login/';

        $request->store_code = Session::get('store');

        $client = new \GuzzleHttp\Client();
        $response = $client->post($url, [\GuzzleHttp\RequestOptions::JSON => json_encode($request)]);
        $products = $response->getBody()->getContents();

        return $products;
    }

    public function loyalty_index()
    {
        $categories = $this->categories;

        return view('account.loyalty', compact('products', 'categories'));
    }
}
