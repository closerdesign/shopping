<?php

namespace App\Http\Controllers;

use App\Mail\OrderCompleted;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PaymentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('approved');
    }

    /**
     *  In Store Payment
     */

    public function instore_payment(Request $request, Order $order)
    {
        $this->validate($request, [
            'final_amount' => 'required|between:0,99.999'
        ]);

        $order->status = 'COMPLETED';
        $order->final_amount = request()->final_amount;
        $order->payment_method = 'InStore';
        $order->save();

        Mail::to($order->email)
            ->send(new OrderCompleted($order));

        return redirect()
            ->route('admin')
            ->with([
                'message' => [
                    'type' => 'success',
                    'message' => 'Order has been completed using In Store Payment Method.'
                ]
            ]);
    }
}
