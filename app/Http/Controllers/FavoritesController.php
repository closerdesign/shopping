<?php

namespace App\Http\Controllers;

use App\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FavoritesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     */

    public function sync(Request $request)
    {
        DB::table('favorites')
            ->where('user_id', auth()->user()->id)
            ->delete();

        foreach (request()->upcs as $upc)
        {
            DB::table('favorites')
                ->insert(
                    [
                        'upc' => $upc,
                        'user_id' => auth()->user()->id
                    ]
                );
        }

        return auth()->user()->favorites()->pluck('upc')->toArray();
    }
}
