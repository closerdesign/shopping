<?php

namespace App\Http\Controllers;

use App\Delivery;
use App\Order;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EnvoyController extends Controller
{
    /**
     *  Create and send delivery to Shipt.
     */
    public static function send($id)
    {
        $order = Order::find($id);

        try {
            $client = new Client();

            $response = $client->get('https://sara.buyforlessok.com/api/store-by-code/'.$order->store);

            $store = json_decode($response->getBody());
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        $del_date = date('Y-m-d', strtotime(Session::get('delivery_date'))).' '.date('H:i', strtotime(Session::get('delivery_time')));

        $shipt_date = date(DATE_ISO8601, strtotime($del_date));

        $data = '{
        
            "pickup": {

            "location": {
        
              "address": "'.$store->address.'",
        
              "address_2": null,
        
              "city": "'.$store->city.'",
        
              "state": "'.$store->state.'",
        
              "postal_code": "'.$store->postal_code.'"
        
            },
        
            "special_instructions": ""
        
          },
        
          "dropoff": {
        
            "location": {
        
              "address": "'.$order->delivery_address.'",
        
              "address_2": "",
        
              "city": "'.$order->delivery_city.'",
        
              "state": "OK",
        
              "postal_code": "'.$order->delivery_zip_code.'"
        
            },
        
            "contact": {
        
              "first_name": "'.$order->delivery_first_name.'",
        
              "last_name": "'.$order->delivery_last_name.'",
        
              "email": "'.$order->email.'",
        
              "phone_number": "'.$order->delivery_phone.'"},
        
            "delivery_instructions": "'.$order->delivery_instructions.'",
        
            "contains_alcohol": false
        
          },
        
          "external_reference_id": '.$order->id.',
        
          "deliver_by": "'.$shipt_date.'",
        
          "requested_items_summary": "",
        
         "tip": 0,
        "order_amount": '.$order->total_order.',
        "total_items":'.count($order->items).'
        
        }';

        try {
            $client = new Client();

            $response = $client->post(env('SHIPT_URL').'/deliveries', [

                'headers' => [
                    'cache-control' => 'no-cache',
                    'Connection' => 'keep-alive',
                    'Content-Length' => '1346',
                    'Accept-Encoding' => 'gzip, deflate',
                    'Host' => 'envoy.shipt.com',
                    'Postman-Token' => '905d0a31-d14d-4a40-88b4-68771d919865,964eb60f-87a9-4957-b84b-13fec2e8b931',
                    'Cache-Control' => 'no-cache',
                    'Accept' => '*/*',
                    'User-Agent' => 'PostmanRuntime/7.15.2',
                    'Authorization' => 'Bearer '.env('SHIPT_ACCESS_TOKEN'),
                    'Content-Type' => 'application/json',
                ],

                'body' => $data,

            ]);

            $json = json_decode($response->getBody());

            $order->delivery_id = $json->id;
            $order->delivery_time = $shipt_date;

            $order->save();

            Delivery::create([

                'order_id' => $order->id,
                'first_name_delivery' => $order->delivery_first_name,
                'last_name_delivery' => $order->delivery_last_name,
                'delivery_address' => $order->delivery_address,
                'phone_delivery' => $order->delivery_phone,
                'zip_code_delivery' => $order->delivery_zip_code,
                'delivery_instructions' => $order->delivery_instructions,
                'city_delivery' => $order->delivery_city,
                'state_delivery' => 'OK',
                'delivery_time' => $order->delivery_time,
                'delivery_reference_id' => $order->delivery_id,

            ]);

            return $order->delivery_id;
        } catch (\Exception $e) {
            return $data;
//            return $e->getMessage();
        }
    }
}
