<?php

namespace App\Http\Controllers;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *  Cart Item By ID.
     */
    public function item_by_id($id)
    {
        return Cart::content()->filter(function ($item) use ($id) {
            return $item->id === $id;
        })->first();
    }

    /**
     *  Update
     */

    public function update(Request $request, $id)
    {
        $item = Cart::update($id, [
            'options' => [
                'allow_substitutions' => request()->allow_substitutions,
                'comments' => request()->comments,
                'dept_code' => request()->dept_code,
                'image' => request()->image,
                'scale_flag' => request()->scale_flag,
            ],
            'qty' => request()->qty,
        ]);

        return response($item,200);
    }
}
