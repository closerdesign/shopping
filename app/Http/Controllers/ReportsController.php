<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['all_by_array']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monthly_by_store = $this->monthly_by_store();
        $daily = $this->daily_by_store();
        $avg_order = $this->avg_order_val();
        $all_time_rev = $this->all_time_rev();
        $total_by_month = $this->total_by_month();
        $total_by_day = $this->total_by_day();
        $avg_order_by_store = $this->avg_order_val();
        $total_store = $this->all_time_rev();
        $avg_order_val_all = $this->avg_order_val_all();

        $sales_by_date = $this->sales_by_date();

        $x = 0;
        $z = 0;
        $sum_total = 0;
        $avg_total = 0;
        $count_total = 0;

        foreach ($total_store as $ts) {
            $sum_total = ($sum_total + $ts->sum_store);
        }

        foreach ($avg_order_by_store as $s) {
            $avg_total = ($avg_total + $s->avg_order);
        }

        foreach ($all_time_rev as $a) {
            $count_total = ($count_total + $a->count_store);
        }

        $avg_total = ($avg_total / count($avg_order_by_store));

        return view('reports.index', compact('monthly_by_store', 'daily', 'avg_order', 'all_time_rev', 'total_by_month', 'x', 'sales_by_date', 'avg_order_by_store', 'total_store', 'z', 'sum_total', 'avg_total', 'count_total', 'avg_order_val_all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function all_by_array()
    {
        $orders['monthly_by_store'] = $this->monthly_by_store();
        $orders['daily'] = $this->daily_by_store();
        $orders['avg_order'] = $this->avg_order_val();
        $orders['all_time_rev'] = $this->all_time_rev();
        $orders['total_by_month'] = $this->total_by_month();
        $orders['total_by_day'] = $this->total_by_day();
        $orders['avg_order_by_store'] = $this->avg_order_val();
        $orders['total_store'] = $this->all_time_rev();
        $orders['x'] = 0;
        $orders['z'] = 0;
        $orders['sum_total'] = 0;
        $orders['avg_total'] = 0;
        $orders['payment_type'] = $this->payment_type();
        $orders['by_complex'] = $this->by_complex();
        $orders['count_total'] = 0;
        $orders['avg_order_val'] = $this->avg_order_val_all();

        //    $orders['orderCount'] = $this->buildOrderCount();

        foreach ($orders['total_store'] as $ts) {
            $orders['sum_total'] = ($orders['sum_total'] + $ts->sum_store);
        }

        foreach ($orders['avg_order_by_store'] as $s) {
            $orders['avg_total'] = ($orders['avg_total'] + $s->avg_order);
        }

        foreach ($orders['all_time_rev'] as $a) {
            $orders['count_total'] = ($orders['count_total'] + $a->count_store);
        }

        $orders['avg_total'] = ($orders['avg_total'] / count($orders['avg_order_by_store']));

        echo json_encode($orders);
    }

    public function buildOrderCount()
    {
        $monthly = $this->monthly_by_store();
        $monthly = json_decode($monthly);

        foreach ($monthly as $m) {
            echo $m['store'];
            echo $m['mnth'];
            echo $m['yr'];
            echo $m['fulfilled'];
            $var[$m['store']][$m['mnth']][$m['yr']] = $m[$m['fulfilled']];
        }

        // return $var;
    }

    public function monthly_by_store()
    {
        $order = DB::select(DB::raw("SELECT count(*) as fulfilled, SUM(final_amount) as totals, MONTH(pickup_date) as mnth, YEAR(pickup_date) as yr, store FROM orders
                                      WHERE pickup_date BETWEEN DATE_SUB(date_add(date_add(LAST_DAY(CURDATE()),interval 1 DAY),interval -1 MONTH), INTERVAL 1 YEAR) AND CURDATE()
                                      AND status IN('DELIVERED', 'COMPLETED') 
                                      GROUP BY MONTH(`pickup_date`), YEAR(pickup_date), store
                                      ORDER BY yr, mnth"));

        return $order;
    }

    public function daily_by_store()
    {
        $order = DB::select(DB::raw("SELECT count(*) as fullfilled, SUM(final_amount) as totals, DAY(pickup_date) as day, store FROM orders
                                      WHERE pickup_date BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE()
                                      AND status IN('DELIVERED', 'COMPLETED')
                                      GROUP BY DAY(`pickup_date`), store
                                      ORDER BY pickup_date"));

        return $order;
    }

    public function avg_order_val()
    {
        $order = DB::select(DB::raw("SELECT AVG(final_amount) as avg_order, store FROM orders WHERE status IN('DELIVERED', 'COMPLETED') GROUP BY store "));

        return $order;
    }

    public function avg_order_val_all()
    {
        $order = DB::select(DB::raw("SELECT AVG(`final_amount`) as avg_basket FROM orders WHERE pickup_date Between '2017-01-01' AND CURDATE() AND `status` IN('DELIVERED', 'COMPLETED')"));

        return $order[0];
    }

    public function all_time_rev()
    {
        return DB::select(DB::raw("SELECT SUM(final_amount) as sum_store, count(1) count_store, store FROM orders WHERE status IN('DELIVERED', 'COMPLETED') GROUP BY store "));
    }

    public function total_by_month()
    {
        $order = DB::select(DB::raw("SELECT count(*) as fulfilled, SUM(final_amount) as totals, MONTH(pickup_date) as mnth, YEAR(pickup_date) as yr FROM orders
                                      WHERE pickup_date BETWEEN DATE_SUB(date_add(date_add(LAST_DAY(CURDATE()),interval 1 DAY),interval -1 MONTH), INTERVAL 1 YEAR) AND CURDATE()
                                      AND status IN('DELIVERED', 'COMPLETED')
                                      GROUP BY MONTH(`pickup_date`), YEAR(pickup_date)
                                      ORDER BY yr, mnth"));

        return $order;
    }

    public function total_by_day()
    {
        $order = DB::select(DB::raw("SELECT count(*) as fullfilled, SUM(final_amount) as totals, pickup_date as day, `pickup_date` FROM orders
                                      WHERE pickup_date BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND CURDATE()
                                      AND status IN('DELIVERED', 'COMPLETED')
                                      GROUP BY DAY(`pickup_date`)
                                      ORDER BY pickup_date"));

        return $order;
    }

    /**
     * show sales by date default 30 days
     * return @array of date[]=$total.
     */
    public function sales_by_date()
    {
        $days = $this->the_days(30);

        foreach ($days as $d) {
            $order = DB::select(DB::raw("SELECT count(*) as fullfilled, SUM(final_amount) as totals, `pickup_date` FROM orders
                                      WHERE pickup_date = '$d'
                                      AND status IN('DELIVERED', 'COMPLETED')
                                      GROUP BY `pickup_date`
                                      ORDER BY pickup_date"));

            if (isset($order[0]->totals)) {
                $r[$d] = $order[0]->totals;
            } else {
                $r[$d] = 0;
            }
        }

        return $r;
    }

    public function payment_type()
    {
        $payment = DB::select(DB::raw("SELECT Distinct(`payment_method`) as payment_method, count(*) as num_method, `store` FROM orders  WHERE status IN('DELIVERED', 'COMPLETED') GROUP BY `store`, `payment_method`"));

        return $payment;
    }

    public function by_complex()
    {
        $complex = DB::select(DB::raw("SELECT Distinct(`complex`) as complex, count(*) as num_complex  FROM orders 
                                      WHERE status IN('DELIVERED', 'COMPLETED') AND is_complex = 1 AND `complex` != ''
                                      GROUP BY  `complex`"));

        return $complex;
    }

    /**
     * @param $start number of days to go back
     * @return array of dates
     */
    public function the_days($start)
    {
        if (! $start) {
            $start = 7;
        }
        $date = date('Y-m-d', strtotime(" -$start days"));
        $end_date = date('Y-m-d', strtotime('today'));

        while (strtotime($date) <= strtotime($end_date)) {
            $days[] = $date;
            //   echo $date."<br>";
            $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
        }

        return $days;
    }
}
