<?php

namespace App\Http\Controllers;

use App\Order;
use Faker\Provider\Payment;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Log;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class StripeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *  Create A Customer
     */

    public function create_customer()
    {
        Stripe::setApiKey(env(session()->get('store') . '_STRIPE_SECRET'));

        $customer = Customer::create([
            'email' => auth()->user()->email
        ]);

        $user = auth()->user();
        $user->stripe_id = $customer->id;
        $user->save();

        return response($customer, 200);
    }

    /**
     *  Create Payment Intent
     */

    public function create_payment_intent()
    {
        Stripe::setApiKey(env(session()->get('store') . '_STRIPE_SECRET'));

        if( auth()->user()->stripe_id == null )
        {
            $this->create_customer();
        }

        $intent = PaymentIntent::create([
            'amount' => round((Cart::total() + (Cart::total() * .2)) * 100),
            'currency' => 'usd',
            'payment_method_types' => ['card'],
            'capture_method' => 'manual',
        ]);

        session()->put('payment_intent', $intent->id);

        Log::info('Payment Intent has been created: ' . $intent->toJSON());

        return $intent;
    }

    /**
     *  Capture Payment
     */

    public static function capture_payment(Order $order, $amount)
    {
        try
        {
            Stripe::setApiKey(env($order->store . '_STRIPE_SECRET'));

            $payment = PaymentIntent::retrieve($order->paypal_token);
            $payment->capture(['amount_to_capture' => $amount * 100]);

            return response($payment, 200);
        }

        catch (\Exception $e)
        {
            return 'Oops! ' . $e->getMessage();
        }
    }
}
