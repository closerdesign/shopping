<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *  Favorites
     */

    public function favorites()
    {
        return auth()->user()->favorites()->pluck('upc')->toArray();
    }
}
