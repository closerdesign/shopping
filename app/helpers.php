<?php

    /**
     *  Format UPC Numbers.
     */
    function format_upc($upc)
    {
        $upc = substr($upc, 0, -1);

        return str_pad($upc, 13, 0, STR_PAD_LEFT);
    }

    function store_data($store_code)
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->get('https://sara.buyforlessok.com/api/store-by-code/'.$store_code);

        $store = $response->getBody();

        $store = json_decode($store);

        return $store;
    }
