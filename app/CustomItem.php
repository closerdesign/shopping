<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomItem extends Model
{
    protected $fillable = [
        'item_name',
        'estimated_price',
        'comments'
    ];
}
