<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotesForm extends Mailable
{
    use Queueable, SerializesModels;

    public $request;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request, $order)
    {
        $this->request = $request;

        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.notes-form')->subject('[Online Shopping] Note for order # '.$this->order->id);
    }
}
