<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CancelledOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    public $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $request)
    {
        $this->order = $order;

        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.cancelled-order')->subject(explode(' ', $this->order->name)[0].', your order has been cancelled.');
    }
}
