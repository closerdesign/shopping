<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ThanxPayPal extends Mailable
{
    use Queueable, SerializesModels;

    public $transactions;

    public $date_from;

    public $date_to;

    public $report;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($transactions, $date_from, $date_to, $report)
    {
        $this->transactions = $transactions;
        $this->date_from = $date_from;
        $this->date_to = $date_to;
        $this->report = $report;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.thanx.paypal-transactions')
            ->subject('[Uptown Grocery Co.] Thanx Transactions Report ['.$this->date_from.' - '.$this->date_to.']')
            ->attachData($this->report, 'thanx-report-'.$this->date_from.'-'.$this->date_to.'.csv', [
                'mime' => 'text/csv',
            ]);
    }
}
