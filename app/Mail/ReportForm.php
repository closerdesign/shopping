<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportForm extends Mailable
{
    use Queueable, SerializesModels;

    public $id;

    public $store;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id, $store)
    {
        $this->id = $id;
        $this->store = $store;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.report-form')->subject('[Online Shopping] Item Reported as Not Carried: '.$this->id);
    }
}
