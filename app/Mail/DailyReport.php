<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DailyReport extends Mailable
{
    use Queueable, SerializesModels;

    public $transactions;

    public $store_code;

    public $date;

    public $report;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($store_code, $transactions, $date, $report)
    {
        $this->store_code = $store_code;
        $this->transactions = $transactions;
        $this->date = $date;
        $this->report = $report;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.reports.daily')
            ->subject('['.$this->store_code->first()->store.'] Online Shopping Report')
            ->attachData($this->report, 'daily-report-'.$this->date.'.csv', [
                'mime' => 'text/csv',
            ]);
    }
}
