<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderConfirmationDelivery extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    public $delivery;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $delivery)
    {
        $this->order = $order;
        $this->delivery = $delivery;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.order-confirmation-delivery')->subject(explode(' ', $this->order->name)[0].', Your online shopping order has been placed.');
    }
}
