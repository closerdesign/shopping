<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
        'is_approved',
        'is_admin',
        'tax_exempt',
        'billing_address',
        'zip_code',
        'phone',
        'preferred_store',
        'city',
        'loyalty_member_id',
        'suspended',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *  Favorites.
     */
    public function favorites()
    {
        return $this->hasMany(\App\Favorite::class);
    }

    /**
     *  Is Admin?
     */
    public function isAdmin()
    {
        $admin = false;

        if ($this->is_admin == 1) {
            $admin = true;
        }

        return $admin;
    }

    /**
     *  Is Approved? -Team Members-.
     */
    public function isApproved()
    {
        $approved = false;

        if ($this->is_approved == 1) {
            $approved = true;
        }

        return $approved;
    }

    /**
     *  Is Favorite.
     */
    public function isFavorite($upc)
    {
        $favorite = false;

        if (count(Favorite::where('user_id', $this->id)->where('upc', $upc)->get()) == 1) {
            $favorite = true;
        }

        return $favorite;
    }
}
