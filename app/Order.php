<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    protected $fillable = [

        'pickup_date',
        'pickup_time',
        'name',
        'phone',
        'email',
        'comments',
        'payment_method',
        'total_order',
        'final_amount',
        'subtotal',
        'total',
        'tax',
        'pos_id',
        'pos_received',
        'status',
        'store',
        'paypal_token',
        'paypal_payer_id',
        'cancel_reason',
        'is_complex',
        'complex',
        'substitutions_preference',
        'billing_address',
        'zip_code',
        'tax_exempt',
        'tax_number',
        'completion_date',
        'shopper',
        'shopper_start_time',
        'shopper_end_time',
        'delivery_tip',
        'delivery_id',
        'delivery_address',
        'delivery_city',
        'delivery_zip_code',
        'delivery_first_name',
        'delivery_last_name',
        'delivery_phone',
        'delivery_instructions',
        'delivery_time',
        'score',
        'score_comments',

    ];

    /**
     *  Substitutions Preference Mutator.
     */
    public function getSubstitutionsPreferenceAttribute($value)
    {
        $subs = [
            null => 'Contact Me',
            'contact_me' => 'Contact Me',
            'use_best_judgement' => 'Use Best Judgement',
            'do_not_substitute' => 'Do Not Substitute',
        ];

        return $subs[$value];
    }

    /**
     *  Is active attribute.
     */
    public function getIsActiveAttribute()
    {
        if ($this->status == 'PLACED' || $this->status == 'In Progress' || $this->status == 'POS') {
            return true;
        }

        return false;
    }

    /**
     *  Label Attribute
     */

    public function getLabelAttribute()
    {
        $vars = [
            'PAYMENT' => 'label-default',
            'PLACED' => 'label-danger',
            'In Progress' => 'label-warning',
            'POS' => 'label-info',
            'COMPLETED' => 'label-success',
            'DELIVERED' => 'label-info',
            'CANCELLED' => 'label-info'
        ];

        return $vars[$this->attributes['status']] . ' text-uppercase';
    }

    /**
     *  Is a Custom Items Order
     */

    public function getIsCustomItemsOrderAttribute()
    {
        if( $this->items()->whereCustomItem(1)->count() == $this->items()->count() )
        {
            return true;
        }

        return false;
    }

    /**
     *  First Name Attribute.
     */

    public function getFirstNameAttribute()
    {
        return explode(' ', $this->attributes['name'])[0];
    }

    /**
     *  Items.
     */
    public function items()
    {
        return $this->hasMany(\App\Item::class);
    }

    /**
     *  Notes.
     */
    public function notes()
    {
        return $this->hasMany(\App\Note::class);
    }

    /**
     *  User Orders.
     */
    public function scopeUser()
    {
        return $this->where('email', Auth::user()->email);
    }

    /**
     * Customer Relationship
     */

    public function customer()
    {
        return $this->belongsTo(User::class, 'user_id', 'id'    );
    }
}
