<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'upc',
        'name',
        'price',
        'qty',
        'allow_substitutions',
        'comments',
        'dept_code',
        'image',
        'picked',
        'picked_qty',
        'out_of_stock',
        'substitute',
        'scale_flag',
        'custom_item'
    ];

    public function getAllowSubstitutionsAttribute($value)
    {
        $subs = 'No';

        if ($value == 1) {
            $subs = 'Yes';
        }

        return $subs;
    }

    public function order()
    {
        return $this->belongsTo(\App\Order::class);
    }

    /**
     *  Subtotal.
     */
    public function getSubtotalAttribute()
    {
        return $this->attributes['picked_qty'] * $this->attributes['price'];
    }

    /**
     *  Is Picked Attribute.
     */
    public function getIsPickedAttribute()
    {
        if ($this->out_of_stock) {
            return true;
        }

        if ($this->picked_qty >= $this->qty) {
            return true;
        }

        return false;
    }

    public function setCustomItemAttribute($value)
    {
        if($value !== null)
        {
            return $value;
        }

        return 0;
    }
}
