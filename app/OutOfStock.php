<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutOfStock extends Model
{
    protected $fillable = [
        'upc',
        'name',
        'store',
        'user_id',
    ];
}
