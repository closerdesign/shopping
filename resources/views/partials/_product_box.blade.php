<div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">

    <div class="product-box">

        <div class="product-promo">
            @if(@$product->promo_tag != "") <span class="label label-danger"
                                                  style="line-height: 2.5">@lang('productbox.sale')</span><br/> @endif
        </div>

        <div class="product-description">

            @if( !Auth::guest() )
                <span class="favorite add-fav" id="fav-{{ @$product->upc }}" title="Add to Favorites">
                    <i id="icon-{{ @$product->upc }}"
                       class="fa @if( !Auth::guest() && Auth::user()->isFavorite($product->upc) ) fa-heart @else fa-heart-o @endif"></i>
                </span>
            @else
                <a href="/login"><span class="favorite" id="fav-{{ $product->upc }}" title="Add to Favorites">
                    <i class="fa fa-heart-o"></i>
                </span></a>
            @endif

            <a data-toggle="modal" data-target="#modal-{{ $product->upc }}" style="cursor: pointer;">
                <div class="product-img" style="background-image: url('{{ @$product->image }}')">
                    <img src="/img/transparent.png" class="img-responsive">
                </div>
            </a>

            <div class="product-price">
                @if(@$product->promo_tag != "")
                    <?php $sale_qty_price = (@$product->sale_price / @$product->sale_qty); ?>
                    ${{ @$product->sale_price }}
                @else
                    ${{ @$product->regular_price }}
                @endif
            </div>

            @if(@$product->sale_qty > 1)
                <div class="product-sale-qty text-center">{{@$product->sale_qty}}
                    <small>for</small>
                </div>
            @endif

            <div class="product-name">
                @if( substr(@$product->brand_name, 0, 13) == '1 LB is about' && @$product->size == 'EA' )<br/>@else
                    <span class="text-red"><b>{{ trim(@$product->brand_name) }}</b></span><br/>
                @endif
                <a data-toggle="modal" data-target="#modal-{{ @$product->upc }}" style="cursor: pointer;"
                   @if(strlen(@$product->name) > 65)title="{{ @$product->name }}"@endif >
                    {{ Str::limit(@$product->name, 65) }}
                </a><br/>
                <span class="text-muted" style="font-size: .8em">{{ @$product->size }}</span>
            </div>

            <div class="product-scale">
                @if( $product->scale_flag == 1 )
                    <span class="label label-warning"><i class="fa fa-balance-scale"
                                                         aria-hidden="true"></i> @lang('productbox.weight')</span>
                @else &nbsp;
                @endif
            </div>

        </div>
        <div class="product-add">

            <form class="add-item" id="add-{{ @$product->upc }}" action="" method="post" loader="false">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $product->upc }}">
                @if( substr(@$product->brand_name, 0, 13) == '1 LB is about' )
                    <input type="hidden" name="name" value="{{ @$product->name }} - {{ @$product->size }}">
                @else
                    <input type="hidden" name="name"
                           value="{{ trim(@$product->brand_name) }} {{ @$product->name }} - {{ @$product->size }}">
                @endif
                <input type="hidden" name="price"
                       value="@if(@$product->promo_tag != ""){{ $sale_qty_price }}@else{{ @$product->regular_price }}@endif">
                <input type="hidden" name="tax" value="{{ @$product->tax_pct }}">
                <input type="hidden" name="dept_code" value="{{ @$product->dept_code }}">
                <input type="hidden" name="image" value="{{ urlencode(@$product->image) }}">
                <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-default @if(@$product->size == 'LB') minusOne-lb @else minusOne @endif"
                                type="button"><i class="fa fa-minus fa-fw"></i></button>
                    </span>
                    <input type="number" step=".5" class="form-control text-center qty-val" name="qty"
                           @if(@$product->size == 'LB') value=".5" min=".5" @else value="1" min="1" @endif>
                    <span class="input-group-btn">
                        <button class="btn btn-default @if(@$product->size == 'LB')plusOne-lb @else plusOne @endif"
                                type="button"><i class="fa fa-plus fa-fw"></i></button>
                    </span>
                </div>
                <button class="btn btn-primary form-control" style="margin-top: 5px;">
                    <i class="fa fa-cart-plus"></i> @lang('productbox.add')
                </button>

            </form>

        </div>

    </div>

</div>