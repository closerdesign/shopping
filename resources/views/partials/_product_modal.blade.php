<div class="modal fade" id="modal-{{ $product->upc }}" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div style="padding: 10px;">

                    @if( !Auth::guest() )
                        <span class="favorite favorite-xs add-fav" id="modalfav-{{ $product->upc }}" title="Add to Favorites">
                            <i id="icon-{{ $product->upc }}" class="fa @if( !Auth::guest() && Auth::user()->isFavorite($product->upc) ) fa-heart @else fa-heart-o @endif"></i>
                        </span>
                    @else
                        <a href="/register">
                            <span class="favorite favorite-xs" id="modalfav-{{ $product->upc }}" title="Add to Favorites">
                                <i class="fa fa-heart-o"></i>
                            </span>
                        </a>
                    @endif

                    <div class="row">

                        <div class="col-md-3 col-sm-6 col-xs-6 text-center">
                            <div class="form-group text-center">
                                <a class="image-popup-no-margins" type="img-popup" href="{{ @$product->image }}">
                                    <img src="{{ @$product->image }}" alt="{{ @$product->name }}" class="img-responsive img-rounded" style="margin: auto; max-height: 300px">
                                </a>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-6">
                            <p>

                                @if(@$product->promo_tag != "")
                                    <span class="label label-danger">@lang('productbox.sale')</span><br />
                                @endif

                                <b>

                                    @if(trim(@$product->brand_name) != "")
                                        @if( substr(@$product->brand_name, 0, 13) == '1 LB is about' && @$product->size == 'EA' ) @else
                                            <span class="text-red">{{ @$product->brand_name }}</span><br />
                                        @endif
                                    @endif

                                    {{ @$product->name }}<br />
                                </b>
                                <small>{{ @$product->size }}</small>
                            </p>
                            <p class="text-muted">
                                {!! @$product->description !!}
                            </p>
                            <p class="text-muted" style="font-size: .8em">
                                UPC: {{ @$product->upc }}
                            </p>

                            @if(@$product->promo_tag != "")
                                <div class="alert alert-info">
                                    <small>@lang('product.sale-alert')</small>
                                </div>
                            @endif

                            @if( @$product->dept_code == '005' || @$product->dept_code == '055' )
                                <div class="alert alert-warning disclaimer">
                                    @lang('product.alcohol-alert')
                                </div>
                            @endif

                            @if( (URL::to('') == 'https://shopping.uptowngroceryco.com') && $product->dept_code == '002' )
                                <div class="alert alert-info disclaimer">
                                    @lang('product.meat-alert')
                                </div>
                            @endif

                        </div>
                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <h2 class="text-right">
                                            <span class="label label-default"  @if(@$product->promo_tag != "") style="text-decoration: line-through" @endif>
                                                ${{ @$product->regular_price }} @if(@$product->size == 'LB') / lb @endif
                                            </span>

                                @if(@$product->promo_tag != "")
                                    <span class="label label-danger">
                                                    <?php $sale_qty_price = (@$product->sale_price / @$product->sale_qty); ?>

                                        @if(@$product->sale_qty > 1)
                                            {{ @$product->sale_qty }} for ${{ @$product->sale_price }}
                                        @else
                                            ${{ number_format($sale_qty_price, 2) }} @if(@$product->size == 'LB') / lb @endif
                                        @endif

                                                </span>
                                @endif

                            </h2>

                            @if( @$product->scale_flag == 1 )
                                <p class="text-right" style="padding-top: 15px; font-size: .9em">
                                    <span class="label label-warning"><i class="fa fa-balance-scale visible-lg-inline" aria-hidden="true"></i> @lang('productbox.weight')</span>
                                </p>
                            @endif

                            <hr />
                            <form class="add-item form-group" id="add-modal-{{ @$product->upc }}" action="" method="post" loader="false">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $product->upc }}">

                                @if( substr(@$product->brand_name, 0, 13) == '1 LB is about' )
                                    <input type="hidden" name="name" value="{{ @$product->name }} - {{ @$product->size }}">
                                @else
                                    <input type="hidden" name="name" value="{{ trim(@$product->brand_name) }} {{ @$product->name }} - {{ @$product->size }}">
                                @endif

                                <input type="hidden" name="price" value="@if(@$product->promo_tag != ""){{ $sale_qty_price }}@else{{ @$product->regular_price }}@endif">
                                <input type="hidden" name="tax" value="{{ @$product->tax_pct }}">
                                <input type="hidden" name="dept_code" value="{{ @$product->dept_code }}">
                                <input type="hidden" name="image" value="{{ urlencode(@$product->image) }}">
                                <div class="input-group">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default @if(@$product->size == 'LB') minusOne-lb @else minusOne @endif" type="button"><i class="fa fa-minus fa-fw"></i></button>
                                                </span>
                                    <input type="number" step=".5" class="form-control text-center qty-val" name="qty" @if(@$product->size == 'LB') value=".5" min=".5" @else value="1" min="1" @endif required>
                                    <span class="input-group-btn">
                                                    <button class="btn btn-default @if(@$product->size == 'LB') plusOne-lb @else plusOne @endif" type="button"><i class="fa fa-plus fa-fw"></i></button>
                                                </span>
                                </div>
                                <button class="btn btn-primary form-control" style="margin-top: 5px;">
                                    <i class="fa fa-cart-plus"></i> @lang('productbox.add')
                                </button>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>