<div class="row">
    <div class="col-md-12 text-right">
        <nav>
            <ul class="pagination pagination-sm">
                @if($products->products->prev_page_url != null)
                    <li>
                        <a href="?@if(isset($_GET['keyword']))keyword={{ $_GET['keyword'] }}&@endif{{ explode("?", $products->products->prev_page_url)[1] }}" aria-label="Previous"><span aria-hidden="true">@lang('pagination.prev')</span></a>
                    </li>
                @endif

                @if($products->products->next_page_url != null)
                    <li>
                        <a href="?@if(isset($_GET['keyword']))keyword={{ $_GET['keyword'] }}&@endif{{ explode("?", $products->products->next_page_url)[1] }}" aria-label="Previous"><span aria-hidden="true">@lang('pagination.next')</span></a>
                    </li>
                @endif
            </ul>
        </nav>
    </div>
</div>