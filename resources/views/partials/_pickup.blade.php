@if(Session::has('store'))
    <div class="pickup-callout">

            <div class="pull-right" id="pickup_loc" data-toggle="modal" data-target="#storeModal">
                @if(!Session::has('is_complex') && !Session::has('delivery_date'))
                    {{--Uptown Locations--}}
                    @if( Session::get('store') == '1230')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): Edmond</h4><br />
                        1230 W. Covell Rd &nbsp;Edmond, OK 73003
                    @elseif(Session::get('store') == '9515')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): The Village</h4><br />
                        9515 N. May Ave. &nbsp;OKC, OK 73120
                    @elseif(Session::get('store') == '1124')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): 1124 NE 36th St.</h4><br />
                        1124 NE 36th Street OKC, OK 73111
                    {{--Buy For Less Locations--}}
                    @elseif(Session::get('store') == '3501')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): NW Expressway</h4><br />
                        3501 NW Expressway &nbsp;OKC, OK 73112
                    @elseif(Session::get('store') == '7957')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): NW 23rd &amp; Council</h4><br />
                        7957 NW 23rd St. &nbsp;OKC, OK 73008
                    {{--Supermercado Locations--}}
                    @elseif(Session::get('store') == '2701')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): SW 29th &amp; May</h4><br />
                        2701 SW 29th St. &nbsp;OKC, OK 73119
                    @elseif(Session::get('store') == '3701')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): NW 36th &amp; MacArthur</h4><br />
                        3701 N. MacArthur Blvd. &nbsp;OKC, OK 73122
                    @elseif(Session::get('store') == '3713')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): SW 36th &amp; Western</h4><br />
                        3713 S. Western Ave. &nbsp;OKC, OK 73109
                    @elseif(Session::get('store') == '4150')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): SW 59th &amp; Walker</h4><br />
                        415 SW 59th St. &nbsp;OKC, OK 73109
                    {{--Smart Saver Locations--}}
                    @elseif(Session::get('store') == '1006')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): Midwest City</h4><br />
                        10011 SE 15th St. &nbsp;Midwest City, OK 73130
                    @elseif(Session::get('store') == '1205')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): Norman</h4><br />
                        1205 E. Lindsey St. &nbsp;Norman, OK 73071
                    @elseif(Session::get('store') == '1201')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): Yukon</h4><br />
                        1201 S. Cornwell Drive &nbsp;Yukon, OK 73099
                    @elseif(Session::get('store') == '2001')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): NE 23rd &amp; MLK</h4><br />
                        2001 NE 23rd St. &nbsp;OKC, OK 73111
                    @elseif(Session::get('store') == '4424')
                        <h4><i class="fa fa-car"></i> @lang('layout.pickup'): SE 44th &amp; High</h4><br />
                        1104 SE 44th St. &nbsp;OKC, OK 73129
                    @endif
                @elseif(Session::has('delivery_date'))
                    <h4><i class="fa fa-car"></i> Delivery from</h4><br />
                    @if( Session::get('store') == '1230')
                        1230 W. Covell Rd &nbsp;Edmond, OK 73003
                    @elseif(Session::get('store') == '9515')
                        9515 N. May Ave. &nbsp;OKC, OK 73120
                    @elseif(Session::get('store') == '1124')
                        1124 NE 36th Street OKC, OK 73111
                    @endif
                @else
                    <h4><i class="fa fa-car"></i> @lang('layout.delivery')</h4><br />
                    {{ Session::get('complex') }}
                @endif
            </div>
            {{--<div class="col-sm-6 col-xs-6" id="pickup_date">--}}
                {{--<h4><i class="fa fa-calendar"></i> On</h4><br />--}}
                {{--@if(Session::has('pickup_time') && Session::has('pickup_date'))--}}
                    {{--{{ date('l, F d', strtotime(Session::get('pickup_date'))) }} at {{ date('h:i A', strtotime(Session::get('pickup_time'))) }}--}}
                    {{--<a class="text-danger" href="{{ action('ShoppingController@pickup_reset') }}"><i class="fa fa-times-circle"></i></a>--}}
                {{--@else--}}
                    {{--<a class="text-danger" style="cursor: pointer;" data-toggle="modal" data-target="#pickupModal">--}}
                        {{--@lang('layout.reserve')--}}
                    {{--</a>--}}
                {{--@endif--}}
            {{--</div>--}}

    </div>
@endif