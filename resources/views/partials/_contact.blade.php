<div class="container">

    <p class="lead text-center"><label class="label label-danger">Questions? Feel free to contact us.</label></p>

    <div class="panel panel-default">
        <div class="panel-body">
            <b>Media Team</b><br />
            <i class="fa fa-phone"></i> help@buyforlessok.com
        </div>
    </div>

</div>