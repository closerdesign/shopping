{{--Store selector--}}
<div id="storeModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <form action="" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@lang('modals.change-store')</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <i class="text-danger fa fa-exclamation-triangle"></i> @lang('modals.session')
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">
                        <i class="fa fa-times"></i> @lang('modals.cancel')
                    </button>
                    <a href="{{ action('StoresController@store_selector') }}" class="btn btn-default" type="submit">
                        <i class="fa fa-check"></i> @lang('modals.confirm')
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>
{{--How It Works--}}
<div class="modal fade" id="howItWorks" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@lang('layout.howitworks')</h4>
            </div>
            <div class="modal-body">
                <p>
                    <ul>
                        @lang('modals.howitworks-list')
                    </ul>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('modals.gotit')</button>
            </div>
        </div>
    </div>
</div>