<nav class="navbar navbar-light navbar-app hidden-xs">

    @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable animated fadeInUp" style="margin-bottom: 0;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if ( Session::has('message') )
        <div class="alert alert-{{ Session::get('message')['type'] }} alert-dismissable animated fadeInUp" style="margin-bottom: 0;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! Session::get('message')['message'] !!}
        </div>
    @endif

    <div class="container">
        <div class="collapse navbar-collapse" id="app-navbar-collapse">

            @if(session()->has('categories'))
            <ul class="nav navbar-nav navbar-left">
                <li>
                    <div class="btn" style="padding-top: 15px;">
                        <i @click="categoriesWrapper = !categoriesWrapper" class="fa fa-bars"></i>
                    </div>
                </li>
            </ul>
            @endif

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">

                @if( auth()->user() )
                <li>
                    <a href="{{ route('checkout') }}">
                        <i class="fa fa-shopping-cart"></i>
                        <span class="badge badge-danger cart-total" v-text="productCount" ></span>
                    </a>
                </li>
                @endif

                <li>
                    <div class="btn" style="padding-top: 15px;">
                        <i @click="accountWrapper = !accountWrapper" class="fa fa-user"></i>
                    </div>
                </li>
            </ul>

            <form action="{{ action('ShoppingController@search_aws') }}" class="navbar-form navbar-right">
                <div class="input-group">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search" name="keyword">
                    </div>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</nav>

<div class="logo-block">
    <div class="text-center">
        <a href="{{ URL::to('') }}">
            <img class="img-circle" style="background: url({{ $brand->logo }})" src="/img/logo-placeholder.png" alt="{{ $brand->name }} Online Shopping Logo">
            {{--        <img class="logo" src="{{ $brand->logo }}" alt="{{ $brand->name }} Online Shopping Logo">--}}
        </a>
    </div>

</div>