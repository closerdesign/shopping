<script>
    $('#add-{{ $product->upc }}').validate({
        submitHandler: function(form)
        {
            if( '{{ $product->scale_flag }}' == '1' )
            {
                var x = confirm('Please remember! This is a weighted item and the unit of measure you are using is ' + '{{ $product->size }}');
            }

            if( (x == true) || ('{{ $product->scale_flag }}' == '0') )
            {
                $('#loader').fadeIn();

                $.post('{{ action('CartController@store') }}', $(form).serialize())
                    .done(function(data){
                    $.get('{{ action('CartController@refresh') }}', function(cart)
                    {
                        $('#cart-container').html(cart);
                    });
                    Notify('Product Added!', null, null, 'success');
                    $('#loader').fadeOut();
                });
            }
        }
    });
</script>