<div class="col-lg-3 col-md-4 col-lg-push-9 col-md-push-8 hidden-sm hidden-xs">

    <div class="panel panel-default">
        <div id="cart-head" class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-shopping-cart"></i> @lang('layout.cart-head')
                </div>
                <div class="col-xs-6">
                    <span class="badge pull-right">
                        {{ round(Gloudemans\Shoppingcart\Facades\Cart::count()) }}
                        @lang('layout.item')@if( \Gloudemans\Shoppingcart\Facades\Cart::count() > 1 || \Gloudemans\Shoppingcart\Facades\Cart::count() == 0 )s @endif
                    </span>
                </div>
            </div>
        </div>

        <div id="cart-body" class="panel-body">

            @if( \Gloudemans\Shoppingcart\Facades\Cart::count() < .5 )
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">@lang('layout.empty-cart')</p>
                    </div>
                </div>
                <div class="row add-custom">
                    @include('partials._addcustom')
                </div>
            @else

                @foreach( \Gloudemans\Shoppingcart\Facades\Cart::content() as $product )
                    <div class="row">
                        <div class="col-xs-12">
                            <p>
                                <b><a @if(preg_match("/[a-z]/i", $product->id)) href="#" @else href="{{ action('ShoppingController@product', $product->id) }}" @endif>{{ $product->name }}</a></b><br />
                                ${{ number_format($product->price, 2) }}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="input-group input-group-xs" style="max-width: 100px">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-xs @if(substr($product->name, strlen($product->name) - 4, strlen($product->name)) == '- LB') minusOne-lb @else minusOne @endif cartQty ajaxQty">
                                        <i class="fa fa-minus fa-fw"></i>
                                    </button>
                                </span>
                                <input class="form-control input-sm text-center qty-val" step=".5" name="qty" rowId="{{ $product->rowId }}" value="{{ $product->qty }}" @if(substr($product->qty, strlen($product->qty) - 2, strlen($product->qty)) == .5) min=".5" @else min="1" @endif readonly>
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-xs @if(substr($product->name, strlen($product->name) - 4, strlen($product->name)) == '- LB') plusOne-lb @else plusOne @endif cartQty ajaxQty">
                                        <i class="fa fa-plus fa-fw"></i>
                                    </button>
                                </span>
                            </div><br />
                        </div>
                        <div class="col-xs-3">
                            <p class="text-right"><b>${{ number_format($product->subtotal, 2) }}</b></p>
                        </div>
                        <div class="col-xs-3" style="padding-right: 0">
                            <button class="btn btn-xs btn-warning" data-toggle="collapse" data-target="#{{ $product->rowId }}">
                                @if($product->options->comments)
                                    <i class="fa fa-commenting"></i>
                                @else
                                    <i class="fa fa-comment"></i>
                                @endif
                            </button>
                            <button onclick="deleteItem('{{ $product->rowId }}')" class="btn btn-primary btn-xs">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>
                    <div class="row collapse" id="{{ $product->rowId }}">
                        <div class="col-md-12">
                            <textarea name="comments" id="comments" item="{{ $product->rowId }}" cols="30" rows="2"
                                      class="form-control comments" placeholder="Enter your comments...">{{ $product->options->comments }}</textarea><br />
                        </div>
                    </div>
                @endforeach
                <div class="row add-custom">
                    @include('partials._addcustom')
                </div><br />
                <div class="row">
                    <div class="col-xs-6">
                        <b>@lang('layout.tax')</b>
                    </div>
                    <div class="col-xs-6">
                        <?php $tax = (\Gloudemans\Shoppingcart\Facades\Cart::total() - \Gloudemans\Shoppingcart\Facades\Cart::subtotal()) ?>
                        <p class="text-right"><b>${{ number_format($tax, 2) }}</b></p>
                    </div>
                </div>
                <p class="text-muted text-left disclaimer">
                    <i class="fa fa-balance-scale fa-2x fa-pull-left fa-border" aria-hidden="true"></i>
                    @lang('layout.weight')
                </p>
                <hr>
                <div class="row">
                    <div class="col-xs-5">@if( \Gloudemans\Shoppingcart\Facades\Cart::total() < 50 )<span class="label label-info">@lang('layout.minimum')</span>@endif</div>
                    <div class="col-xs-7"><p class="lead text-right">@lang('layout.total') ${{ \Gloudemans\Shoppingcart\Facades\Cart::total() }}</p></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            @if( \Gloudemans\Shoppingcart\Facades\Cart::total() < 50 )
                                <a class="btn btn-primary form-control disabled">
                                    <i class="fa fa-shopping-cart"></i> @lang('layout.checkout')
                                </a>
                            @else
                                <a href="{{ action('CartController@checkout') }}" class="btn btn-primary form-control">
                                    <i class="fa fa-shopping-cart"></i> @lang('layout.checkout')
                                </a>
                            @endif
                        </p>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>

<div id="mobile-cart" class="visible-sm visible-xs">
    <div class="panel panel-default panel-cart">
        <div id="mobile-cart-head" class="panel-heading">
            <div class="row">
                <div class="col-xs-7">
                    <i class="fa fa-shopping-cart"></i>
                    @lang('layout.cart-head')
                    <small><i id="cart-toggle-icon" class="text-muted fa fa-chevron-right"></i></small>
                </div>
                <div class="col-xs-5">
                    <span class="badge pull-right">
                        {{ round(Gloudemans\Shoppingcart\Facades\Cart::count()) }}
                        @lang('layout.item')@if( \Gloudemans\Shoppingcart\Facades\Cart::count() > 1 || \Gloudemans\Shoppingcart\Facades\Cart::count() == 0 )s @endif
                    </span>
                </div>
            </div>
        </div>
        <div class="panel-body">

            @if( \Gloudemans\Shoppingcart\Facades\Cart::count() < .5 )
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">@lang('layout.empty-cart')</p>
                    </div>
                </div>
                <div class="row add-custom">
                    @include('partials._addcustom')
                </div>
            @else

                @foreach( \Gloudemans\Shoppingcart\Facades\Cart::content()->sortBy('name') as $product )
                    <div class="row">
                        <div class="col-xs-12">
                            <p>
                                <b><a @if(preg_match("/[a-z]/i", $product->id)) href="#" @else href="{{ action('ShoppingController@product', $product->id) }}" @endif>{{ $product->name }}</a></b><br />
                                ${{ number_format($product->price, 2) }}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6" style="padding-right: 0;">
                            <div class="input-group input-group-xs" style="width: 100px">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-xs @if(substr($product->name, strlen($product->name) - 4, strlen($product->name)) == '- LB') minusOne-lb @else minusOne @endif cartQty ajaxQty" type="submit">
                                        <i class="fa fa-minus fa-fw"></i>
                                    </button>
                                </span>
                                <input class="form-control input-sm text-center qty-val" step=".5" name="qty" rowId="{{ $product->rowId }}" value="{{ $product->qty }}" @if(substr($product->qty, strlen($product->qty) - 2, strlen($product->qty)) == .5) min=".5" @else min="1" @endif readonly>
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-xs @if(substr($product->name, strlen($product->name) - 4, strlen($product->name)) == '- LB') plusOne-lb @else plusOne @endif cartQty ajaxQty" type="submit">
                                        <i class="fa fa-plus fa-fw"></i>
                                    </button>
                                </span>
                            </div><br />
                        </div>
                        <div class="col-xs-2">
                            <p class="text-right"><b>${{ number_format($product->subtotal, 2) }}</b></p>
                        </div>
                        <div class="col-xs-4">
                            <div class="pull-right">
                                <button class="btn btn-xs btn-warning" data-toggle="collapse" data-target="#mobile{{ $product->rowId }}">
                                    @if($product->options->comments)
                                        <i class="fa fa-commenting"></i>
                                    @else
                                        <i class="fa fa-comment"></i>
                                    @endif
                                </button>
                                <button class="btn btn-xs btn-primary" onclick="deleteItem('{{ $product->rowId }}')">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row collapse" id="mobile{{ $product->rowId }}">
                        <div class="col-md-12">
                        <textarea name="comments" id="comments" item="{{ $product->rowId }}" cols="30" rows="2"
                                  class="form-control comments" placeholder="Enter your comments...">{{ $product->options->comments }}</textarea><br />
                        </div>
                    </div>
                @endforeach
                <div class="row add-custom">
                    @include('partials._addcustom')
                </div><br />
                <div class="row">
                    <div class="col-xs-6">
                        <b>@lang('layout.tax')</b>
                    </div>
                    <div class="col-xs-6">
                        <p class="text-right"><b>${{ number_format($tax, 2) }}</b></p>
                    </div>
                </div>
                <p class="text-muted text-left disclaimer">
                    <i class="fa fa-balance-scale fa-2x fa-pull-left fa-border" aria-hidden="true"></i>
                    @lang('layout.weight')
                </p>
                <hr>
                <div class="row">
                    <div class="col-xs-5">@if( \Gloudemans\Shoppingcart\Facades\Cart::total() < 50 )<span class="label label-info">@lang('layout.minimum')</span>@endif</div>
                    <div class="col-xs-7"><p class="lead text-right">@lang('layout.total') ${{ \Gloudemans\Shoppingcart\Facades\Cart::total() }}</p></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <a href="{{ action('CartController@checkout') }}" class="btn btn-primary form-control">
                                <i class="fa fa-shopping-cart"></i> @lang('layout.checkout')
                            </a>
                        </p>
                    </div>
                </div>

            @endif

            <hr />@include('partials._account')

        </div>
    </div>
</div>

{{--<div class="mobile-cart-footer visible-sm visible-xs">--}}
    {{--<div class="row">--}}
        {{--<div class="col-sm-5 col-xs-5 text-left">--}}
            {{--<a class="btn-cart-xs" id="mobile-cart-trigger">--}}
                {{--@if( \Gloudemans\Shoppingcart\Facades\Cart::count() > 0 )--}}
                    {{--<small><span class="badge cart-total-xs">{{ round(\Gloudemans\Shoppingcart\Facades\Cart::count()) }}</span></small>--}}
                {{--@endif--}}
                {{--<i class="fa fa-shopping-cart"></i>--}}
                {{--${{ \Gloudemans\Shoppingcart\Facades\Cart::total() }}--}}
            {{--</a>--}}
        {{--</div>--}}
        {{--<div class="col-sm-7 col-xs-7 text-right" style="text-transform: uppercase; padding-left: 0; margin-left: 0;">--}}
            {{--@if( \Gloudemans\Shoppingcart\Facades\Cart::total() < 50 )--}}
                {{--<a tabindex="0" role="button" href="#" title="Minimum not met" data-toggle="popover" data-trigger="focus"--}}
                   {{--data-placement="top" data-content="There is a minimum of $50 for online orders.">--}}
                    {{--@lang('layout.checkout') <i class="fa fa-arrow-circle-right"></i></a>--}}
                {{--</a>--}}
            {{--@else--}}
                {{--<a href="/checkout">@lang('layout.checkout') <i class="fa fa-arrow-circle-right"></i></a>--}}
            {{--@endif--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}