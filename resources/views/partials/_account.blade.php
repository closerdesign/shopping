@if( Auth::guest() )
    <div class="row">
        <div class="col-md-3 col-sm-3 hidden-xs">
            <img src="/img/shopping-cart.png" alt="My Account" class="img-responsive">
        </div>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <p class="lead">
                @lang('layout.save-time')
            </p>
        </div>
    </div>
    <p class="text-center">
        <a href="/login" class="btn btn-primary form-control">
            @lang('layout.sign-in') <i class="fa fa-sign-in"></i>
        </a>
    </p>
    <p class="text-center">
        @lang('layout.create')
    </p>
@else
    <p>
        <a href="{{ action('AccountsController@my_account') }}" class="btn btn-primary form-control">
            <i class="fa fa-user"></i> @lang('layout.account')
        </a>
    </p>
    @if( Cart::count() > 0 )
        <p>
            <a href="{{ action('CartController@clear') }}" class="btn btn-primary form-control">
                <i class="fa fa-refresh"></i> @lang('layout.clear-cart')
            </a>
        </p>
    @endif
    <p>
        <a href="/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-primary form-control">
            <i class="fa fa-sign-out"></i> @lang('layout.logout')
        </a>
    </p>
@endif