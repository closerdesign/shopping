<div class="row">
    <div class="col-md-12 text-right">
        <nav>
            <ul class="pagination pagination-sm">
                @if(@$vars['previous'] > 0 )
                    <li>
                        <a href="?@if(isset($_GET['keyword']))keyword={{ $_GET['keyword'] }}&@endif{{ 'page='.$vars['previous']}} " aria-label="Previous"><span aria-hidden="true">@lang('pagination.prev')</span></a>
                    </li>
                @endif

                @if($vars['next'] !='' )
                    <li>
                        <a href="?@if(isset($_GET['keyword']))keyword={{ $_GET['keyword'] }}&@endif{{ 'page='.$vars['next'] }}" aria-label="Previous"><span aria-hidden="true">@lang('pagination.next')</span></a>
                    </li>
                @endif
            </ul>
        </nav>
    </div>
</div>