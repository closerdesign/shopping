@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <p class="lead text-center">
                    <i class="fa fa-envelope"></i> @lang('contact.header')
                </p>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if ( Session::has('message') )
                    <div class="alert alert-{{ Session::get('message')['type'] }}">
                        {{ Session::get('message')['message'] }}
                    </div>
                @endif

                <form method="post" action="{{ action('ShoppingController@contact_form') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="recaptcha" id="recaptcha">
                    <div class="form-group">
                        <label for="first_name">@lang('contact.name')</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                        <input type="text" class="sb-control" name="first_name" value="" >
                    </div>
                    <div class="form-group">
                        <label for="email">@lang('contact.email')</label>
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="phone">@lang('contact.phone')</label>
                        <input type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="contact_preference">@lang('contact.contact')</label>
                        <select class="form-control" name="contact_preference" id="contact_preference">
                            <option value="">Select...</option>
                            <option @if( old('contact_preference') == 'Email') selected @endif value="Email">@lang('contact.email')</option>
                            <option @if( old('contact_preference') == 'Phone') selected @endif value="Phone">@lang('contact.phone')</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="store">@lang('contact.store')</label>
                        <select name="store" id="store" class="form-control" required >
                            <option value="">Select...</option>
                            @if( (URL::to('') == 'http://shopping-bfl.test:8000') || (URL::to('') == 'https://shopping.buyforlessok.com') )
                                <option value="3501">NW Expressway</option>
                            @elseif( (URL::to('') == 'https://shopping.smartsaverok.com') )
                                <option value="1006">Midwest City</option>
                            @elseif( (URL::to('') == 'https://supermercado.buyforlessok.com') )
                                <option value="2701">SW 29th St.</option>
                            @else
                                <option value="1230">Edmond</option>
                                <option value="9515">The Village</option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message">@lang('contact.message')</label>
                        <textarea name="message" id="" cols="30" rows="10" class="form-control">{{ old('message') }}</textarea>
                    </div>

                    <button type="submit" class="btn btn-primary btn-block">
                        <i class="fa fa-paper-plane"></i> @lang('contact.send')
                    </button>
                </form>
            </div>
        </div>
    </div>

@endsection