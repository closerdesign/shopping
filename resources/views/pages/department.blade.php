@extends('layouts.app')

@section('content')

    @include('partials._topbar')

    <div class="container">
        <div class="row">

            @include('partials._cart')

            <div class="col-md-8 col-md-pull-4">

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Department</div>
                            <div class="panel-body">
                                {{--<div class="row">--}}
                                    @foreach($items as $item)
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">

                                        <div class="product-box">

                                            <div class="product-description">

                                                <a href="{{ action('ProductsController@show', $item->id) }}">
                                                    <img src="http://placehold.it/500x500" alt="" class="img-responsive">
                                                </a>

                                                <div class="product-price hidden-xs">
                                                    ${{ $item->regular_price }}
                                                </div>

                                                <div class="product-name">
                                                    <a href="{{ action('ProductsController@show', $item->id) }}">
                                                        {{ $item->description }}<br />
                                                        {{ $item->size }} {{ $item->uom }}
                                                    </a>
                                                    <div class="product-price-xs visible-xs pull-right">
                                                        ${{ $item->regular_price }}
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="product-add">

                                                <form action="{{ action('CartController@store') }}" method="post">
                                                    <div class="input-group">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="{{ $item->sku }}">
                                                        <input type="hidden" name="name" value="{{ $item->description }}">
                                                        <input type="hidden" name="price" value="{{ $item->regular_price }}">
                                                        <input type="number" class="form-control text-center" name="qty" value="1" min="1">
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-danger form-control">
                                                                <i class="fa fa-shopping-cart"></i> Add to Cart
                                                            </button>
                                                        </span>
                                                    </div>
                                                </form>  

                                            </div>                                          

                                        </div>

                                    </div>
                                    @endforeach
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection