@extends('layouts.app')

@section('content')

    @include('partials._topbar')

    <div class="container">
        <div class="row">
            <div class="col-md-2 col-md-offset-5 col-xs-6 col-xs-offset-3">
                <p class="text-center">
                    <img src="/img/complete.png" alt="Thank You" class="img-responsive">
                </p>
            </div>
        </div>
        <p class="lead text-center">
            Thank You, {{ explode(' ',$order->name)[0] }}! Your order has been placed.
        </p>
        @if($order->delivery_fee > 0)
            <p class="text-center">A confirmation email has been sent to {{ $order->email }}. You'll receive another email once your order has been picked up for delivery.</p><hr />
        @else
            <p class="text-center">A confirmation email has been sent to {{ $order->email }}. You'll receive another email once your order has been completed and is ready for pickup.</p><hr />
        @endif
        <p class="text-center">
            <a href="{{ URL::to('/notes/' . $order->id) }}" class="btn btn-primary"><i class="fa fa-sticky-note"></i> Add Note to Order</a>
            <a href="/" class="btn btn-primary"><i class="fa fa-home"></i> Home</a>

        </p>
    </div>
@endsection
