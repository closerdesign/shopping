@extends('layouts.app')

@section('content')

    <div class="container main-container">
        <div class="col-md-6 col-md-offset-3">
            <p class="lead text-center">
                <b>@lang('home.skip')</b>
            </p>
            <p class="lead text-center">
                @lang('home.order')
            </p>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <noscript>
                    <div class="alert alert-danger">
                        It looks like Javascript is not enabled in your browser. For full functionality of this site,
                        Javascript is required. Please enable it in order to continue.
                    </div>
                </noscript>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-9 col-sm-9">
                                        <p class="lead hidden-xs">
                                            &nbsp;
                                        </p>
                                        <p class="lead">
                                            @lang('store-selector.location')<br />
                                        </p>
                                    </div>
                                    <div class="col-md-3 col-sm-3 hidden-xs form-group">
                                        <img src="/img/store.png" alt="Please Select a Store" class="img-responsive">
                                    </div>
                                </div>
                                <form action="{{ action('StoresController@store_select') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group form-group-lg">
                                        <select name="store" id="store" class="form-control">
                                            <option value="">Select...</option>
                                            {{--Buy For Less Locations--}}
                                            @if( (URL::to('') == 'http://shopping-bfl.test:8000') || (URL::to('') == 'https://shopping.buyforlessok.com') )
                                                <option value="3501">NW Expressway</option>
                                                {{--<option value="7957">NW 23rd &amp; Council</option>--}}
                                            {{--SuperMercado Locations--}}
                                            @elseif( (URL::to('') == 'http://shopping-sm.test:8000') || (URL::to('') == 'https://supermercado.buyforlessok.com') )
                                                <option value="2701">SW 29th &amp; May</option>
                                                {{--<option value="3701">NW 36th &amp; MacArthur</option>--}}
                                                {{--<option value="3713">SW 36th &amp; Western</option>--}}
                                                {{--<option value="4150">SW 59th &amp; Walker</option>--}}
                                            {{--Smart Saver Locations--}}
                                            @elseif( (URL::to('') == 'http://shopping-ss.test:8000') || (URL::to('') == 'https://shopping.smartsaverok.com') )
                                                <option value="1006">Midwest City</option>
                                                {{--<option value="1205">Norman</option>--}}
                                                {{--<option value="1201">Yukon</option>--}}
                                                {{--<option value="2001">NE 23rd &amp; MLK</option>--}}
                                                {{--<option value="4424">SE 44th &amp; High</option>--}}
                                            {{--Uptown Grocery Locations--}}
                                            @else
                                                <option value="1230">Edmond</option>
                                                <option value="9515">The Village</option>
                                                {{--<option value="1124">1124 NE 36th Street</option>--}}
                                            @endif
                                        </select>
                                    </div>
                                    <div class="clearfix">
                                        @if( (URL::to('') == 'http://shopping.test:8000') || (URL::to('') == 'https://shopping.uptowngroceryco.com') )
                                            <p class="text-muted pull-left"><small>@lang('store-selector.delivery')</small></p>
                                        @endif
                                        <button class="btn btn-primary form-control" type="submit">
                                            @lang('store-selector.continue') <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p class="visible-xs visible-sm text-center" style="margin-top: -10px;">
            <a href="#" data-toggle="modal" data-target="#howItWorks">@lang('store-selector.howitworks')</a>
        </p>
    </div>

    @endsection