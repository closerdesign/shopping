@extends('layouts.app')

@section('content')

    @include('partials._topbar')

    <div class="container">
        <div class="row">

            <div id="cart-container">
                @include('partials._cart')
            </div>

            @if( (URL::to('') == 'http://shopping-bfl.test:8000') || (URL::to('') == 'https://shopping.buyforlessok.com') )
                <?php $brand = 'Buy For Less' ?>
            @elseif( (URL::to('') == 'http://shopping-sm.test:8000') || (URL::to('') == 'https://supermercado.buyforlessok.com') )
                <?php $brand = 'SuperMercado' ?>
            @elseif( (URL::to('') == 'http://shopping-ss.test:8000') || (URL::to('') == 'https://shopping.smartsaverok.com') )
                <?php $brand = 'Smart Saver' ?>
            @else
                <?php $brand = 'Uptown Grocery Co' ?>
            @endif

            <div class="col-md-8 col-md-pull-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Terms of Use</div>
                    <div class="panel-body" style="font-size: .8em">

                        <p>{{ $brand }} is pleased to offer the convenience of 24-hour online grocery shopping via a
                            secure internet ordering system. Our easy to navigate web site allows you to enjoy the convenience
                            of shopping from home or office and save precious time. Your order will be selected at {{ $brand }},
                            where a trained personal shopper will shop for you. We are honored to shop for you and your family
                            and look forward to meeting you soon!
                        </p>

                        <h4>What days and times are available to shop?</h4>
                        <p>You may place a order at any time. However, orders are available to be pick up between the hours
                            of 10am to 7:30pm, 7 days a week. There is a limit to the number of orders that are allowed per
                            each pick up time slot. Once a time slot is full, you will no longer be able to select it during
                            checkout. We recommend reserving a convenient time at the beginning of your shopping experience.
                        </p>

                        <h4>Is there a minimum order?</h4>
                        <p>The online shopping service at  {{ $brand }} is FREE, with a minimum order of $30.</p>

                        <h4>Will I receive a confirmation email?</h4>
                        <p>Once you place your order we will send you an email confirming your order. If you do not receive
                            the email confirmation please contact your store directly to ensure that your order was processed correctly.
                        </p>
                        <p>Once your order has been completed, you will also receive an email with your final total and
                            pick-up instructions.
                        </p>

                        <h4>What payment options do you offer?</h4>
                        <p>{{ $brand }} orders are processed via Credit Card and PayPal internet payment service.</p>

                        <h4>Can I give my shopper special instructions about my order?</h4>
                        <p>There are 2 places where you can enter special instructions for your order. For example, you can
                            specify that you would like "Green Bananas" in the notes on your bananas. You can also specify
                            substitution instructions for specific products, for example if you want to allow for substitutions
                            on all products except, for example, the milk you have selected; you can uncheck that “allow substitutions”
                            button on milk.</p>
                        <p>
                            <ul>
                                <li>After placing each item in the shopping cart, you can add specific comments for each product.
                                    Click on the comment button and you will be given a pop up box to enter instructions for your shopper.
                                </li>
                                <li>During the checkout process you will be given the opportunity, by clicking 'Add comment',
                                    or by editing an existing comment to add any further special instructions or comments for this order.</li>
                            </ul>
                        </p>

                        <h4>Can I use coupons on my order?</h4>
                        <p>Unfortunately no. We currently do not accept coupons when online shopping!
                        </p>

                        <h4>What if I cannot find a product on the web site?</h4>
                        <p>Our online store is always growing. If you cannot find an item but you would like to add it to
                            today's order, click on the ‘Add Item’ button in your cart. You will be given a popup form to
                            enter those products you could not find on the site. Please enter as much information as possible
                            about the product and your best price estimate, and if the store carries the item it will be added to your order.
                        </p>

                        <h4>Can I add items to my order after it has already been placed?</h4>
                        <p>Yes! You can add items to your order until your order has started being picked by your shopper.</p>
                        <p>To add an item:
                            <ul>
                                <li>View your confirmation email</li>
                                <li>Click "Add Note To Order"</li>
                                <li>Let your shopper know an item that you let off, or any additional instructions you might have</li>
                            </ul>
                            Your Shopper will be notified that you have updated your order!
                        </p>

                        <h4>What if the product I order is not available? What is your substitution policy?</h4>
                        <p>In the checkout process we will ask you if you would like to allow for substitutions, your substitution
                            and contact methods preferences. Our goal is to never be out of stock on any item. When you place
                            your order, you will be able to select your Substitution Preference. Your shopper will contact
                            you with substitutions for items that are out of stock in store. If you do not wish to be contact
                            or you have no preference in substitution, please select the appropriate preference.</p>

                        <h4>How do I know my produce, dairy, meat and deli items will be fresh?</h4>
                        <p>Not all perishable produce is the same. How it is grown, picked, processed and handled makes a
                            big difference in how it tastes and how long it lasts. At {{ $brand }}, we start by buying
                            only premium quality perishable products. Then we ship it the fastest way possible and monitor
                            the temperature through each step of the process. This means you will always have the highest
                            quality and freshest products.</p>

                        <h4>Can I order alcohol?</h4>
                        <p>Orders that include alcohol beverages will be delivered to an adult over 21 years old. An adult
                            MUST be present at the time of pickup or delivery- or the alcohol products will be removed from
                            the order. It's the law.
                        </p>

                        <h4>What am I restricted from ordering online?</h4>
                        <p>At this time we cannot deliver tobacco, prescription medication, propane tanks, or lottery cards
                            because of legal restrictions.
                        </p>

                        <h4>Can I tip my shopper?</h4>
                        <p>It is our pleasure to provide this service to you, our valued guest. Providing this service is
                            the primary responsibility of every {{ $brand }} personal shopper. Therefore, our employees
                            do not accept tips. Your return business, recommendation to friends and family, and thanks are
                            the best tip we can receive.</p>

                        <h4>Why was I charged a different amount than my original total?</h4>
                        <p>The final charged amount will always reflect the cost of the items that you receive. There are
                            several reasons that this charge could be more or less than the original total at checkout. </p>
                        <p>
                            <ul>
                                <li>Special Requests -  we won't know the cost of these items until the shopper finds them
                                    in the store</li>
                                <li>Out of stock items - items that are out of stock in the store will be removed from the
                                    order, reducing the final charge.</li>
                                <li>Substitutions - if the item you requested is out of stock and an adequate substitution
                                    is selected by the shopper, you will be be charged for the price of the substituted item
                                </li>
                                <li>Added Items - items added to your cart after checkout will not be reflected in the original total</li>
                                <li>Weighted Items- For produce, deli, seafood and meat items that are priced by the pound,
                                    your shopper will select the items that most closely match the weight you wish to purchase.
                                    You will be billed only for the actual amount of product selected. Consequently, the total
                                    dollar amount of your order may vary slightly.</li>
                            </ul>
                        </p>
                        <p>If the order exceeds your initial authorized charge, you may have to provide additional payment
                            to your shopper at the time of pick up.</p>

                        <h4>There are damaged items in my order. What do I do?</h4>
                        <p>Oh no! We apologize for any inconvenience that this may have caused. If there is any issue with
                            your order, please contact us at onlineshopping@buyforlessok.com</p>

                        <h4>Could mother-nature affect my pick-up time?</h4>
                        <p>It is our hope that ‘Mother Nature’ will never prevent you from picking up your groceries in a
                            timely manner. Please let us know if you will be delayed in picking up your order. Furthermore,
                            we will have your personal shopper contact you in the event that your order will be delayed due
                            to weather or power outages.
                        </p>

                        <h4>Privacy Policy</h4>
                        <p>Our guests' privacy is of the utmost importance to {{ $brand }}. We do not sell, trade, or
                            rent personal information about individual members (such as name, address, email address) to
                            third parties. During registration and checkout, a user is required to provide their contact
                            information. This information is used to contact the user about the services on our site for
                            which they have expressed interest and to contact the user if we have trouble processing an order.
                            We value our guests and we take seriously their privacy concerns.</p>

                        <h4>User Agreement</h4>
                        <p>Before you use this website, you must read and agree to the terms of this User Agreement. If you
                            do not agree to be legally bound by the terms of this User Agreement, do not use or access this
                            website and do not use any of our services. Your use of this website constitutes your agreement
                            to this User Agreement.</p>
                        <p>You agree to use this website only for the purpose of online shopping. Much of the data, images
                            and other information on this site is licensed from third parties. You agree that you will not
                            copy, reproduce, modify, create derivative works from, or publicly display (on a website or elsewhere)
                            any content from our website, except with the prior written permission of {{ $brand }}.
                            You also agree that you will not use any automatic device to monitor or copy any of our web pages
                            or the content contained in our web pages, nor will you take any action that imposes an abnormal
                            or unreasonable burden on our hardware and software infrastructure.</p>

                    </div>
                </div>
            </div>
        </div>
    </div><br /><br />

@endsection