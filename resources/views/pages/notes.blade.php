@extends('layouts.app')

@section('content')

    <div class="container">

        <p class="lead text-center">
            <i class="fa fa-sticky-note"></i> @lang('notes.addtoorder')
        </p>

        <div class="row">
            <div class="col-md-6 col-md-offset-3">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if ( Session::has('message') )
                    <div class="alert alert-{{ Session::get('message')['type'] }}">
                        {{ Session::get('message')['message'] }}
                    </div>
                @endif

                <form method="post" action="{{ action('ShoppingController@notes_form', $order->id) }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="recaptcha" id="recaptcha">
                    <div class="form-group">
                        <label for="message">@lang('notes.message')</label>
                        <textarea name="message" id="" cols="30" rows="10" class="form-control">{{ old('message') }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">
                        <i class="fa fa-paper-plane"></i> @lang('notes.send')
                    </button>
                </form>
            </div>
        </div>
    </div>

@endsection