@extends('layouts.app')

@section('content')

{{--    @include('partials._topbar')--}}

{{--    <div class="container" id="checkout">--}}

{{--        <div class="row">--}}
{{--            <div class="col-md-6 col-md-offset-3">--}}
{{--                <p class="lead text-center">--}}
{{--                    <i class="fa fa-shopping-cart"></i> @lang('checkout.checkout')--}}
{{--                </p>--}}

{{--                @foreach( \Gloudemans\Shoppingcart\Facades\Cart::content()->sortBy('name') as $product )--}}
{{--                    <cart-item-component :item="{{ json_encode($product) }}"></cart-item-component>--}}
{{--                @endforeach--}}

{{--                <p class="lead text-right">Subtotal: <span v-text="subtotal"></span></p>--}}

{{--                <div class="form-group"></div>--}}

{{--                <div class="alert alert-warning text-center">--}}
{{--                    <i class="fa fa-balance-scale" aria-hidden="true"></i>--}}
{{--                    @lang('checkout.weighted')--}}
{{--                </div>--}}

{{--                <div class="alert alert-info text-center">--}}
{{--                    @lang('checkout.additional-hold')--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--    </div>--}}

{{--    <p v-show="subtotal < 50" class="lead text-center">--}}
{{--        $50 minimum order not met yet.--}}
{{--    </p>--}}

{{--    <div v-show="subtotal >= 50">--}}

{{--        <div class="container">--}}

{{--            <div class="row">--}}
{{--                <div class="col-md-6 col-md-offset-3">--}}

{{--                    @if( !session()->has('pickup_date') )--}}
{{--                    <form action="{{ action('ShoppingController@pickup_settings') }}" method="post" class="form-group">--}}
{{--                        {{ csrf_field() }}--}}
{{--                        <div id="pickup_datetime">--}}
{{--                            <div class="form-group">--}}
{{--                                <label for="pickup_date">@lang('modals.date')</label>--}}
{{--                                <select name="pickup_date" id="pickup_date_modal" class="form-control" >--}}
{{--                                    <option value="">@lang('general.select')</option>--}}
{{--                                    @foreach(range(0,6) as $i)--}}
{{--                                        <option value="{{ date('Y-m-d', strtotime('+' . $i . ' days')) }}">{{ date('l, F d-Y', strtotime('+' . $i . ' days')) }}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                            <div class="form-group">--}}
{{--                                <label for="pickup_time">@lang('modals.time')</label>--}}
{{--                                <select class="form-control" name="pickup_time" id="pickup_time_modal">--}}

{{--                                </select>--}}
{{--                            </div>--}}
{{--                            <div class="form-group" id="pickup-loading" style="display:none">--}}
{{--                                <img src="/img/loading.gif" height="10" /> Loading available times, please wait...--}}
{{--                            </div>--}}
{{--                            <button class="btn btn-primary btn-block" type="submit">--}}
{{--                                <i class="fa fa-check"></i> @lang('modals.save')--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                    @endif--}}

{{--                </div>--}}
{{--            </div>--}}

{{--            @if( session()->has('pickup_date') )--}}
{{--            <div class="row" v-if="subtotal >= 50">--}}
{{--                <div class="col-md-6 col-md-offset-3">--}}

{{--                    @if( session()->has('pickup_date') )--}}
{{--                        <ul class="list-group">--}}
{{--                            <li class="list-group-item text-center">--}}
{{--                                <b>Pickup Date:</b>--}}
{{--                                <br />{{ date('l, F d, Y', strtotime(session()->get('pickup_date'))) }}--}}
{{--                            </li>--}}
{{--                            <li class="list-group-item text-center">--}}
{{--                                <b>Pickup Time:</b>--}}
{{--                                <br />{{ date('h:i A', strtotime(session()->get('pickup_time'))) }}--}}
{{--                            </li>--}}
{{--                            <li class="list-group-item text-center">--}}
{{--                                <a class="btn-xs btn-danger" href="{{ action('ShoppingController@pickup_reset') }}">--}}
{{--                                    Change--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    @endif--}}

{{--                    <credit-card-component :stripekey="'{{ env(session()->get('store') . '_STRIPE_KEY') }}'"></credit-card-component>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--            @endif--}}

{{--        </div>--}}
{{--    </div>--}}

@endsection