@extends('layouts.app')

@section('content')

    @include('partials._topbar')

    <div class="container">
        <div class="row">

            <div id="cart-container">
                @include('partials._cart')
            </div>

            <div class="col-md-8 col-md-pull-4">
                <form method="post" action="{{ action('ShoppingController@feedback_form') }}">
                    {{ csrf_field() }}
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-sticky-note"></i> @lang('contact.feedback')
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="first_name">@lang('contact.name')</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                                <input type="text" class="sb-control" name="first_name" value="" >
                            </div>
                            <div class="form-group">
                                <label for="email">@lang('contact.email')</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="experience">@lang('contact.experience')</label>
                                <select class="form-control" name="experience" id="experience">
                                    <option value="">Select...</option>
                                    <option @if( old('experience') == 'Excellent') selected @endif value="Excellent">@lang('contact.excellent')</option>
                                    <option @if( old('experience') == 'Good') selected @endif value="Good">@lang('contact.good')</option>
                                    <option @if( old('experience') == 'Fair') selected @endif value="Fair">@lang('contact.fair')</option>
                                    <option @if( old('experience') == 'Poor') selected @endif value="Poor">@lang('contact.poor')</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="message">@lang('contact.suggestions')</label>
                                <textarea name="message" id="" cols="30" rows="10" class="form-control">{{ old('message') }}</textarea>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group" style="overflow: hidden;">
                                        {!! Recaptcha::render() !!}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-lg btn-primary">
                                            <i class="fa fa-paper-plane"></i> @lang('contact.send')
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><br /><br />

@endsection