@extends('layouts.app')

@section('content')

    @include('partials._topbar')

    <div class="container">

        <p class="lead text-center">
            <i class="fa fa-shopping-basket"></i> My Favorites
            <small>- @lang('category.showing') {{ $products->products->from }} @lang('category.to') {{ $products->products->to }}
                @lang('category.of') {{ $products->products->total }} @lang('category.total')</small>
        </p>

        <div class="row row-no-gutters">
            @foreach($products->products->data as $product) <product-component :user="{{ json_encode(auth()->user()) }}" :product="{{ json_encode($product) }}"></product-component> @endforeach
        </div>

        @include('partials._pagination')

    </div>

@endsection