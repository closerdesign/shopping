@extends('layouts.admin')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-md-9 col-sm-9">
                <p class="lead">
                    Order # {{ $order->id }}
                </p>
            </div>
            <div class="col-xs-4 col-md-3 col-sm-3 text-right">
                <div class="dropdown">
                    <a href="#" class="btn btn-primary" type="button" data-toggle="dropdown">
                        Action <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="{{ action('OrdersController@summary', [1, $order->id]) }}" target="_blank">
                                <i class="fa fa-file-pdf-o"></i> Order Summary
                            </a>
                        </li>
                        <li>
                            <a href="{{ action('OrdersController@summary', [0, $order->id]) }}" target="_blank">
                                <i class="fa fa-file-pdf-o"></i> Order Summary [No Images]
                            </a>
                        </li>
                        <li>
                            <a href="{{ action('OrdersController@packaging', $order->id) }}" target="_blank">
                                <i class="fa fa-file-pdf-o"></i> Packaging Info
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        @if( $order->is_active == true )
        <div class="row">
            <div class="col-md-12 form-group">

                    @if( $order->isActive && $order->status !== 'POS' )
                    <a class="btn-lg btn-danger btn-block" href="{{ route('picking-order', $order->id) }}">
                        Start Picking Items
                    </a>
                    @endif

                    @if( $order->status == 'POS' )
                    <form onsubmit="return confirm('Are you sure?')" action="{{ action('CartController@payment_do', $order->id) }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="status" value="completed_with_paypal">
                        <div class="form-group">
                            <label for="final_amount text-center">Final Amount</label>
                            <input type="number" class="form-control input-lg text-center" name="final_amount" step=".01" required >
                        </div>
                        <button class="btn-lg btn-danger btn-block text-center">
                            Charge Customer
                        </button>
                    </form>
                    @endif

            </div>
        </div>
        @endif

        @if( $order->status == 'COMPLETED' )

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-edit"></i> Guest Signature</div>
                <div class="panel panel-body">

                    @if( $order->signature == '' )

                        <p>Please sign below:</p>

                        <div class='js-signature'></div>

                        <p>
                            <button class="btn btn-primary" id="clearBtn" onclick="clearCanvas();">Clear</button>
                            <button class="btn btn-primary" id="saveBtn" onclick="saveSignature();" disabled>Save Signature</button>
                        </p>

                    @else
                        <img src="{{ $order->signature }}" class="img-responsive" />
                    @endif

                    <div id="sig-error"></div>

                </div>
            </div>

        @endif

        @if($order->is_complex == 1)
            <div class="alert alert-info">
                <p class="text-center"><i class="fa fa-exclamation-triangle"></i> Order for delivery to: <b>{{ $order->complex }}</b></p>
            </div>
        @endif

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <div class="row">
                                <div class="col-md-6 text-center">
                                    <i class="fa fa-user"></i> {{ $order->name }}
                                </div>
                                <div class="col-md-6 text-center">
                                    <i class="fa fa-building-o"></i> {{ $order->store }} &nbsp;
                                    <i class="fa fa-calendar"></i> {{ date('F d, Y', strtotime($order->pickup_date)) }} &nbsp;
                                    <i class="fa fa-clock-o"></i> {{ date('g:i A', strtotime($order->pickup_time)) }}
                                </div>
                            </div>
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-xs-6">
                                <dl>
                                    <dt>Guest Info</dt>
                                    <dd>
                                        <small>
                                            <i class="fa fa-map-marker"></i> {{ $order->billing_address }} &nbsp;{{ $order->zip_code }}<br />
                                            <?php $phone = "(".substr($order->phone, 0, 3).") ".substr($order->phone, 3, 3)."-".substr($order->phone,6); ?>
                                            <i class="fa fa-phone"></i> {{ $phone }}<br />
                                            <i class="fa fa-envelope"></i> <a href="mailto:{{ $order->email }}">{{ $order->email }}</a>
                                            @if( Auth::user()->isAdmin())
                                                <br /><i class="fa fa-list"></i> <a onclick="$('#guest-orders').submit()" style="cursor: pointer">View Guest Orders</a>
                                                <form id="guest-orders" action="{{action('AdminController@customer_orders')}}" method="post">
                                                    {{ csrf_field() }}
                                                    <input name="email" type="hidden" value="{{$order->email}}">
                                                </form>
                                            @endif
                                        </small>
                                    </dd>
                                </dl>
                            </div>
                            <div class="col-xs-6">
                                <dl>
                                    <dt>Total</dt>
                                    <dd>
                                        ${{ $order->total_order }}&nbsp;
                                        @if( $order->payment_method == 'Stripe' )
                                            <i class="fa fa-cc-stripe"></i><br />
                                        @else
                                            <i class="fa fa-paypal"></i><br />
                                        @endif
                                        @if($order->status == 'COMPLETED' || $order->status == 'DELIVERED')
                                            <small><b>Final: ${{ $order->final_amount }}</b></small>
                                        @endif
                                    </dd>
                                    <dd><span class="label label-info">{{ $order->status }}</span></dd>
                                </dl>
                            </div>
                            <div class="col-xs-12 text-center">
                                <dl>
                                    <dt>Substitutions</dt>
                                    <dd>{{ $order->substitutions_preference }}</dd>
                                    <dt>Tax Exempt</dt>
                                    @if($order->tax_exempt == '1')
                                        <dd>Yes: <span class="text-red">{{ $order->tax_number }}</span></dd>
                                    @else
                                        <dd>No</dd>
                                    @endif
                                    @if(!empty($order->loyalty_number))
                                        <dt>Loyalty Number</dt>
                                        <dd><span class="text-red">{{ $order->loyalty_number }}</span></dd>
                                    @endif
                                    <dt>Payment Transaction ID</dt>
                                    <dd>{{ $order->paypal_token }}</dd>
                                </dl>
                                <div class="dropdown">
                                    <a href="#" class="btn btn-block btn-primary" data-toggle="dropdown">
                                        Send Notification <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <a href="{{ action('OrdersController@call_notification', $order->id) }}"
                                               onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-fw fa-phone"></i> I Called You
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ action('OrdersController@text_notification', $order->id) }}"
                                               onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-fw fa-mobile-phone"></i> I Texted You
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ action('OrdersController@over_total', $order->id) }}"
                                               onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-fw fa-money"></i> Order Is Over Authorized Total
                                            </a>
                                        </li>
                                        @if( Auth::user()->store == '' )
                                            <li>
                                                <a href="{{ action('OrdersController@guest_check', $order->id) }}"
                                                   onclick="return confirm('Are you sure?')">
                                                    <i class="fa fa-fw fa-frown-o"></i> Did Something Go Wrong?
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <dt class="border-top">Comments</dt>
                                @if($order->comments != "")
                                    <dd>{!! $order->comments !!}</dd>
                                @endif
                                @if( $order->score_comments != null )
                                    <p><b>Score Comments:</b> {{ $order->score_comments }}</p>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <dt class="border-top">Notes</dt>
                                @if(count($order->notes) > 0)
                                    @foreach($order->notes as $note)
                                        <dd>{{ $note->note }}</dd>
                                    @endforeach
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Cancel this order
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        @if( $order->is_active )
                            <form action="{{ action('OrdersController@cancel_order', $order->id) }}" method="post" onsubmit="return confirm('Are you sure?')">
                                @csrf
                                <div class="form-group" id="reason_for_cancellation_block">
                                    <label for="cancel_reason">Reason For Cancellation</label><br />
                                    <small><b>Note:</b> Guest will receive a copy of this message in cancellation confirmation email.</small>
                                    <textarea name="cancel_reason" id="" cols="30" rows="10" class="form-control" >{{ old('cancel_reason') }}</textarea>
                                </div>
                                <button class="btn btn-danger btn-block" type="submit">
                                    Cancel Order
                                </button>
                            </form>
                            @else
                            <p>This order can't be cancelled in its current status.</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Collect payment in store
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        <form action="{{ route('pay-in-store', $order->id) }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="final_amount">Final Amount</label>
                                <input type="text" class="form-control" name="final_amount" step="0.01" min="0.01" required >
                            </div>
                            <button class="btn btn-success">
                                Complete Order
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection

@section('js')
    <script>
        $(document).ready(function(){

            $('#status').change(function(){
                $('.btn-order-status').hide();
                var order_status = $(this).val();
                $('#final_amount_block').hide();
                $('#reason_for_cancellation_block').hide();
                if( (order_status == 'completed_with_paypal') || (order_status == 'completed_with_in_store_payment_method') || (order_status == 'completed_with_paypal_here')  )
                {
                    $('#final_amount_block').show();
                    $('.btn-order-status').show();
                    $('#btn_in_progress_block').hide();
                }
                if(order_status == 'cancelled')
                {
                    $('#reason_for_cancellation_block').show();
                    $('.btn-order-status').show();
                }

                if (order_status == 'in_progress'){
                    $('#btn_in_progress_block').show();
                }
            });

            $('.check-row').click(function() {
                $(this).toggleClass('btn-success').closest('tr').toggleClass('success');

                var id = $(this).attr('item-id');

                $.ajax( {
                    type : 'POST',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    url  : 'picked/' + id,
                    data : id,
                    success: function(){

                    },
                    error: function(){
                        Notify('There was an issue saving picked status. Please refresh and try again.', null, null, 'danger');
                    }
                });

            });

            $(document).on('click', '.item-comment', function(){
               var id = $(this).attr('item-id');
               var rowId = '#' + id + '-comments';
               $(rowId).fadeToggle();
            });

            $(document).on('focusout', '.picker-comment', function() {
                var item = $(this).attr('item-id');
                var comment = encodeURIComponent($(this).val());
                comment = comment.trim();
                if(comment !== "")
                {
                    $.ajax( {
                        type : 'POST',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        url  : 'picker-comment/' + item + '/' + comment,
                        data : comment,
                        success: function(){
                            Notify('Comment Saved!', null, null, 'success');
                        },
                        error: function(){
                            Notify('Comment could not be saved! Please refresh and try again.', null, null, 'danger');
                        }
                    });
                }
            });

            $('.img-popup-link').magnificPopup({
                type: 'image'
            });

            $('.table-responsive').on('show.bs.dropdown', function () {
                $('.table-responsive').css( "overflow", "inherit" );
            }).on('hide.bs.dropdown', function () {
                $('.table-responsive').css( "overflow", "auto" );
            });

            $('.js-signature').jqSignature({height: 250, autoFit: true, lineWidth: 3});

            $('.js-signature').on('jq.signature.changed', function() {
                $('#saveBtn').attr('disabled', false);
            });

            $('#gi-toggle').on('click', function() {
                $('#guest-info').slideToggle();
                $(this).find('i').toggleClass('fa-chevron-up fa-chevron-down');
            });
        });


        function clearCanvas() {
            $('.js-signature').jqSignature('clearCanvas');
            $('#saveBtn').attr('disabled', true);
        }

        function saveSignature() {
            var dataUrl = $('.js-signature').jqSignature('getDataURL');
            var img = $('<img>').attr('src', dataUrl);

            $.ajax( {
                type : 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url  : 'signature/' + {{ $order->id }},
                data : {data:dataUrl},
                success: function(){
                    $(document).scrollTop(0);
                    window.location = '/admin';
                },
                error: function(){
                    $('#sig-error').empty().append($('<p class="text-red">').text("There was an issue saving signature. Please try again."));
                }

            });
        }

    </script>
@endsection