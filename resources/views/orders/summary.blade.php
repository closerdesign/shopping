<html>
<head>
    <tite>Summary for Order No. {{ $order->id }}</tite>
    <style>
        body{
            font-family: Helvetica;
            font-size: 9pt;
        }
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>
<body>

    <p style="text-align: center">
        <b>ORDER DETAILS</b>
    </p>

    <hr>

    @if($order->is_complex == 1)
        <p style="text-align: center; color: red; text-transform: uppercase; border: solid 1px red; font-weight: bold; padding: 5pt;">
            Order for Delivery to: {{ $order->complex }}
        </p>
    @endif

    @if($order->tax_exempt == 1)
        <p style="text-align: center; color: red; text-transform: uppercase; border: solid 1px red; font-weight: bold; padding: 5pt;">
            Order is tax exempt, {{ $order->tax_number }}
        </p>
    @endif

    @if($count < 2)
        <p style="text-align: center; color: red; text-transform: uppercase; border: solid 1px red; font-weight: bold; padding: 5pt;">
            First order for this guest
        </p>
    @endif

    @if( $order->delivery_fee > 0 )
        <p style="text-align: center; background-color: black; color: white; text-transform: uppercase; font-weight: bold; padding: 5pt;">
            Order is for Shipt Delivery
        </p>
    @endif

    @if( !empty($order->loyalty_number) )
        <p style="text-align: center; color: red; text-transform: uppercase; border: solid 1px red; font-weight: bold; padding: 5pt;">
            <i class="fa fa-star"></i> Guest Loyalty Number: {{ $order->loyalty_number }}
        </p>
    @endif

    <table width="100%" cellpadding="5">
        <tr>
            <td width="25%"><b>Pickup Date</b></td>
            <td width="25%">{{ date('F d, Y', strtotime($order->pickup_date)) }}</td>
            <td width="25%"><b>Pickup Time</b></td>
            <td width="25%">{{ date('g:i A', strtotime($order->pickup_time)) }}</td>
        </tr>
        <tr>
            <td><b>Name</b></td>
            <td>{{ $order->name }}</td>
            <td><b>Phone</b></td>
            <td>{{ $order->phone }}</td>
        </tr>
        <tr>
            <td><b>Email</b></td>
            <td>{{ $order->email }}</td>
            <td><b>Payment Method</b></td>
            <td>{{ $order->payment_method }}</td>
        </tr>
        <tr>
            <td><b>Total Order</b></td>
            <td>{{ number_format($order->total_order, 2) }}</td>
            <td><b>Status</b></td>
            <td>{{ $order->status }}</td>
        </tr>
        <tr>
            <td><b>Store</b></td>
            <td>{{ $order->store }}</td>
            <td><b>Substitutions Preference</b></td>
            <td>{{ $order->substitutions_preference }}</td>
        </tr>
        <tr>
            <td colspan="4"><b>Comments</b></td>
        </tr>
        <tr>
            <td colspan="4">
                {!! $order->comments !!}
            </td>
        </tr>
        @if(count($order->notes) > 0)
            <tr>
                <td colspan="4" class="text-left" style="color: red;"><b>NOTES</b></td>
            </tr>
            @foreach($order->notes as $note)
                <tr>
                    <td colspan="4">{{ $note->note }}</td>
                </tr>
            @endforeach
        @endif

    </table>

    <hr>

    <p style="text-align: center">
        <b>ITEMS LIST</b>
    </p>

    <hr>

    <p>
        If an item is out of stock, please find an appropriate substitution suggestion for the guest.
        This would preferably be <b>same product, different brand</b>. However, please use your best judgement
        (Pay attention to gluten free and organic items). If the item is continually out of stock, please
        notate in your admin page by <b>reporting an item</b> that we do not carry.
    </p>

    <table width="100%" cellpadding="5">
        <tr>
            <th width="5%">
                &nbsp;
            </th>
            <th width="10%" style="text-align: center">
                UPC
            </th>
            <th width="10%" style="text-align: center">
                Qty
            </th>
            <th width="50%" style="text-align: center">
                Name
            </th>
            <th width="10%" style="text-align: center">
                Price
            </th>
            <th width="15%" style="text-align: center">
                Subtotal
            </th>
        </tr>

        @foreach($order->items->groupBy('dept_code') as $group)

            <tr>
                <th colspan="6" style="text-align: center; font-weight: bold">
                    {{ $departments[strval($group->first()->dept_code)] ?? '' }}
                    @if( $departments[strval($group->first()->dept_code)] == 'BEER' || $departments[strval($group->first()->dept_code)] == 'WINE' )
                        <span style="color: red;"> <b>[ID CHECK REQUIRED!]</b></span>
                    @elseif( $departments[strval($group->first()->dept_code)] == 'UNDEFINED' )
                        <span style="color: red;"> <b>[Check ID if order contains alcohol]</b></span>
                    @endif
                </th>
            </tr>

            @foreach($group as $item)
                <tr>
                    <td>&nbsp;</td>
                    <td style="text-align: center">
                        @if($images == 1)
                            @if($item->image != "") <img style="margin: 5pt;" src="{{ urldecode($item->image) }}" width="50"> @endif
                        @endif
                        {{ $item->upc }}
                    </td>
                    <td style="text-align:center">{{ $item->qty }}</td>
                    <td>
                        <b>{{ $item->name }}</b>
                        @if($item->allow_substitutions == 'Yes')<br />Substitutions are allowed.@endif
                        @if($item->comments != "")
                            <p><b>Comments:</b><br />{{ $item->comments }}</p>
                        @endif
                        @if( $item->dept_code == 999 )
                            <br /><b style="color: red;">YOU WILL NEED TO RING THIS ITEM AT THE POS LEVEL.</b>
                        @endif
                    </td>
                    <td style="text-align:right">{{ number_format($item->price, 2) }}</td>
                    <td style="text-align:right">{{ number_format( ($item->price*$item->qty) , 2) }}</td>
                </tr>
            @endforeach

            @endforeach
    </table>

</body>
</html>