@component('mail::message')
# Tell us how we did with your Online Shopping order from:<br />{{ $order->store->brands->name }} {{ $order->store->name }}...

Your feedback is valuable to us, and we appreciate your willingness to take this survey.

@component('mail::button', ['url' => $order->store->brands->online_shopping_website . '/order/score/' . $order->id . '/4'])
Great
@endcomponent

@component('mail::button', ['url' => $order->store->brands->online_shopping_website . '/order/score/' . $order->id . '/3'])
Good
@endcomponent

@component('mail::button', ['url' => $order->store->brands->online_shopping_website . '/order/score/' . $order->id . '/2'])
Okay
@endcomponent

@component('mail::button', ['url' => $order->store->brands->online_shopping_website . '/order/score/' . $order->id . '/1'])
Bad
@endcomponent

@endcomponent
