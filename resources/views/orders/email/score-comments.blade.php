@component('mail::message')
# Feedback for our Online Shopping Order #{{ $order->id }}

{{ $order->score_comments }}

@component('mail::button', ['url' => route('order.show', $order->id)])
Click here to check Order Details
@endcomponent

@endcomponent
