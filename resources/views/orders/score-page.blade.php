@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="lead text-center">Thank you for your feedback {{ explode(" ", $order->name)[0] }}!</p>

        @if( $order->score_comments == null )

        <p class="text-center">
            Would you like to add any additional comments?
        </p>

        <form action="{{ route('order.score-comments', $order->id) }}" method="post">
            @csrf
            <div class="form-group">
                <textarea class="form-control" name="score_comments" id="score-comments" cols="30" rows="10">{{ old('score_comments') }}</textarea>
            </div>
            <button class="btn btn-lg btn-success btn-block">
                Send
            </button>
        </form>

        @endif
    </div>

    @endsection