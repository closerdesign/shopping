<html>
<head>
    <style>
        body{
            font-family: Helvetica, sans-serif;
            margin: -35px;
        }
        .order {
            border-collapse: collapse;
            font-size: 10pt;
            width: 100%;
        }
        table, th, td {
            border: none;
            vertical-align: top;
        }
        .bags {
            font-size: 9px;
        }
        .underline {
            border-bottom: 2px solid black;
        }
        .dotted {
            border-bottom: 1px solid dotted;
        }
    </style>
</head>
<body>

    <table width="100%">
        <tr>
            <td style="width: 50%; font-size: 9px">
                <div style="width: 50%; float: left;"><b>Order No.</b> {{ $order->id }}</div>
                <div style="width: 50%; float: right;text-align: right;"><b>Guest Phone</b> {{ $order->phone }}</div>
                <hr class="underline" style="clear: both;" />
                <table class="order" cellspacing="20" cellpadding="20">
                    <tr>
                        <td width="25%"><b>Name</b></td>
                        <td>{{ $order->name }}</td>
                    </tr>
                    <tr>
                        <td width="25%"><b>Time</b></td>
                        <td>{{ date('g:i A', strtotime($order->pickup_time)) }}</td>
                    </tr>
                    <tr>
                        <td width="25%"><b>Date</b></td>
                        <td>{{ date('F d, Y', strtotime($order->pickup_date)) }}</td>
                    </tr>
                </table>
                <hr class="underline" />
                <p>&nbsp;</p>
                <table class="bags" width="100%">
                    <tr>
                        <td width="25"><b>Total Bags</b></td>
                        <td class="underline" width="100">&nbsp;</td>
                        @if($order->is_complex == 1)
                            <td style="text-align: center;" width="25">
                                <b>Delivery to</b>
                            </td>
                            <td class="underline" style="text-align: center;">{{ $order->complex }}</td>
                        @else
                            <td>&nbsp;</td>
                        @endif
                    </tr>
                </table><br />
                <table width="100%" cellpadding="10" cellspacing="10">
                    <tr><td class="dotted"><b>Dry:</b> &nbsp;</td></tr>
                    <tr><td class="dotted"><b>Cooler:</b> &nbsp;</td></tr>
                    <tr><td class="dotted"><b>Freezer:</b> &nbsp;</td></tr>
                    <tr><td class="dotted"><b>Warm:</b> &nbsp;</td></tr>
                </table>
            </td>
            <td style="width: 50%; font-size: 9px">
                <div style="width: 50%; float: left;"><b>Order No.</b> {{ $order->id }}</div>
                <div style="width: 50%; float: right;text-align: right;"><b>Guest Phone</b> {{ $order->phone }}</div>
                <hr class="underline" style="clear: both;" />
                <table class="order" cellspacing="20" cellpadding="20">
                    <tr>
                        <td width="25%"><b>Name</b></td>
                        <td>{{ $order->name }}</td>
                    </tr>
                    <tr>
                        <td width="25%"><b>Time</b></td>
                        <td>{{ date('g:i A', strtotime($order->pickup_time)) }}</td>
                    </tr>
                    <tr>
                        <td width="25%"><b>Date</b></td>
                        <td>{{ date('F d, Y', strtotime($order->pickup_date)) }}</td>
                    </tr>
                </table>
                <hr class="underline" />
                <p>&nbsp;</p>
                <table class="bags" width="100%">
                    <tr>
                        <td width="25"><b>Total Bags</b></td>
                        <td class="underline" width="100">&nbsp;</td>
                        @if($order->is_complex == 1)
                            <td style="text-align: center;" width="25">
                                <b>Delivery to</b>
                            </td>
                            <td class="underline" style="text-align: center;">{{ $order->complex }}</td>
                        @else
                            <td>&nbsp;</td>
                        @endif
                    </tr>
                </table><br />
                <table width="100%" cellpadding="10" cellspacing="10">
                    <tr><td class="dotted"><b>Dry:</b> &nbsp;</td></tr>
                    <tr><td class="dotted"><b>Cooler:</b> &nbsp;</td></tr>
                    <tr><td class="dotted"><b>Freezer:</b> &nbsp;</td></tr>
                    <tr><td class="dotted"><b>Warm:</b> &nbsp;</td></tr>
                </table>
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td style="width: 50%; font-size: 9px">
                <div style="width: 50%; float: left;"><b>Order No.</b> {{ $order->id }}</div>
                <div style="width: 50%; float: right;text-align: right;"><b>Guest Phone</b> {{ $order->phone }}</div>
                <hr class="underline" style="clear: both;" />
                <table class="order" cellspacing="20" cellpadding="20">
                    <tr>
                        <td width="25%"><b>Name</b></td>
                        <td>{{ $order->name }}</td>
                    </tr>
                    <tr>
                        <td width="25%"><b>Time</b></td>
                        <td>{{ date('g:i A', strtotime($order->pickup_time)) }}</td>
                    </tr>
                    <tr>
                        <td width="25%"><b>Date</b></td>
                        <td>{{ date('F d, Y', strtotime($order->pickup_date)) }}</td>
                    </tr>
                </table>
                <hr class="underline" />
                <p>&nbsp;</p>
                <table class="bags" width="100%">
                    <tr>
                        <td width="25"><b>Total Bags</b></td>
                        <td class="underline" width="100">&nbsp;</td>
                        @if($order->is_complex == 1)
                            <td style="text-align: center;" width="25">
                                <b>Delivery to</b>
                            </td>
                            <td class="underline" style="text-align: center;">{{ $order->complex }}</td>
                        @else
                            <td>&nbsp;</td>
                        @endif
                    </tr>
                </table><br />
                <table width="100%" cellpadding="10" cellspacing="10">
                    <tr><td class="dotted"><b>Dry:</b> &nbsp;</td></tr>
                    <tr><td class="dotted"><b>Cooler:</b> &nbsp;</td></tr>
                    <tr><td class="dotted"><b>Freezer:</b> &nbsp;</td></tr>
                    <tr><td class="dotted"><b>Warm:</b> &nbsp;</td></tr>
                </table>
            </td>
            <td style="width: 50%; font-size: 9px">
                <div style="width: 50%; float: left;"><b>Order No.</b> {{ $order->id }}</div>
                <div style="width: 50%; float: right;text-align: right;"><b>Guest Phone</b> {{ $order->phone }}</div>
                <hr class="underline" style="clear: both;" />
                <table class="order" cellspacing="20" cellpadding="20">
                    <tr>
                        <td width="25%"><b>Name</b></td>
                        <td>{{ $order->name }}</td>
                    </tr>
                    <tr>
                        <td width="25%"><b>Time</b></td>
                        <td>{{ date('g:i A', strtotime($order->pickup_time)) }}</td>
                    </tr>
                    <tr>
                        <td width="25%"><b>Date</b></td>
                        <td>{{ date('F d, Y', strtotime($order->pickup_date)) }}</td>
                    </tr>
                </table>
                <hr class="underline" />
                <p>&nbsp;</p>
                <table class="bags" width="100%">
                    <tr>
                        <td width="25"><b>Total Bags</b></td>
                        <td class="underline" width="100">&nbsp;</td>
                        @if($order->is_complex == 1)
                            <td style="text-align: center;" width="25">
                                <b>Delivery to</b>
                            </td>
                            <td class="underline" style="text-align: center;">{{ $order->complex }}</td>
                        @else
                            <td>&nbsp;</td>
                        @endif
                    </tr>
                </table><br />
                <table width="100%" cellpadding="10" cellspacing="10">
                    <tr><td class="dotted"><b>Dry:</b> &nbsp;</td></tr>
                    <tr><td class="dotted"><b>Cooler:</b> &nbsp;</td></tr>
                    <tr><td class="dotted"><b>Freezer:</b> &nbsp;</td></tr>
                    <tr><td class="dotted"><b>Warm:</b> &nbsp;</td></tr>
                </table>
            </td>
        </tr>
    </table>





    {{--<div style="width: 50%; float: left;"><b>Order No.</b> {{ $order->id }}</div>--}}
    {{--<div style="width: 50%; float: right;text-align: right;"><b>Guest Phone</b> {{ $order->phone }}</div>--}}
    {{--<hr class="underline" style="clear: both;" />--}}

    {{--<table class="order" cellspacing="20" cellpadding="20">--}}
        {{--<tr>--}}
            {{--<td width="25%"><b>Name</b></td>--}}
            {{--<td>{{ $order->name }}</td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
            {{--<td width="25%"><b>Time</b></td>--}}
            {{--<td>{{ date('g:i A', strtotime($order->pickup_time)) }}</td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
            {{--<td width="25%"><b>Date</b></td>--}}
            {{--<td>{{ date('F d, Y', strtotime($order->pickup_date)) }}</td>--}}
        {{--</tr>--}}
    {{--</table>--}}

    {{--<hr class="underline" />--}}

    {{--<p>&nbsp;</p>--}}

    {{--<table class="bags" width="100%">--}}
        {{--<tr>--}}
            {{--<td width="100"><b>Total Bags</b></td>--}}
            {{--<td class="underline" width="100">&nbsp;</td>--}}
            {{--@if($order->is_complex == 1)--}}
                {{--<td style="text-align: center;" width="100">--}}
                    {{--<b>Delivery to</b>--}}
                {{--</td>--}}
                {{--<td class="underline" style="text-align: center;">{{ $order->complex }}</td>--}}
            {{--@else--}}
                {{--<td>&nbsp;</td>--}}
            {{--@endif--}}
        {{--</tr>--}}
    {{--</table><br />--}}

    {{--<table width="100%" cellpadding="10" cellspacing="10">--}}
        {{--<tr><td class="dotted"><b>Dry:</b> &nbsp;</td></tr>--}}
        {{--<tr><td class="dotted">&nbsp;</td></tr>--}}
        {{--<tr><td class="dotted"><b>Cooler:</b> &nbsp;</td></tr>--}}
        {{--<tr><td class="dotted">&nbsp;</td></tr>--}}
        {{--<tr><td class="dotted"><b>Freezer:</b> &nbsp;</td></tr>--}}
        {{--<tr><td class="dotted">&nbsp;</td></tr>--}}
        {{--<tr><td class="dotted"><b>Warm:</b> &nbsp;</td></tr>--}}
        {{--<tr><td class="dotted">&nbsp;</td></tr>--}}
    {{--</table>--}}

</body>
</html>