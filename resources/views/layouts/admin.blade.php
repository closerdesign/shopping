<!DOCTYPE html>
<html lang="en">
<head>

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N2KRN4X');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Online Shopping') }}</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
          integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/bower_components/pickadate/lib/themes/default.css">
    <link rel="stylesheet" href="/bower_components/pickadate/lib/themes/default.date.css">
    <link rel="stylesheet" href="/bower_components/pickadate/lib/themes/default.time.css">
    <link rel="stylesheet" href="/bower_components/magnific-popup/dist/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="/css/datatables.css">

    <link href="/css/app.css" rel="stylesheet">

    <script> window.Laravel = <?php echo json_encode([ 'csrfToken' => csrf_token(), ]); ?> </script>

</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N2KRN4X" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="app" class="admin-container">

    <nav class="navbar navbar-default navbar-fixed-top">

        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ action('AdminController@index') }}">Online Shopping Manager</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">

                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">

                    @if( !auth()->guest() )

                        @if(Auth::user()->isAdmin())


                        @endif

                        <li>
                            <a href="{{ url('/logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-fw fa-sign-out"></i> Logout
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>

                    @endif

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="container">
        @if (count($errors) > 0)
            <div class="alert alert-danger animated fadeInUp" style="margin-bottom: 0;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if ( Session::has('message') )
            <div class="alert alert-{{ Session::get('message')['type'] }} animated fadeInUp">
                {!! Session::get('message')['message'] !!}
            </div>
        @endif
    </div>

    <div id="page-content-wrapper">

        <div id="notifications"></div>

        @yield('content')

    </div><!-- /page content wrapped -->

</div><!-- /app -->

<!-- Scripts -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- <script src="/js/app.js"></script> -->
<script src="/bower_components/pickadate/lib/picker.js"></script>
<script src="/bower_components/pickadate/lib/picker.date.js"></script>
<script src="/bower_components/pickadate/lib/picker.time.js"></script>
<script src="/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
<script src="/bower_components/jq-signature/jq-signature.min.js"></script>
<script type="text/javascript" charset="utf8" src="/js/datatables.js"></script>
<script src="/bower_components/notify-js/Notify.js"></script>

@yield('js')

</body>
</html>
