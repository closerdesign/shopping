<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PDFXJQ7');</script>
    <!-- End Google Tag Manager -->

    <meta name="google-site-verification" content="Qdg8Huqg6bV7UaRAOvtjlcN1mL3CyfRj2Ar-foTKYoo"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Online Shopping Platform</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
          integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/bower_components/pickadate/lib/themes/default.css">
    <link rel="stylesheet" href="/bower_components/pickadate/lib/themes/default.date.css">
    <link rel="stylesheet" href="/bower_components/pickadate/lib/themes/default.time.css">
    <link rel="stylesheet" href="/bower_components/magnific-popup/dist/magnific-popup.css">
    <link rel="stylesheet" href="/css/app.css">

    <script> window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?></script>
    <script src="https://js.stripe.com/v3/"></script>

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDFXJQ7"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="app">

    @if( auth()->user()->suspended ?? false )
    <div class="alert alert-danger">
        Your account has been suspended.
    </div>
    @endif

    <div class="alert alert-danger">
        Our system is not taking orders at this time. We apologize because of the inconvenience.
    </div>

    @if( auth()->user() )

        <div id="very-top">

            <div id="topbar">

                @if( session()->has('categories') )
                    <div class="hamburger">
                        <div class="btn" v-on:click="categoriesWrapper = true">
                            <i class="fa fa-bars fa-2x"></i>
                        </div>
                    </div>
                @endif
                <div class="top-logo" v-show="mobileLogo">
                    <a href="{{ URL::to('') }}">
                        <img src="{{ $brand->logo }}" alt="{{ $brand->name }} Online Shopping Logo">
                    </a>
                </div>
                <div class="top-shopping-cart">
                    <a href="{{ route('checkout') }}">
                        <i class="fa fa-2x fa-shopping-cart"></i>
                        <span class="badge" v-text="productCount"></span>
                    </a>
                </div>
                <div class="top-search">
                    <i v-on:click="accountWrapper = true" class="fa fa-2x fa-user"></i>
                </div>
                <div class="top-search">
                    <a href="#" onclick="$('#top-search').toggle(); $('#vt-search').focus();">
                        <i class="fa fa-2x fa-search"></i>
                    </a>
                </div>

            </div>
            <div id="top-search">
                <form action="{{ action('ShoppingController@search_aws') }}">
                    <div class="input-group">
                        <input type="text" class="form-control input-lg" name="keyword" placeholder="Search..." id="vt-search" required>
                        <div class="input-group-btn">
                            <button class="btn btn-success btn-lg">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <transition name="slide">
            <div id="sidebar-wrapper" class="sidebar-wrapper" v-if="categoriesWrapper">
                <p class="text-right">
                    <i v-on:click="categoriesWrapper = false" class="fa fa-close fa-2x"></i>
                </p>
                <ul>
                    @if(session()->has('categories'))
                        @foreach(session()->get('categories') as $category)
                            @if($category->parent_id == null)
                                <li>
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       data-target="#category{{ $category->id }}" class="collapsed">
                                        <i class="fa fa-chevron-circle-down"></i> {{ $category->name }}
                                    </a>
                                    <ul class="sidebar-sub-menu collapse" id="category{{ $category->id }}">
                                        @foreach($category->children as $subcategory)
                                            <li>
                                                <a href="{{ action('ShoppingController@category', $subcategory->id . '-' . Str::slug($subcategory->name)) }}">
                                                    {{ $subcategory->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endif
                        @endforeach
                    @endif
                </ul>
            </div>
        </transition>

        <transition name="slide">
            <div id="account-wrapper" class="sidebar-wrapper" v-if="accountWrapper">
                <p class="text-right">
                    <i v-on:click="accountWrapper = false" class="fa fa-close fa-2x"></i>
                </p>
                <ul>
                    <li>
                        <a href="{{ action('AccountsController@previous_orders') }}">
                            <i class="fa fa-clock-o"></i> @lang('layout.prev-orders')
                        </a>
                    </li>
                    <li>
                        <a href="{{ action('AccountsController@favorites') }}">
                            <i class="fa fa-heart"></i> @lang('layout.favorites')
                        </a>
                    </li>
                    <li>
                        <a href="{{ action('AccountsController@change_password') }}">
                            <i class="fa fa-key"></i> @lang('layout.change-pass')
                        </a>
                    </li>
                    <li>
                        <a href="{{ action('AccountsController@personal_info') }}">
                            <i class="fa fa-user"></i> @lang('layout.personal-info')
                        </a>
                    </li>
                    <li>
                        <a href="{{ action('ShoppingController@contact') }}">
                            <i class="fa fa-envelope"></i> @lang('layout.contact')
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                        document.getElementById('logout-form-very-top').submit();">
                            <i class="fa fa-sign-out"></i> @lang('layout.logout')
                        </a>

                        <form id="logout-form-very-top" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            @csrf
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        </transition>

        <div id="nav-container">@include('partials._nav')</div>

    @endif

    <div class="your-store">
        <a href="{{ route('personal-info') }}">
            Your store: {{ session()->get('store_data')->name ?? '' }} {{ session()->get('store_data')->address ?? '' }}
        </a>
    </div>

    <div class="admin-container">@yield('content')</div>

    <div class="container">@include('partials._disclaimer')</div>

    @include('partials._modals')

    <custom-item-component ref="customItem"></custom-item-component>

</div>

<script src="https://www.google.com/recaptcha/api.js?render={{ env('RECAPTCHA_V3_KEY') }}"></script>
<script>
    grecaptcha.ready(function () {
        grecaptcha.execute('{{ env('RECAPTCHA_V3_KEY') }}', {action: 'homepage'}).then(function (token) {
            if (document.getElementById('recaptcha')) {
                document.getElementById('recaptcha').value = token;
            }
        });
    });
</script>

<!-- Scripts -->
<script src="/js/app.js"></script>
<script src="/js/cart.js"></script>

@yield('js')

@if( auth()->user() )
<script>
    window.smartlook||(function(d) {
        var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
        var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
        c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', 'fbe0de35643538c91dd56ebf525d7085e5609412');
    smartlook('identify', uid, {
        "name": "{{ auth()->user()->name . ' ' . auth()->user()->last_name }}",
        "email": "{{ auth()->user()->email }}"
    });
</script>
@endif

</body>
</html>
