@extends('layouts.app')

@section('content')

    @include('partials._topbar')

    <div class="container">
        <p class="lead text-center">My Previous Orders</p>
        @if( count($placed) ==  0 )
            <p class="lead text-center">
                There are not yet any orders placed for your account.
            </p>
        @else
            <div class="table-responsive">
                <table class="table table-striped table-condensed">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th class="text-center">Type</th>
                        <th class="text-center">Pickup Date</th>
                        <th class="text-center">Pickup Time</th>
                        <th class="text-center">Ticket Amount</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($placed as $p)
                        <tr>
                            <td class="text-center">
                                <a href="{{ action('AccountsController@order_details', $p->id) }}" class="btn btn-success btn-xs">
                                    <i class="fa fa-plus"></i> View Order Details
                                </a>
                            </td>
                            <td class="text-center">
                                @if($p->delivery_fee > 0)
                                    Delivery
                                @else
                                    Pickup
                                @endif
                            </td>
                            <td class="text-center">{{ date('F d, Y', strtotime($p->pickup_date)) }}</td>
                            <td class="text-center">{{ date('g:i A', strtotime($p->pickup_time)) }}</td>
                            <td class="text-center">{{ $p->final_amount }}</td>
                            <td class="text-center">
                                @if($p->status == 'CANCELLED')
                                    <span style="color: crimson">{{$p->status}}</span>
                                @else
                                    <span class="label {{ $p->label }} btn-block">{{$p->status}}</span>
                                @endif
                            </td>
                            <td>
                                @if( $p->status === 'PLACED' )
                                <a href="{{ URL::to('/notes/' . $p->id) }}" class="btn-xs btn-default btn-block text-center">
                                    Add Note
                                </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $placed->render() }}
            </div>
        @endif
    </div>

@endsection

