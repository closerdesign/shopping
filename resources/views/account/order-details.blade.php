@extends('layouts.app')

@section('content')

    @include('partials._topbar')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <p class="lead text-center">
                    [{{ $order->id }}] Order Details
                    <br />Pickup Date: {{ $order->pickup_date }}
                </p>
                @if($order->pickup_date >= '2020-06-01' && $order->status == 'DELIVERED')
                    <p class="text-center">
                        <a class="btn btn-default" href="{{ action('AccountsController@reorder_items', $order->id) }}" onclick="return confirm('You are adding previously ordered items to your cart. Because this order was placed in the past, prices may vary.')">
                            <i class="fa fa-cart-plus"></i> Add Items To Your Cart
                        </a>
                    </p>
                @endif
                @if( $order->status == 'PLACED' )
                <p class="text-center">
                    Your order has been received and your personal shopper will start working on it soon.<br />
                    <a class="btn btn-primary" href="{{ route('add-note', $order->id) }}">Forgot something? No worries. You can still send a note to your shopper.</a>
                </p>
                @endif
                <div class="table-responsive">
                    <table class="table table-condensed table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th class="text-center">Qty</th>
                            <th class="text-right">Price</th>
                            <th class="text-center">Subtotal</th>
                        </tr>
                        @foreach($order->items->where('custom_item', 0) as $item)
                            <tr>
                                <td>
                                    {{ $item->name }}
                                </td>
                                <td class="text-center">{{ $item->qty }}</td>
                                <td class="text-right">
                                    {{ number_format($item->price, 2) }}
                                </td>
                                <td class="text-right">
                                    {{ number_format(($item->price * $item->qty),2) }}
                                </td>
                            </tr>
                        @endforeach
                        </thead>
                    </table>
                </div>
                <div class="alert alert-info">
                    <i class="fa fa-exclamation-triangle"></i> Custom items are no included.
                </div>
                <hr>
                <div class="text-center">
                    <a href="{{ action('AccountsController@previous_orders') }}" class="btn btn-danger">
                        <i class="fa fa-arrow-circle-left"></i> Go Back
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection