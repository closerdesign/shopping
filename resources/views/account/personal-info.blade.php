@extends('layouts.app')

@section('content')

    @include('partials._topbar')

    <div class="container">
        <p class="lead text-center">
            <i class="fa fa-user"></i> @lang('layout.personal-info')
        </p>
        <form action="{{ action('AccountsController@update_user') }}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">@lang('account.name')</label>
                <input id="last_name" type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" required>
            </div>
            <div class="form-group">
                <label for="last_name">@lang('account.last-name')</label>
                <input id="last-name" type="text" class="form-control" name="last_name" value="{{ Auth::user()->last_name }}" required>
            </div>
            <div class="form-group">
                <label for="email">@lang('account.email')</label>
                <input type="email" class="form-control" name="email" value="{{ auth()->user()->email }}" required >
            </div>
            <div class="form-group">
                <label for="billing_address">@lang('account.billing-address')</label>
                <input type="text" class="form-control" name="billing_address" value="{{ auth()->user()->billing_address }}" required >
            </div>
            <div class="form-group">
                <label for="city">@lang('account.city')</label>
                <input type="text" class="form-control" name="city" value="{{ auth()->user()->city }}" required >
            </div>
            <div class="form-group">
                <label for="zip_code">@lang('account.zip-code')</label>
                <input type="text" class="form-control" name="zip_code" value="{{ auth()->user()->zip_code }}" required >
            </div>
            <div class="form-group">
                <label for="phone">@lang('account.phone')</label>
                <input type="text" class="form-control" name="phone" value="{{ auth()->user()->phone }}" required >
            </div>
            <div class="form-group">
                <label for="form-group">@lang('account.preferred-store')</label>
                <select name="preferred_store" id="preferred_store" class="form-control">
                    <option value="">@lang('general.select')</option>
                    @foreach($brand->stores as $store)
                        <option @if( auth()->user()->preferred_store == $store->store_code ) selected @endif value="{{ $store->store_code }}">{{ $store->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="loyalty_member_id">@lang('account.loyalty-member-id')</label>
                <input type="text" class="form-control" name="loyalty_member_id" value="{{ auth()->user()->loyalty_member_id }}" >
            </div>
            <button type="submit" class="btn btn-block btn-success">Update</button>
        </form>
    </div>

@endsection