@extends('layouts.app')

@section('content')

    <div class="container main-container">
        <div class="row">
            <div id="cart-container">
                @include('partials._cart')
            </div>
            <div class="col-md-8 col-md-pull-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-user"></i> @lang('layout.account')
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{ action('AccountsController@previous_orders') }}">
                                    <div class="panel panel-default text-center" style="padding: 25px;">
                                        <i class="fa fa-4x fa-clock-o"></i><br />
                                        @lang('layout.prev-orders')
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="{{ action('AccountsController@favorites') }}">
                                    <div class="panel panel-default text-center" style="padding: 25px;">
                                        <i class="fa fa-4x fa-heart"></i><br />
                                        @lang('layout.favorites')
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="{{ action('AccountsController@change_password') }}">
                                    <div class="panel panel-default text-center" style="padding: 25px;">
                                        <i class="fa fa-4x fa-key"></i><br />
                                        @lang('layout.change-pass')
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="{{ action('AccountsController@personal_info') }}">
                                    <div class="panel panel-default text-center" style="padding: 25px;">
                                        <i class="fa fa-4x fa-user"></i><br />
                                        @lang('layout.personal-info')
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="/">
                                    <div class="panel panel-default text-center" style="padding: 25px;">
                                        <i class="fa fa-4x fa-home"></i><br />
                                        Home
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br /><br />
@endsection
