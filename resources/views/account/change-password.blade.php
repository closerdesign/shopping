@extends('layouts.app')

@section('content')

    @include('partials._topbar')

    <div class="container">
        <p class="lead text-center"><i class="fa fa-key"></i> @lang('layout.change-pass')</p>

        <form action="{{ action('AccountsController@update_pass') }}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="password">@lang('changepass.current')</label>
                <input id="password" type="password" class="form-control" name="password" required>
            </div>
            <div class="form-group">
                <label for="new_pass">@lang('changepass.new')</label>
                <input type="password" id="new_pass" name="new_pass" class="form-control" minlength="5" required>
            </div>
            <div class="form-group">
                <label for="confirm_pass">@lang('changepass.confirm')</label>
                <input type="password" id="confirm_pass" name="confirm_pass" class="form-control" minlength="5" required>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>

@endsection