@extends('layouts.app')

@section('content')

    @include('partials._topbar')

    <div class="container">
        <div class="row">

            <div id="cart-container">
                @if (isset($missing_items))
                    <div class="alert alert-warning">
                        <strong>The following items were removed from your cart</strong><br />
                        @foreach($missing_items as $item)
                            {{$item}} <br />
                        @endforeach
                    </div>
                @endif
                @include('partials._cart')
            </div>

            <div class="col-md-8 col-md-pull-4">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Loyalty Login
                        </div>
                        <div class="panel-body">
                                 <form method="POST" action="{{ action('AccountsController@loyalty') }}">
                                     {{csrf_field()}}
                                     <div class="form-group">
                                         <label for="username">Loyalty Username</label>
                                         <input class="form-control" name="UserName" type="text" id="username">
                                     </div>
                                     <div class="form-group">
                                         <label for="password">Password</label>
                                         <input class="form-control" type="password" name="Password" id="password">
                                     </div>
                                    <div class="form-group pull-right">
                                        <button class="btn btn-primary">Submit</button>
                                    </div>
                                 </form>


                        </div>
                    </div>

            </div>
        </div>
    </div>

@endsection

