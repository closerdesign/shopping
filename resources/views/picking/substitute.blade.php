@extends('layouts.admin')

@section('content')

    <div class="container-fluid">

        <p class="lead text-center">Item Substitution</p>

        @if( $order->items->filter(function($item){ return $item->is_picked !== true && $item->out_of_stock !== 1; })->count() > 0 )
        <p class="lead text-center">
            From the list below, please select the item you will substitute with
            <br /><b>{{ $product->name }}</b>:
        </p>
        <ul class="list-group">
            @foreach( $order->items->filter(function($item){ return ($item->picked !== 1) && ($item->out_of_stock !== 1); }) as $item )
            <li class="list-group-item text-center">
                <a onclick="return confirm('Are you sure you want to substitute ' + '{{ $item->name }}' + ' with ' + '{{ $product->name }}' + '?')" href="{{ route('add-substitute', [$order->id, $item->id, $product->upc]) }}"  >
                    {{ $item->name }}
                </a>
            </li>
            @endforeach
        </ul>
        <p class="lead text-center">
            Or
        </p>
        @else
        <p class="lead text-center">
            There are no items to substitute.
        </p>
        @endif

        <p class="text-center">
            <a href="{{ route('add-item', [$order->id, $product->upc]) }}" class="btn-lg btn-success">
                Add item to the order
            </a>
        </p>

    </div>

    @endsection