@extends('layouts.admin')

@section('content')
    
    <div class="container-fluid">
        <p class="lead text-center">Price Check</p>
        <form action="{{ route('price-check') }}">
            <div class="form-group">
                <input type="text" class="form-control input-lg text-center" name="upc" placeholder="Scan Item" id="scan">
            </div>
        </form>

        @if( $product != null )
        <p class="text-center">
            <b>{{ $product->product_description }}</b>
        </p>
        <p class="lead text-center">
            {{ $product->ip_unit_price }}
        </p>
        <p class="text-center">
            <span class="badge badge-primary">{{ $product->descriptive_size }}</span>
        </p>
        @else
        <div class="alert alert-danger text-center">
            Product Not Found
        </div>
        @endif
    </div>
    
    @endsection

@section('js')

    <script>
        $(document).ready(function(){
            $('#scan').focus();
        });
    </script>

@endsection