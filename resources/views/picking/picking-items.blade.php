@extends('layouts.admin')

@section('content')

    <div class="container-fluid">

        <p class="lead text-center">
            Picking Order # {{ $order->id }}<br />{{ number_format(($order->items->filter(function($item){ return $item->is_picked; })->count() / $order->items->count()) * 100, 0) }}%
        </p>

        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <form action="{{ action('PickingController@picked', $order->id) }}" method="post">
                    @csrf
                    <div class="form-group">
                        <div class="input-group">
                            <input type="number" class="form-control text-center" name="upc" id="scan" placeholder="Scan Item" required >
                            <span class="input-group-btn">
                    <button class="btn btn-danger btn-block">
                        Add
                    </button>
                    </span>
                        </div>
                    </div>

                </form>

                @if( ($order->items->filter(function($item){ return $item->is_picked || $item->out_of_stock == 1; })->count() / $order->items->count()) >= 1 )

                    @if( !$order->is_custom_items_order )

                        @if( $order->comments !== null || $order->comments !== '' )
                        <div class="form-group">
                            <button class="btn-lg btn-success btn-block" data-toggle="modal" data-target="#pos-confirm">
                                Send to POS
                            </button>
                            <div class="modal fade" tabindex="-1" role="dialog" id="pos-confirm">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Did you read the customer comments?</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p class="lead">{{ $order->comments }}</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No. Go Back.</button>
                                            <a href="{{ action('PosController@add_order', [$order->id, 2]) }}" class="btn btn-primary">Yes. Send to POS.</a>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                        @else
                        <div class="form-group">
                            <a class="btn-lg btn-success btn-block text-center" href="{{ action('PosController@add_order', [$order->id, 2]) }}">
                                Send to POS
                            </a>
                        </div>
                        @endif


                    @else
                    <div class="form-group">
                        <button class="btn btn-danger btn-block">
                            <i class="fa fa-exclamation-triangle"></i> This order contains only custom orders and can't be sent to the POS.
                        </button>
                    </div>
                    @endif

                @endif

                <div class="form-group">
                    <select id="dept-select" class="form-control">
                        <option value="">All items</option>
                        @foreach($order->items->groupBy('dept_code') as $dept)
                            <option value="{{ $dept[0]->dept_code }}">{{ DB::table('store_departments')->where('store_department', $dept[0]->dept_code)->first()->description }}</option>
                        @endforeach
                    </select>
                </div>

                @if($order->comments !== null && $order->comments !== '')
                    <div class="alert alert-danger">
                        {{ $order->comments }}
                    </div>
                @endif

            </div>
        </div>

        @foreach( $order->items->sortBy('is_picked') as $item )
        <div class="row">
            <div class="col col-sm-4 col-sm-offset-4">
                <div class="panel @if( $item->is_picked ) panel-success @else panel-danger @endif item item-{{ $item->dept_code }}">
                    <div class="panel-body">
                        @if( $item->comments !== null && $item->comments !== '' )
                            <div class="alert alert-danger">
                                <i class="fa fa-exclamation-triangle"></i> {{ $item->comments }}
                            </div>
                        @endif
                        @if( $item->custom_item )
                        <div class="alert alert-danger">
                            <i class="fa fa-exclamation-triangle"></i> You will need to ring this item at the POS level.
                        </div>
                        @endif
                        <div class="col-xs-4">
                            <img
                                    src="/img/transparent.png"
                                    style="background:url({!! $item->image !!}); background-size: contain; background-repeat: no-repeat; background-position: center top;"
                                    alt=""
                                    class="img-responsive"
                            >
                            <div style="font-size: 9px;" class="text-center">
                                {{ $item->upc }}
                            </div>
                            @if( !$item->is_picked && !$item->out_of_stock )
                                <form action="{{ route('is-out-of-stock', $item->id) }}" onsubmit="return confirm('Is the item {{ str_replace("'", "`", $item->name) }} out of stock?')" method="post">
                                    @csrf
                                    <button class="btn-xs btn-default btn-block" style="font-size: 9px;">Out of Stock</button>
                                </form>
                            @endif
                        </div>
                        <div class="col-xs-8">
                            <p class="text-right">
                                <b>{{ $item->name }}</b>
                                <br />{{ $item->price }}
                            </p>
                            <p class="text-right">
                        <span class="row">
                            <span class="col-xs-6">
                                Ordered Qty: {{ $item->qty }}
                            </span>
                            <span class="col-xs-6">
                                <a onclick="$('#scan').focus()" href="#" data-toggle="modal" data-target="#picking-modal-{{ $item->id }}">Picked Qty: {{ $item->picked_qty }}</a>
                            </span>
                        </span>
                            </p>

                            <!-- Modal -->
                            <div class="modal fade" id="picking-modal-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        @if( $item->comments !== null && $item->comments !== '' )
                                        <div class="alert alert-danger">
                                            <p>
                                                <b><i class="fa fa-exclamation-triangle"></i> A note has been added to this item:</b><br />
                                                {{ $item->comments }}
                                            </p>
                                        </div>
                                        @endif
                                        <form action="{{ route('item.update', $item->id) }}" method="post">
                                            @csrf
                                            @method('PATCH')
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title text-center" id="myModalLabel">Enter picked Qty</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>{{ $item->name }}</label>
                                                    <input type="number" name="picked_qty" class="form-control input-lg text-center" placeholder="1.0" step="0.01" min="0" value="{{ $item->qty }}">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        @if( $item->allow_substitutions == 'Yes' )
                        Substitutions allowed.
                        @else
                        Substitutions NOT allowed.
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach

        <p class="text-center">
            <i>{{ $order->items->count() }} items found.</i>
        </p>

    </div>

    <div class="container">
        <div class="alert alert-info">
            If an item is out of stock, please find an appropriate substitution suggestion for the guest. This would preferably be <b>same product, different brand</b>. However, please use your best judgement (Pay attention to gluten free and organic items). If the item is continually out of stock, please notate in your admin page by <b>reporting an item</b> that we do not carry.
        </div>
    </div>

    @endsection

@section('js')

    <script>
        $(document).ready(function(){
            $('#scan').focus();
        });

        $('#dept-select').change(function(){
            if( $(this).val() === "" )
            {
                $('.item').show();
            }else{
                var dept = $(this).val();
                $('.item').hide();
                $('.item-' + dept).show();
            }
        });
    </script>

    @endsection