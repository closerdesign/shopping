@extends('layouts.admin')

@section('content')

    <?php $colors = [
        '1006' => 'rgba(129, 229, 41, 0.9)',
        '2701' => 'rgba(173, 41, 229, 0.9)',
        '3501' => 'rgba(255, 99, 132, 0.9)',
        '1230' => 'rgba(255, 206, 86, 0.9)',
        '9515' => 'rgba(54, 162, 235, 0.9)',
    ] ?>

    <div class="container">
        <div class="row panel panel-default">
            <div class="col-sm-6 text-center panel-body">
                <p class="lead">Average Order - ${{ number_format($avg_order_val_all->avg_basket, 2) }}</p>
                <p class="text-center">
                    @foreach($avg_order_by_store as $s)
                        <span style="color: {{$colors[$s->store]}}">
                            <b>{{$s->store}}</b>
                        </span>
                        <small>${{number_format($s->avg_order, 2)}}</small> @if(!$loop->last)<span style="color: #eee">|</span>@endif
                    @endforeach
                </p>
            </div>
            <div class="col-sm-6 text-center panel-body">
                <p class="lead">Sum Total - ${{ number_format($sum_total, 2)}}</p>
                <p class="text-center">
                    @foreach($total_store as $ts)
                        <span style="color: {{$colors[$ts->store]}}">
                            <b>{{$ts->store}}</b>
                        </span>
                        <small>${{number_format($ts->sum_store, 2)}}</small> @if(!$loop->last)<span style="color: #eee">|</span>@endif
                    @endforeach
                </p>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart"></i> Daily Sales
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#daily_total" aria-controls="daily_total" role="tab" data-toggle="tab">Total</a></li>
                                <li role="presentation"><a href="#daily_store" aria-controls="daily_store" role="tab" data-toggle="tab">By Store</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="daily_total"><canvas id="daylyLineChart"></canvas></div>
                                <div role="tabpanel" class="tab-pane fade" id="daily_store"><canvas id="daily"></canvas></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart"></i> Monthly Sales
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#monthly_total" aria-controls="monthly_total" role="tab" data-toggle="tab">Total</a></li>
                                <li role="presentation"><a href="#monthly_store" aria-controls="monthly_store" role="tab" data-toggle="tab">By Store</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="monthly_total"><canvas id="myLineChart"></canvas></div>
                                <div role="tabpanel" class="tab-pane fade" id="monthly_store"><canvas id="monthly"></canvas></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>
    <script>

        // Monthly By Store

        var monthly = document.getElementById('monthly');
        monthly.height = 75;
        var monthlyChart = new Chart(monthly, {
            type: 'bar',
            data: {
                labels: [
                    @foreach($monthly_by_store as $ms)
                    @if($x != $ms->mnth)
                        '{{date('M',mktime(0, 0, 0, $ms->mnth, 10))}}',
                    @endif
                    <?php  $x = $ms->mnth ;?>
                    @endforeach
                ],
                datasets: [{
                    label: '1230',
                    data: [
                        @foreach($monthly_by_store as $ms)
                            @if($ms->store == '1230')
                                '{{$ms->totals}}',
                            @endif
                        @endforeach
                    ],
                    backgroundColor: 'rgba(255, 206, 86, 0.9)',
                    hoverBackgroundColor: 'rgba(255, 206, 86, 0.7)',
                    borderWidth: 0
                },
                {
                    label: '9515',
                    data: [
                        @foreach($monthly_by_store as $ms)
                            @if($ms->store == '9515')
                                '{{$ms->totals}}',
                            @endif
                        @endforeach
                    ],
                    backgroundColor: 'rgba(54, 162, 235, 0.9)',
                    hoverBackgroundColor: 'rgba(54, 162, 235, 0.7)',
                    borderWidth: 0
                },
                {
                    label: '3501',
                    data: [
                        @foreach($monthly_by_store as $ms)
                            @if($ms->store == '3501')
                                '{{$ms->totals}}',
                            @endif
                        @endforeach
                    ],
                    backgroundColor: 'rgba(255, 99, 132, 0.9)',
                    hoverBackgroundColor: 'rgba(255, 99, 132, 0.7)',
                    borderWidth: 0
                },
                {
                    label: '1006',
                    data: [
                        @foreach($monthly_by_store as $ms)
                                @if($ms->mnth != $z)
                                    @if($ms->store == '1006')
                                        '{{$ms->totals}}',
                                    @else
                                        0,
                                    @endif
                                @endif
                        <?php $z = $ms->mnth;?>
                        @endforeach
                    ],
                    backgroundColor: 'rgba(129, 229, 41, 0.9)',
                    hoverBackgroundColor: 'rgba(129, 229, 41, 0.7)',
                    borderWidth: 0
                },
                {
                    label: '2701',
                    data: [
                        @foreach($monthly_by_store as $ms)
                                @if($ms->store == '2701')
                            '{{$ms->totals}}',
                        @endif
                        @endforeach
                    ],
                    backgroundColor: 'rgba(173, 41, 229, 0.9)',
                    hoverBackgroundColor: 'rgba(173, 41, 229, 0.7)',
                    borderWidth: 0
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Daily By Store

        var daily = document.getElementById('daily');
        daily.height = 75;
        var dailyChart = new Chart(daily, {
            type: 'bar',
            data: {
                labels: [
                    @foreach($daily as $day)
                            @if($x != $day->day)
                        '{{ $day->day }}',
                    @endif
                    <?php  $x = $day->day;?>
                    @endforeach
                ],
                datasets: [{
                    label: '1230',
                    data: [
                        @foreach($daily as $day)
                                @if($day->store == '1230')
                            '{{$day->totals}}',
                        @endif
                        @endforeach
                    ],
                    backgroundColor: 'rgba(255, 206, 86, 0.9)',
                    hoverBackgroundColor: 'rgba(255, 206, 86, 0.7)',
                    borderWidth: 0
                },
                {
                    label: '9515',
                    data: [
                        @foreach($daily as $day)
                                @if($day->store == '9515')
                            '{{$day->totals}}',
                        @endif
                        @endforeach
                    ],
                    backgroundColor: 'rgba(54, 162, 235, 0.9)',
                    hoverBackgroundColor: 'rgba(54, 162, 235, 0.7)',
                    borderWidth: 0
                },
                {
                    label: '3501',
                    data: [
                        @foreach($daily as $day)
                                @if($day->store == '3501')
                            '{{$day->totals}}',
                        @endif
                        @endforeach
                    ],
                    backgroundColor: 'rgba(255, 99, 132, 0.9)',
                    hoverBackgroundColor: 'rgba(255, 99, 132, 0.7)',
                    borderWidth: 0
                },
                {
                    label: '1006',
                    data: [
                        @foreach($daily as $day)

                                @if($day->store == '1006')
                            '{{$day->totals}}',
                        @endif
                        @endforeach
                    ],
                    backgroundColor: 'rgba(129, 229, 41, 0.9)',
                    hoverBackgroundColor: 'rgba(129, 229, 41, 0.7)',
                    borderWidth: 0
                },
                {
                    label: '2701',
                    data: [
                        @foreach($daily as $day)
                                @if($day->store == '2701')
                            '{{$day->totals}}',
                        @endif
                        @endforeach
                    ],
                    backgroundColor: 'rgba(173, 41, 229, 0.9)',
                    hoverBackgroundColor: 'rgba(173, 41, 229, 0.7)',
                    borderWidth: 0
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Monthly Total

        var ctxLine = document.getElementById('myLineChart');
        ctxLine.height = 75;
        var myLineChart = new Chart(ctxLine, {
            type: 'line',
            data:{
                labels: [
                    @foreach($total_by_month as $m)
                        '{{date('M',mktime(0, 0, 0, $m->mnth, 10))}}',
                    @endforeach
                ],
                datasets:[{
                    label: 'Monthly Sales',
                    backgroundColor: 'rgba(0, 255, 255, 0.2)',
                    data: [
                        @foreach($total_by_month as $ms)
                        {{$ms->totals}},
                        @endforeach
                    ]
                }]
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Daily Total

        var dailyLine = document.getElementById('daylyLineChart');
        dailyLine.height = 75;
        var dailyLineChart = new Chart(dailyLine, {
            type: 'line',
            data:{
                labels: [
                    @foreach($sales_by_date as $d => $totals)
                    {{date('d', strtotime($d))}},
                    @endforeach
                ],

                datasets:[{
                    label: 'Daily Sales',
                    backgroundColor: 'rgba(255, 255, 0, 0.2)',
                    data: [
                        @foreach($sales_by_date as $d => $totals)
                        {{$totals}},
                        @endforeach
                    ]
                }]
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

    </script>

@endsection