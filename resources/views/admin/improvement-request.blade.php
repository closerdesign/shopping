@extends('layouts.admin')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form method="post" action="{{ action('AdminController@improvement_request_form') }}">
                    {{ csrf_field() }}
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-edit"></i> Improvement Request
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="subject">Subject</label>
                                <input type="text" class="form-control" name="subject" value="{{ old('subject') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="details">Details</label>
                                <textarea name="details" id="" cols="30" rows="10" class="form-control" required>{{ old('details') }}</textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-lg btn-primary">
                                            <i class="fa fa-paper-plane"></i> Send Request
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection