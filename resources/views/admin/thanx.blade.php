<table>
    <tr>
        <th>Email</th>
        <th>Amount</th>
        <th>Date</th>
    </tr>
    @foreach( $transactions as $transaction )
    <tr>
        <td>{{ $transaction->email }}</td>
        <td>{{ $transaction->final_amount }}</td>
        <td>{{ $transaction->pickup_date }}</td>
    </tr>
    @endforeach
</table>