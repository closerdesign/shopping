@extends('layouts.admin')

@section('content')

    <div class="container">

        @if( $orders->count() == 0 )
        <p class="lead text-center">
            Great Job!
            <br />All orders have been picked!
        </p>
        @else
        <div class="row">
            <div class="col-md-6">
                <p class="lead">Placed Orders</p>
            </div>
            <div class="col-md-6 text-right">
                <form action="" method="get">
                    <div class="form-group form-inline">
                        <div class="input-group">
                            <input class="form-control" name="keyword" id="keyword" placeholder="Search Orders..." required >
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-search fa-lg"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row row-no-gutters">
            @foreach( $orders as $order )
            <div class="col-md-4" style="padding: 3px;">
                <div class="panel panel-default" style="height: 195px; max-height: 195px !important;">
                    <div class="panel-body">
                        <div style="height: 125px">
                            <div class="col-xs-7">
                                <b>{{$order->name}}</b>
                                <br />({{substr($order->phone, 0, 3)}}) {{substr($order->phone, 3, 3)}}-{{substr($order->phone, 6)}}
                                <br /><a href="mailto:{{$order->email}}">{{$order->email}}</a>
                                @if($order->is_complex != '')
                                    <br /><b>{{ date('g:i A', strtotime($order->pickup_time)) }}</b><br /><small>{{ date('Y-m-d', strtotime($order->pickup_date)) }}<br /><span class="badge" style="font-size: 8px"><i class="fa fa-car"></i> {{ Str::limit($order->complex, 8)}}</span></small>
                                @else
                                    <br /><span class="badge">{{date('g:i A', strtotime($order->pickup_time))}}</span> {{ date('Y-m-d', strtotime($order->pickup_date)) }}
                                @endif
                            </div>
                            <div class="col-xs-5 text-right">
                                <span class="label {{ $order->label }} btn-block">{{$order->status}}</span>
                                @if($order->final_amount > 0)
                                    ${{number_format($order->total_order, 2)}}<br /><small><i>{{count($order->items)}} Items</i><br />{{$order->payment_method}}<br /><b>Final: ${{number_format($order->final_amount, 2)}}</b></small>
                                @else
                                    ${{ number_format($order->total_order, 2)}}<br /><small><i>{{count($order->items)}} Items</i><br />{{$order->payment_method}}</small>
                                    <br /><small>{{$order->store}}</small>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <a class="btn btn-primary btn-block" href="{{ action('OrdersController@show', $order->id)}}">
                                <i class="fa fa-plus-circle"></i> {{$order->id}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        {{ $orders->appends(request()->except('page'))->render() }}
        @endif

    </div>

@endsection
