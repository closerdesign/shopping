@extends('layouts.admin')

@section('content')
    <div class="container">
        <p><button class="btn btn-primary" onclick="javascript:window.history.back();"><i class="fa fa-arrow-circle-left"></i> Back</button></p>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-list"></i> Orders for {{$orders[0]->name}} &nbsp; ({{$orders[0]->email }})
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped dataTable" id="co">
                            <thead>
                                <tr>
                                    <th class="text-center">Id</th>
                                    <th>Pickup Date</th>
                                    <th>Store</th>
                                    <th>Total Order</th>
                                    <th>Final Order</th>
                                    <th class="text-right">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td class="text-center">
                                            <a href="/admin/order/{{$order->id}}" class="label label-primary">
                                                <i class="fa fa-plus-circle"></i> {{$order->id}}
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            {{$order->pickup_date}}
                                        </td>
                                        <td class="text-center">
                                            {{$order->store}}
                                        </td>
                                        <td class="text-center">
                                            {{$order->total_order}}
                                        </td>
                                        <td class="text-center">
                                            {{$order->final_amount}}
                                        </td>
                                        <td class="text-right">
                                            {{$order->status}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready( function () {
            $('#co').DataTable(
                {
                    "paging": true,
                    "lengthChange":true,
                    "pageLength":25,
                    "ordering": false,
                    "order": [1, 'asc']
                }
            );

        });
    </script>
@endsection