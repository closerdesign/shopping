<table class="table table-striped table-condensed">
    <thead>
    <tr>
        <th>UPC</th>
        <th>Product Name</th>
        <th>Units Sold</th>
    </tr>
    </thead>
    <tbody>
    @foreach($items as $item)
        <tr>
            <td>{{ $item->upc }}</td>
            <td>{{ $item->name }}</td>
            <td class="text-center">{{ $item->count }}</td>
        </tr>
    @endforeach
    </tbody>
</table>