@extends('layouts.admin')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-list"></i> Placed Orders
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="display" id="orders">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Pickup</th>
                            <th style="min-width: 280px">Name</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Store</th>
                            <th>Shopper</th>
                            <th>Created</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready( function () {
            $('#orders').DataTable({
                "paging": true,
                "stateSave": true,
                "ordering": false,
                "deferRender": false,
                "serverSide": true,
                "processing": true,
                "language": {
                    "processing": '<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>'
                },
                "ajax": '{!! action('AdminController@index_data') !!}',
                "columns": [
                    { data: 'id', name: 'id', defaultContent: 'testing', className: 'text-center' },
                    { data: 'pickup_time', name: 'pickup', className: 'text-center' },
                    { data: 'name', name: 'name' },
                    { data: 'total_order', name: 'total', className: 'text-right' },
                    { data: 'status', name: 'status', className: 'text-center' },
                    { data: 'store', name: 'store', className: 'text-center' },
                    { data: 'shopper', name: 'shopper', className: 'text-center' },
                    { data: 'created_at', name: 'created', className: 'text-center', width: 50 }
                ]
            });
        });
    </script>
@endsection