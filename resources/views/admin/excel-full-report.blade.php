<table>
    <tr>
        <th>ID</th>
        <th>Pickup Date</th>
        <th>Pickup Time</th>
        <th>Name</th>
        <th>Billing Address</th>
        <th>ZIP Code</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Payment Method</th>
        <th>Total Order</th>
        <th>Final Amount</th>
        <th>Status</th>
        <th>Paypal Token</th>
        <th>Paypal Payer ID</th>
        <th>Created</th>
        <th>Store</th>
        <th>Cancel Reason</th>
        <th>Completion Date</th>
        <th>Is Complex?</th>
        <th>Complex</th>
    </tr>
    @foreach($orders as $order)
    <tr>
        <td>{{ $order->id }}</td>
        <td>{{ $order->pickup_date }}</td>
        <td>{{ $order->pickup_time }}</td>
        <td>{{ $order->name }}</td>
        <td>{{ $order->billing_address }}</td>
        <td>{{ $order->zip_code }}</td>
        <td>{{ $order->phone }}</td>
        <td>{{ $order->email }}</td>
        <td>{{ $order->payment_method }}</td>
        <td>{{ $order->total_order }}</td>
        <td>{{ $order->final_amount }}</td>
        <td>{{ $order->status }}</td>
        <td>{{ $order->paypal_token }}</td>
        <td>{{ $order->paypal_payer_id }}</td>
        <td>{{ date('Y-m-d', strtotime($order->created_at)) }}</td>
        <td>{{ $order->store }}</td>
        <td>{{ $order->cancel_reason }}</td>
        <td>{{ $order->completion_date}}</td>
        <td>{{ $order->is_complex }}</td>
        <td>{{ $order->complex }}</td>
    </tr>
    @endforeach
</table>