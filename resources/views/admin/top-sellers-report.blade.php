@extends('layouts.admin')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-star"></i> Top Sellers
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <p class="lead">A list of items sold by quantity for completed orders. They are ordered by quantity sold from greatest to least, grouped together by UPC.</p>
                        <hr>
                        <p>
                            <a href="{{ action('AdminController@top_sellers_report_excel') }}" class="btn btn-default"><i class="fa fa-file-excel-o"></i> Export to Excel</a>
                        </p>
                    </div>
                    <div class="col-md-8">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th>UPC</th>
                                    <th>Product Name</th>
                                    <th>Units Sold</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{{ $item->upc }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td class="text-center">{{ $item->count }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $items->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection