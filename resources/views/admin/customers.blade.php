@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-star"></i> Top Customers
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover dataTable" id="cs">
                            <thead>
                                <tr>
                                    <th class="text-left">User</th>
                                    <th class="text-center" width="75">Orders</th>
                                    <th class="text-center">Last Order Date</th>
                                    <th class="text-right">Total</th>
                                    <th class="text-right">Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($numbers as $row)
                                    <tr>
                                        <td>
                                            {{$row->email}}
                                        </td>
                                        <td class="text-center">
                                            {{$row->num_email}}
                                        </td>
                                        <td class="text-center">
                                            {{date('m/d/Y', strtotime($row->last_date))}}
                                        </td>
                                        <td class="text-right">
                                            ${{number_format($row->sum_orders, '2', '.', ',')}}
                                        </td>
                                        <td class="text-right">
                                            <form action="{{action('AdminController@customer_orders')}}" method="post">
                                                {{ csrf_field() }}
                                                <input name="email" type="hidden" value="{{$row->email}}">
                                                <button type="submit" class="btn btn-sm btn-primary">Orders</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready( function () {
            $('#cs').DataTable(
                {
                    "paging": true,
                    "lengthChange":true,
                    "pageLength":25,
                    "ordering": false,
                    "order": [1, 'asc']
                }
            );

        });
    </script>
@endsection
