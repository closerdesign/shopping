<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-N2KRN4X');</script>
    <!-- End Google Tag Manager -->

    <title>{{ $brand->name }} Online Shopping</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{ $brand->icon }}"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login-resources/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login-resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login-resources/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login-resources/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login-resources/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login-resources/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login-resources/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login-resources/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login-resources/css/util.css">
    <link rel="stylesheet" type="text/css" href="/login-resources/css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N2KRN4X"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form" action="{{ url('/login') }}" method="post">

                {{ csrf_field() }}

                <span class="login100-form-title p-b-34">
                    <img src="{{ $brand->logo }}" alt="{{ $brand->name }} Online Shopping" width="120" />
                    <br />Online Shopping
                </span>

                @if (count($errors) > 0)
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                @endif

                <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type email">
                    <input id="email" class="input100" type="email" name="email" placeholder="Email Address" value="{{ old('email') }}" >
                    <span class="focus-input100"></span>
                </div>
                <div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
                    <input class="input100" type="password" name="password" placeholder="Password">
                    <span class="focus-input100"></span>
                </div>

                <div class="container-login100-form-btn m-b-20">
                    <button class="login100-form-btn">
                        Sign in
                    </button>
                </div>

                @if( (URL::to('') === 'https://staging.uptowngroceryco.com') || (URL::to('') === 'http://shopping.test:8000') || (URL::to('') === 'https://shopping.uptowngroceryco.com') )
                <div class="container-login100-form-btn">
                    <a href="/sign-in/facebook" class="login100-form-btn" style="background-color: #3b5998;">
                        Facebook Sign In
                    </a>
                </div>
                @endif

                <div class="w-full text-center p-t-27 p-b-239">

                    <a href="{{ url('/password/reset') }}" class="txt2">
                        Forgot Your Password?
                    </a>
                </div>

                <div class="w-full text-center">
                    <a href="{{ url('/register') }}" class="txt3">
                        Sign Up
                    </a>
                </div>
            </form>

            <div class="login100-more" style="background-image: url('/login-resources/images/bg-01.jpg');"></div>
        </div>
    </div>
</div>



<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="/login-resources/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="/login-resources/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="/login-resources/vendor/bootstrap/js/popper.js"></script>
<script src="/login-resources/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="/login-resources/vendor/select2/select2.min.js"></script>
<script>
    $(".selection-2").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect1')
    });
</script>
<!--===============================================================================================-->
<script src="/login-resources/vendor/daterangepicker/moment.min.js"></script>
<script src="/login-resources/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="/login-resources/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="/login-resources/js/main.js"></script>

@if (  auth()->check() )
<script>
    smartlook('identify', '{{ Auth::user()->id }}', {
        "name": "{{ auth()->user()->name }} {{ auth()->user()->last_name }}",
        "email": "{{ auth()->user()->email }}"
    });
</script>
@endif

</body>
</html>
