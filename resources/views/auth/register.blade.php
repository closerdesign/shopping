@extends('layouts.app')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">

                <p class="text-center">
                    <a href="{{ URL::to('') }}">
                        <img width="180" src="{{ $brand->logo }}" alt="{{ $brand->name }} - Online Shopping" />
                    </a>
                </p>

                <div class="panel panel-default">
                    <div class="panel-heading">@lang('register.register')</div>
                    <div class="panel-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('message')['type'] }}">
                                {{ Session::get('message')['message'] }}
                            </div>
                        @endif

                        <form role="form" method="POST" action="{{ url('/register') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="control-label">@lang('register.name')</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="last_name" class="control-label">@lang('register.last-name')</label>
                                <input id="last-name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="control-label">@lang('register.email')</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required >

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="billing_address">Billing Address</label>
                                <input type="text" class="form-control" name="billing_address" value="{{ old('billing_address') }}" required >
                            </div>

                            <div class="form-group">
                                <label for="city">City</label>
                                <input type="text" class="form-control" name="city" value="{{ old('city') }}" required >
                            </div>
                            
                            <div class="form-group">
                                <label for="zip_code">ZIP Code</label>
                                <input type="number" class="form-control" name="zip_code" value="{{ old('zip_code') }}" maxlength="5" minlength="5" required >
                            </div>

                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="number" class="form-control" name="phone" value="{{ old('phone') }}" required >
                            </div>

                            <div class="form-group">
                                <label for="preferred_store">Preferred Store</label>
                                <select name="preferred_store" id="preferred_store" class="form-control" required >
                                    <option value="">Select...</option>
                                    @foreach($brand->stores as $store)
                                        <option value="{{ $store->store_code }}">{{ $store->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="control-label">@lang('register.password')</label>
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="control-label">@lang('register.confirm-password')</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block">
                                    @lang('register.register')
                                </button>
                                <p class="text-center">
                                    <a class="btn btn-link" href="{{ url('/login') }}">
                                        @lang('register.already-have-an-account')
                                    </a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
