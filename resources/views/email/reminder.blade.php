@extends('email.layout')

@section('content')

    @if($order->store == '3501')
        <?php $address = '3501 NW Expressway - OKC, OK 73112';
        $store_phone = '405-537-8134';
        $brand = 'Buy For Less' ?>
    @elseif($order->store == '2701')
        <?php $address = '2701 SW 29th St. - OKC, OK 73119';
        $store_phone = '405- 685-7791';
        $brand = 'SuperMercado' ?>
    @elseif($order->store == '9515')
        <?php $address = '9515 N. May Ave. - OKC, OK 73120';
        $store_phone = '405-436-2392';
        $brand = 'Uptown Grocery Co' ?>
    @elseif($order->store == '1230')
        <?php $address = '1230 W. Covell Rd - Edmond, OK 73003';
        $store_phone = '405-513-1917';
        $brand = 'Uptown Grocery Co' ?>
    @elseif($order->store == '1124')
        <?php $address = '1124 NE 36th Street OKC, OK 73111';
        $store_phone = '405-594-0769';
        $brand = 'Uptown Grocery Co' ?>
    @elseif($order->store == '1006')
        <?php $address = '10011 SE 15th St. - Midwest City, OK 73130';
        $store_phone = '405- 733-4385';
        $brand = 'Smart Saver' ?>
    @endif

    <h1>A Friendly Reminder</h1>
    <hr />

    <p>Today is the day to pick up your groceries at {{ $brand }}!</p>

    <table width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td width="50%" style="text-align: center; background-color: #ccc;"><b>Time</b></td>
            <td width="50%" style="text-align: center; background-color: #ccc;"><b>Location</b></td>
        </tr>
        <tr>
            <td style="background-color: #f5f5f5; text-align: center">{{ date('g:i A', strtotime($order->pickup_time)) }}</td>
            <td style="background-color: #f5f5f5; text-align: center">{{ $address }}</td>
        </tr>
        <tr>
            <td width="50%" style="text-align: center; background-color: #ccc;"><b>Name</b></td>
            <td width="50%" style="text-align: center; background-color: #ccc;"><b>ID</b></td>
        </tr>
        <tr>
            <td style="background-color: #f5f5f5; text-align: center">{{ $order->name }}</td>
            <td style="background-color: #f5f5f5; text-align: center">{{ $order->id }}</td>
        </tr>
        <tr>
            <td width="50%" style="text-align: center; background-color: #ccc;"><b>Phone</b></td>
            <td width="50%" style="text-align: center; background-color: #ccc;"><b>Email</b></td>
        </tr>
        <tr>
            <td style="background-color: #f5f5f5; text-align: center">{{ $order->phone }}</td>
            <td style="background-color: #f5f5f5; text-align: center">{{ $order->email }}</td>
        </tr>
        <tr>
            <td width="50%" style="text-align: center; background-color: #ccc;"><b>Payment Method</b></td>
            <td width="50%" style="text-align: center; background-color: #ccc;"><b>Total</b></td>
        </tr>
        <tr>
            <td style="background-color: #f5f5f5; text-align: center">{{ $order->payment_method }}</td>
            <td style="background-color: #f5f5f5; text-align: center">{{ number_format($order->total_order, 2) }}</td>
        </tr>
        <tr>
            <td colspan="2" width="100%" style="text-align: center; background-color: #ccc;"><b>Comments</b></td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: #f5f5f5; text-align: center">{{ $order->comments }}</td>
        </tr>
    </table><br />

    <h2>Thank you for your business!</h2>
    <hr />

    <p>If you have any questions or concerns, please call {{ $store_phone }}. We look forward to seeing you!</p>



@endsection