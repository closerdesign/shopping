<style>
    td{
        border-width: 1px; border-style: solid; border-color: #e9e9e9; padding: 5px; font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 11px;
    }
</style>

<p style="color: red">CSV file attached.</p>

<p>Please check the information below:</p>

<table width="600" align="center">
    <tr>
        <th>Order ID</th>
        <th>Email</th>
        <th>Amount</th>
        <th>Date</th>
        <th>Store</th>
        <th>Payment Method</th>
    </tr>
    @foreach( $transactions as $transaction )
    <tr>
        <td>{{ $transaction->id }}</td>
        <td>{{ $transaction->email }}</td>
        <td>{{ $transaction->final_amount }}</td>
        <td>{{ $transaction->pickup_date }}</td>
        <td>{{ $transaction->store }}</td>
        <td>{{ $transaction->payment_method }}</td>
    </tr>
    @endforeach
</table>