@extends('email.layout')

@section('content')
    @if($order->store == '3501')
        <p style="text-align: center;"><img src="https://shopping.buyforlessok.com/img/buyforless-logo.png" width="200" /></p>
    @elseif($order->store == '2701')
        <p style="text-align: center;"><img src="https://shopping.buyforlessok.com/img/supermercado-logo.png" width="200" /></p>
    @elseif($order->store == '9515')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
    @elseif($order->store == '1230')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
    @elseif($order->store == '1124')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
    @elseif($order->store == '1006')
        <p style="text-align: center;"><img src="https://shopping.smartsaverok.com/img/smartsaver-logo.png" width="200" /></p>
    @endif
    <p>
        Hello, {{ explode(' ',$order->name)[0] }}! I am just sending you
        a quick message to see if you had any issues with your recent online shopping experience.
    </p>
    <p>
        If so, and there is anything we can do to help, or if you have any other questions or comments,
        please do not hesitate to contact us by replying to this email or calling <b>405-302-6273 ext. 333</b>.
    </p>

    <p>Thank You!</p>

@endsection