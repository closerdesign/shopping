@extends('email.layout')

@section('content')
    @if($order->store == '3501')
        <p style="text-align: center;"><img src="https://shopping.buyforlessok.com/img/buyforless-logo.png" width="200" /></p>
        <?php $brand = 'Buy For Less';
        $address = '3501 NW Expressway  OKC, OK 73112';
        $doors = 'west';
        $store_phone = '405-537-8134' ?>
    @elseif($order->store == '2701')
        <p style="text-align: center;"><img src="https://shopping.buyforlessok.com/img/supermercado-logo.png" width="200" /></p>
        <?php $brand = 'SuperMercado';
        $address = '2701 SW 29th St.  OKC, OK 73119';
        $doors = 'south';
        $store_phone = '405-685-7791' ?>
    @elseif($order->store == '9515')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
        <?php $brand = 'Uptown Grocery Co';
        $address = '9515 N. May Ave.  OKC, OK 73120';
        $doors = 'south';
        $store_phone = '405-436-2392' ?>
    @elseif($order->store == '1230')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
        <?php $brand = 'Uptown Grocery Co';
        $address = '1230 W. Covell Rd  Edmond, OK 73003';
        $doors = 'west';
        $store_phone = '405-513-1917' ?>
    @elseif($order->store == '1124')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
        <?php $brand = 'Uptown Grocery Co';
        $address = '1124 NE 36th Street  OKC, OK 73111';
        $doors = 'north';
        $store_phone = '405-594–0769' ?>
    @elseif($order->store == '1006')
        <p style="text-align: center;"><img src="https://shopping.smartsaverok.com/img/smartsaver-logo.png" width="200" /></p>
        <?php $brand = 'Smart Saver';
        $address = '10011 SE 15th St. Midwest City, OK 73130';
        $doors = 'west';
        $store_phone = '405-733-4385' ?>
    @endif
    <p>
        Hello, {{ explode(" ", $order->name)[0] }}! You order is completed and your groceries are ready for pick up!
        However your order total exceeded the original authorized total. Because we are not authorized to charge a higher
        amount, we will need to take payment once you arrive. We apologize for any inconvenience this may cause.
    </p>

    @if($order->payment_method != 'InStore')
        @if($order->is_complex != '1')
            <p>
                Your order can be picked up at:<br />
                <b>{{ $brand }}, {{ $address }}</b>
            </p>

            <p>On <b>{{ date('F d, Y', strtotime($order->pickup_date)) }}</b> at <b>{{ date('g:i A', strtotime($order->pickup_time)) }}</b>.</p>

            <p>
                When you arrive, please pull up to our online shopping doors on the {{ $doors }} side of the building, indicated with a stop sign. Please call the number
                listed on the stop sign, and wait for one of our employees to assist you with your groceries!
            </p>
        @else
            <p>
                Your order is set for delivery to: <b>{{ $order->complex }}</b>
            </p>

            <p>On <b>{{ date('F d, Y', strtotime($order->pickup_date)) }}</b> at <b>{{ date('g:i A', strtotime($order->pickup_time)) }}</b>.</p>
        @endif
    @endif

    <p>
        For any immediate concerns, please call <b>{{ $store_phone }}</b>.
    </p>

@endsection