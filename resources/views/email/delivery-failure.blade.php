@extends('email.layout')

@section('content')

    <p>A delivery order has been placed but failed to get a shipt ID</p>
    <p>
        <a href="{{ action('OrdersController@show', $order->id) }}">
            Click here to see details
        </a>
    </p>

@endsection