@extends('email.layout')

@section('content')

    <p>The following UPC has been reported as an <b>item we don't carry</b>:</p>

    <p><b>UPC:</b><br />{{ $id }}</p>

    <p><b>Store:</b><br />{{ $store }}</p>

    <p><b>Submitted By:</b><br />{{ Auth::user()->name }}</p>

@endsection