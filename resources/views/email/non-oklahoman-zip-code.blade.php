@extends('email.layout')

@section('content')
    
    <p>Order was placed with a Non Oklahoman Zip Code:</p>
    <p>
        <a href="{{ action('OrdersController@show', $order->id) }}">
            Click here to see details
        </a>
    </p>
    
    @endsection