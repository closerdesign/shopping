@extends('email.layout')

@section('content')

    @if($order->store == '3501')
        <p style="text-align: center;"><img src="https://shopping.buyforlessok.com/img/buyforless-logo.png" width="200" /></p>
        <?php $url = 'https://shopping.buyforlessok.com/terms';
        $store_phone = '405-537-8134' ?>
    @elseif($order->store == '2701')
        <p style="text-align: center;"><img src="https://shopping.buyforlessok.com/img/supermercado-logo.png" width="200" /></p>
        <?php $url = 'https://supermercado.buyforlessok.com/terms';
        $store_phone = '405-685-7791' ?>
    @elseif($order->store == '9515')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
        <?php $url = 'https://shopping.uptowngroceryco.com/terms';
        $store_phone = '405-436-2392' ?>
    @elseif($order->store == '1230')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
        <?php $url = 'https://shopping.uptowngroceryco.com/terms';
        $store_phone = '405-513-1917' ?>
    @elseif($order->store == '1124')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
        <?php $url = 'https://shopping.uptowngroceryco.com/terms';
        $store_phone = '405-594-0769' ?>
    @elseif($order->store == '1006')
        <p style="text-align: center;"><img src="https://shopping.smartsaverok.com/img/smartsaver-logo.png" width="200" /></p>
        <?php $url = 'https://shopping.smartsaverok.com/terms';
        $store_phone = '405-733-4385' ?>
    @endif

    <p>
        Hello, {{ explode(' ',$order->name)[0] }}! I am fulfilling your online shopping order today.
        I attempted to contact you by text regarding substitutions, however I was unable to reach you. Please contact us
        at <b>{{ $store_phone }}</b> to move forward!
    </p>

    <p>Are you busy? No problem. We'll continue shopping, using our <a href="{{ $url }}">substitution policy</a>.</p>

    <p>Thank You!</p>

@endsection