@extends('email.layout')

@section('content')

    @foreach($request->toArray() as $field => $value)

    <p>
        <b style="text-transform: uppercase">{{ $field }}:</b> {{ $value }}
    </p>

    @endforeach

    @endsection