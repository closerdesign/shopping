@extends('email.layout')

@section('content')
    @if($order->store == '3501')
        <p style="text-align: center;"><img src="https://shopping.buyforlessok.com/img/buyforless-logo.png" width="200" /></p>
    @elseif($order->store == '2701')
        <p style="text-align: center;"><img src="https://shopping.buyforlessok.com/img/supermercado-logo.png" width="200" /></p>
    @elseif($order->store == '9515')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
    @elseif($order->store == '1230')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
    @elseif($order->store == '1124')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
    @elseif($order->store == '1006')
        <p style="text-align: center;"><img src="https://shopping.smartsaverok.com/img/smartsaver-logo.png" width="200" /></p>
    @endif
    <p>
        <b>We're sorry, {{ explode(" ", $order->name)[0] }}, but we can't fulfill your order!</b>
    </p>

    <p>We're sorry to say that your <b>order No. {{ $order->id }}</b> was recently cancelled because:</p>

    <p>{{ $request->cancel_reason }}</p>

    <hr>

    <p>We apologize for any inconvenience this may have caused. Of course, your card hasn't been charged. If you think
        that this might be a misunderstanding, please feel free to respond to this message and let us know. </p>

    @endsection