@extends('email.layout')

@section('content')

    <p>The following item has been reported as <b>out of stock</b>:</p>

    <p><b>Item:</b><br />{{ $item->name }}</p>

    <p><b>UPC:</b><br />{{ $item->upc }}</p>

    <p><img src="{{ urldecode($item->image) }}" alt="{{ $item->name }}" width="250"></p>

@endsection