@extends('email.layout')

@section('content')
    @if($order->store == '3501')
        <p style="text-align: center;"><img src="https://shopping.buyforlessok.com/img/buyforless-logo.png" width="200" /></p>
        <?php $brand = 'Buy For Less';
        $address = '3501 NW Expressway  OKC, OK 73112';
        $store_phone = '405-537-8134' ?>
    @elseif($order->store == '2701')
        <p style="text-align: center;"><img src="https://shopping.buyforlessok.com/img/supermercado-logo.png" width="200" /></p>
        <?php $brand = 'SuperMercado';
        $address = '2701 SW 29th St.  OKC, OK 73119';
        $store_phone = '405-685-7791' ?>
    @elseif($order->store == '9515')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
        <?php $brand = 'Uptown Grocery Co';
        $address = '9515 N. May Ave.  OKC, OK 73120';
        $store_phone = '405-436-2392' ?>
    @elseif($order->store == '1006')
        <p style="text-align: center;"><img src="https://shopping.smartsaverok.com/img/smartsaver-logo.png" width="200" /></p>
        <?php $brand = 'Smart Saver';
        $address = '10011 SE 15th St.  Midwest City, OK 73130';
        $store_phone = '405- 733-4385' ?>
    @elseif($order->store == '1230')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
        <?php $brand = 'Uptown Grocery Co';
        $address = '1230 W. Covell Rd  Edmond, OK 73003';
        $store_phone = '405-513-1917' ?>
    @elseif($order->store == '1124')
        <p style="text-align: center;"><img src="https://shopping.uptowngroceryco.com/img/uptown-logo.png" width="200" /></p>
        <?php $brand = 'Uptown Grocery Co';
        $address = '1124 NE 36th Street OKC, OK 73111';
        $store_phone = '405-594-0769' ?>
    @endif
    <p><b>Thank you for shopping at {{ $brand }}, {{ explode(' ',$order->name)[0] }}!</b><br />(Your pantry will thank you too!)</p>

    <p>We have received your order, and will get started on it right away! You can expect to receive a confirmation email
        when your order has been completed. As always, thank you for shopping local and believing in our team! </p>

    <p>Please note that an additional hold of 20% was added to the total amount for any difference in pricing
        on your weighted items and/or custom items.</p>

    <p>Did you forget something, or just need to leave a note for your personal shopper? There's still time!</p>

    <p style="text-align: center;">
        <a href="{{ URL::to('/notes/' . $order->id) }}" style="background-color: #3498db; border: none; color: white; padding: 15px; text-align: center; text-decoration: none; font-size: 16px;">Add a Note to your Order</a>
    </p>

    <hr />

    <h1>Your Order</h1>

    <table width="100%" border="0" cellpadding="5" cellspacing="2">
        <tr>
            <th style="width: 50%; background-color: #ccc">Name</th>
            <th style="background-color: #ccc">ID</th>
        </tr>
        <tr>
            <td style="text-align: center; background-color: #f5f5f5">{{ $order->name }}</td>
            <td style="text-align: center; background-color: #f5f5f5">{{ $order->id }}</td>
        </tr>
        <tr>
            <th style="width: 50%; background-color: #ccc">Billing Address</th>
            <th style="background-color: #ccc">ZIP Code</th>
        </tr>
        <tr>
            <td style="text-align: center; background-color: #f5f5f5">{{ $order->billing_address }}</td>
            <td style="text-align: center; background-color: #f5f5f5">{{ $order->zip_code }}</td>
        </tr>
        <tr>
            <th style="background-color: #ccc">Phone</th>
            <th style="background-color: #ccc">Email</th>
        </tr>
        <tr>
            <td style="text-align: center; background-color: #f5f5f5">{{ $order->phone }}</td>
            <td style="text-align: center; background-color: #f5f5f5">{{ $order->email }}</td>
        </tr>
        <tr>
            <th style="background-color: #ccc">Payment Method</th>
            <th style="background-color: #ccc">Total</th>
        </tr>
        <tr>
            <td style="text-align: center; background-color: #f5f5f5">{{ $order->payment_method }}</td>
            <td style="text-align: center; background-color: #f5f5f5">{{ number_format($order->total_order, 2) }}</td>
        </tr>
    </table><br />

    <table width="100%" border="0" cellpadding="5" cellspacing="2">
        @if($order->is_complex != '1')
            <tr>
                <th colspan="2" style="background-color: #ccc">Pickup Location</th>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; background-color: #f5f5f5">{{ $brand }}, {{ $address }}</td>
            </tr>
        @else
            <tr>
                <th colspan="2" style="background-color: #ccc">Delivery To</th>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; background-color: #f5f5f5">{{ $order->complex }}</td>
            </tr>
        @endif
            <tr>
                <th style="background-color: #ccc">Date</th>
                <th style="background-color: #ccc">Time</th>
            </tr>
            <tr>
                <td style="width: 50%; text-align: center; background-color: #f5f5f5">{{ date('F d, Y', strtotime($order->pickup_date)) }}</td>
                <td style="text-align: center; background-color: #f5f5f5">{{ date('g:i A', strtotime($order->pickup_time)) }}</td>
            </tr>
    </table>

    @if( !empty($order->comments) )
    <br /><table width="100%" border="0" cellpadding="5" cellspacing="2">
        <tr>
            <th colspan="2" style="background-color: #ccc">Comments</th>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center; background-color: #f5f5f5">{{ $order->comments }}</td>
        </tr>
    </table>
    @endif

    <hr />

    <h1>Items</h1>

    <table width="100%" border="0" cellpadding="5" cellspacing="2">
        <tr>
            <th style="background-color: #ccc">Name</th>
            <th style="background-color: #ccc">Price</th>
            <th style="background-color: #ccc">Qty</th>
            <th style="background-color: #ccc">Subtotal</th>
        </tr>
        @foreach($order->items as $item)
            <tr>
                <td style="background-color: #f5f5f5">{{ $item->name }}</td>
                <td style="text-align: right; background-color: #f5f5f5">{{ number_format($item->price, 2) }}</td>
                <td style="text-align: center; background-color: #f5f5f5">{{ $item->qty }}</td>
                <td style="text-align: right; background-color: #f5f5f5">{{ number_format(($item->price * $item->qty), 2) }}</td>
            </tr>
            @if( !empty($item->comments) )
                <tr><td colspan="4" style="background-color: #f5f5f5;"><b>Your Comments:</b> {{ $item->comments }} </td></tr>
            @endif
        @endforeach
    </table>

    <hr />

    <p>
        If you have any questions, or concerns, you can respond to this message, send us an email at
        <a href="mailto:onlineshopping@buyforlessok.com">onlineshopping@buyforlessok.com</a> or call {{ $store_phone }}.
    </p>

@endsection