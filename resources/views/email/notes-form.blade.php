@extends('email.layout')

@section('content')

    <p><b>{{ $order->name }}</b> has added the following note to their order: </p>

    <p><blockquote>{{ $request->message }}</blockquote></p><hr />

    <h3>Order Details</h3>

    <p>
        <table width="100%" border="0" cellpadding="5" cellspacing="2">
            <tr>
                <th style="width: 50%; background-color: #ccc">Name</th>
                <th style="background-color: #ccc">ID</th>
            </tr>
            <tr>
                <td style="text-align: center; background-color: #f5f5f5">{{ $order->name }}</td>
                <td style="text-align: center; background-color: #f5f5f5">{{ $order->id }}</td>
            </tr>
            <tr>
                <th style="background-color: #ccc">Phone</th>
                <th style="background-color: #ccc">Email</th>
            </tr>
            <tr>
                <td style="text-align: center; background-color: #f5f5f5">{{ $order->phone }}</td>
                <td style="text-align: center; background-color: #f5f5f5">{{ $order->email }}</td>
            </tr>
            <tr>
                <th style="background-color: #ccc">Payment Method</th>
                <th style="background-color: #ccc">Total</th>
            </tr>
            <tr>
                <td style="text-align: center; background-color: #f5f5f5">{{ $order->payment_method }}</td>
                <td style="text-align: center; background-color: #f5f5f5">{{ number_format($order->total_order, 2) }}</td>
            </tr>
        </table><br />
    </p>

    <p>
        <table width="100%" border="0" cellpadding="5" cellspacing="2">
            @if($order->is_complex != '1')
                <tr>
                    <th colspan="2" style="background-color: #ccc">Store</th>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center; background-color: #f5f5f5">{{ $order->store }}</td>
                </tr>
            @else
                <tr>
                    <th colspan="2" style="background-color: #ccc">Delivery To</th>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center; background-color: #f5f5f5">{{ $order->complex }}</td>
                </tr>
            @endif
            <tr>
                <th style="background-color: #ccc">Date</th>
                <th style="background-color: #ccc">Time</th>
            </tr>
            <tr>
                <td style="width: 50%; text-align: center; background-color: #f5f5f5">{{ date('F d, Y', strtotime($order->pickup_date)) }}</td>
                <td style="text-align: center; background-color: #f5f5f5">{{ date('g:i A', strtotime($order->pickup_time)) }}</td>
            </tr>
        </table>
    </p>

@endsection