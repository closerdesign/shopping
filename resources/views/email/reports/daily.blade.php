@extends('email.layout')

@section('content')

    <p>Good Morning,</p>
    <p>Please find attached our Online Shopping Report for the store {{ $store_code->first()->store }} that contains the transactions performed on {{ $date }}.</p>
    <p>Regards,</p>
    <p>
        <b>BFL Media Team</b>
    </p>

    @endsection