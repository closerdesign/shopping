@extends('email.layout')

@section('content')

    <p><b>Store</b>:<br />{{ $request->store }}</p>
    <p><b>Name</b>:<br />{{ $request->name }}</p>
    <p><b>Email</b>:<br />{{ $request->email }}</p>
    <p><b>Phone</b>:<br />{{ $request->phone }}</p>
    <p><b>Contact Preference</b>:<br />{{ $request->contact_preference }}</p>
    <p><b>Message</b>:<br />{{ $request->message }}</p>

    @endsection