@extends('email.layout')

@section('content')

    <p>Please check the details below:</p>

    <p><b>Subject:</b><br />{{ $request->subject }}</p>
    <p><b>Details:</b><br />{{ $request->details }}</p>

@endsection