@extends('email.layout')

@section('content')

    <p><b>Name</b>:<br />{{ $request->name }}</p>
    <p><b>Email</b>:<br />{{ $request->email }}</p>
    <p><b>Experience</b>:<br />{{ $request->experience }}</p>
    <p><b>Message</b>:<br />{{ $request->message }}</p>

@endsection