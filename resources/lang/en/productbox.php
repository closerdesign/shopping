<?php

return [

    'sale' => 'On Sale!',
    'weight' => 'Final cost based on weight',
    'add' => 'Add to Cart',

];
