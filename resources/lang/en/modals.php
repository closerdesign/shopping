<?php

return [

    'change-store' => 'Change store',
    'session' => 'Changing stores will require your session and shopping cart to be cleared. Would you still like to proceed?',
    'cancel' => 'Cancel',
    'confirm' => 'Confirm',
    'select-pickup' => 'Select a pickup date / time',
    'apt' => 'Order for Apartment Community Delivery ONLY',
    'apt-name' => 'Name of Apartment Community',
    'date' => 'Pickup Date',
    'time' => 'Pickup Time',
    'partner-msg' => '<p>Please Note: All deliveries are made Thursday, between 1 and 5 PM.</p> <p>Uptown Grocery partners exclusively with apartment communities! Want to bring this amenity to your complex? 
                      Please <a href="/contact">contact us</a> and let us know!</p>',
    'save' => 'Save',
    'howitworks-list' => '<li><b>Select a store</b></li>
                          <li><b>Fill your cart</b><br /><small><span class="text-info">$50 minimum</span> for all online orders</small></li>
                          <li><b>Select a pickup date and time on the checkout page</b><br /><small>There are currently only 2 pickups per half hour for each store.</small></li>
                          <li><b>Checkout and pay</b><br /><small>You can pay with a credit card or Paypal.</small></li>
                          <li><b>Show up at the selected store at your selected date and time for pickup</b><br /><small>When you arrive, please pull up to our online shopping doors, indicated with a stop sign. Call the number listed on the stop sign, and wait for one of our employees to assist you with your groceries!</small></li>',
    'gotit' => 'Got it!',
    'custom' => 'Add Custom Item',
    'item' => 'Item Name',
    'price' => 'Estimated Price',
    'guess' => 'Please provide your best guess for the price of the item. You can also add any additional comments in your cart or on the checkout page.',
    'addtocart' => 'Add to Cart',

];
