<?php

return [

    'checkout' => 'Checkout',
    'pay-now' => 'Pay Now',
    'review' => 'Please review your items. You can make changes by clicking the &quot;Edit&quot; tab below.',
    'disallow' => 'Disallow all substitutions',
    'weight' => 'Final cost based on weight',
    'allow-subs' => 'Allow Substitutions',
    'add-comment' => 'Add comment',
    'edit' => 'Edit',
    'cybermonday' => '<b>CYBER MONDAY:</b> Get $10 OFF your entire purchase when you spend $50 or more! Does not apply to Tobacco, Alcoholic Beverages, and Pharmacy. Discount will be reflected on your final receipt at the time of pickup!',
    'tax' => 'Estimated Tax',
    'total' => 'Total',
    'weighted' => 'Some of these items may need to be weighed. You\'ll pay for the final weight.',
    'personal' => 'Personal Information',
    'at' => 'at',
    'reserve' => 'Click to reserve your pickup time',
    'pickup-header' => 'Location, Date &amp; Time',
    'subpref' => 'Substitution Preference',
    'subs-msg' => 'Please choose what you would prefer your personal shopper to do if an item is out of stock.',
    'contactme' => 'Contact Me',
    'judgement' => 'Use Best Judgement',
    'nosubs' => 'Do Not Substitute',
    'apt-num' => 'Apt Number',
    'name' => 'First Name',
    'last-name' => 'Last Name',
    'phone' => 'Phone',
    'phone-msg' => 'Please provide a number our pickers can use to contact you about substitution preferences, clarifications on comments, etc.',
    'email' => 'Email',
    'email-msg' => 'You\'ll receive a confirmation email once your order is complete.',
    'comments' => 'Comments',
    'billing-address' => 'Billing Address',
    'zip-code' => 'Zip Code',
    'tax-exempt' => 'Tax Exempt',
    'tax-number' => 'Tax Exempt Number',
    'yes' => 'Yes',
    'grilling-agree' => 'I have read and agree to the meat grilling terms &amp; conditions.',
    'grilling-click' => 'Click here to view.',
    'promo' => 'Promo Code',
    'promo-msg' => 'Discount will be reflected on your final receipt at the time of pickup.',
    'payment' => 'Payment Method',
    'creditcard' => 'Credit Card',
    'minimum' => '$50 minimum not yet met',
    'keep-shopping' => 'Keep shopping!',
    'no-pickup' => 'You have not yet selected a date and time for pickup.<br />Please <a class="alert-danger-link" data-toggle="modal" data-target="#pickupModal">click here to reserve one</a>.',
    'additional-hold' => 'We will place an additional hold of 20% to the total amount for any difference in pricing on your weighted items.',
    'echeck' => 'Please note: If you select eCheck, your pickup date needs to be at least 7 days out to process the payment.',
    'delivery_eligible' => 'Type in your zip code to see if you are eligible for grocery delivery <span class="text-red">($14 charge)</span>.',
    'look_up' => 'Look Up',
    'eligible' => 'Your zip code is eligible!',
    'delivery-click' => 'Click here to reserve a delivery time.',
    'not_eligible' => 'Sorry, that zip code is not eligible for delivery at this time.',
    'alcohol-law' => 'Sorry! State Law prohibits alcohol delivery, if you would like delivery please remove all alcohol from your order.',
    'delivery_modal_header' => 'Select delivery time',
    'schedule_delivery' => ' If you would like to schedule delivery for an additional $14 charge, please select a delivery date and time below.',
    'delivery_date' => 'Delivery Date',
    'delivery_time' => 'Delivery Time',

    'first-name' => 'First Name',
    'city' => 'City',
    'loyalty-program-number' => 'Loyalty Program Number',
    'delivery-address' => 'Address',
    'delivery-city' => 'City',
    'delivery-zip-code' => 'Zip Code',
    'delivery-first-name' => 'First Name',
    'delivery-last-name' => 'Last Name',
    'delivery-phone' => 'Phone',
    'delivery-instructions' => 'Delivery Instructions',
    'billing-information' => 'Additional Information',
    'delivery-information' => 'Delivery Information',
    'order-information' => 'Order Information',
    'get-your-order-delivered' => 'Get Your Order Delivered!',

];
