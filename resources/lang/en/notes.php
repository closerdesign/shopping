<?php

return [

    'order' => 'Order No.',
    'name' => 'Name',
    'date' => 'Pickup Date',
    'time' => 'Pickup Time',
    'total' => 'Total Order',
    'subs' => 'Substitutions Preference',
    'comments' => 'Comments',
    'addtoorder' => 'Add a Note to your Order',
    'message' => 'Message',
    'send' => 'Send',

];
