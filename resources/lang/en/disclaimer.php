<?php

return [

    'disclaimer' => '<h5>Disclaimer</h5>
        <p class="disclaimer" style="text-align: justify;"><small>Prices subject to change without notice. Items subject to availability.
            We reserve the right to limit quantities. Not liable for typographical or pictorial errors. While we strive
            to obtain accurate product information, we cannot guarantee or ensure the accuracy,completeness or timeliness
            of any product information. We recommend that you do not solely rely on the information presented on this
            website and that you always read labels, warnings, and directions and other information provided with the product
            before using or consuming a product. For additional information about a product, please contact the manufacturer.
            Buy For Less assumes no liability for inaccuracies or misstatements about products. Information and statements
            regarding dietary supplements have not been evaluated by the Food and Drug Administration and are not intended
            to diagnose, treat, cure or prevent any disease or health condition.
            Content on this website is for reference purposes only and is not intended to substitute for advice given by a
            physician, pharmacist or other licensed healthcare professional. You should not use this information as
            self-diagnosis or for treating a health problem or disease. Contact your health-care provider immediately
            if you suspect that you have a medical problem.</small></p>',

];
