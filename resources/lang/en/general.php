<?php

return [

    'lang-tag' => 'Español',
    'lang-code' => 'es',

    'envoy-error' => 'Oops! Something went wrong when coordinating your delivery. Please try again.',

    'select' => 'Select...',

    'please-enter-your-credentials' => 'Please Enter Your Credentials',

];
