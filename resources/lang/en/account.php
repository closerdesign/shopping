<?php

return [

    'name' => 'Name',
    'last-name' => 'Last Name',
    'email' => 'Email',
    'billing-address' => 'Billing Address',
    'city' => 'City',
    'zip-code' => 'Zip Code',
    'phone' => 'Phone',
    'preferred-store' => 'Preferred Store',
    'complete-personal-info' => 'Notice:  Please update your personal information',
    'loyalty-member-id' => 'Loyalty Member ID',

];
