<?php

return [

    'header' => 'Contact Us',
    'name' => 'Name',
    'email' => 'Email',
    'phone' => 'Phone',
    'contact' => 'Contact Preference',
    'store' => 'Store',
    'message' => 'Message',
    'send' => 'Send Message',
    'feedback' => 'Feedback',
    'experience' => 'How was your experience?',
    'excellent' => 'Excellent',
    'good' => 'Good',
    'fair' => 'Fair',
    'poor' => 'Poor',
    'suggestions' => 'Suggestions / Message',

];
