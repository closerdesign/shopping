<?php

return [

    'showing' => 'Showing',
    'to' => 'to',
    'of' => 'of',
    'total' => 'total items.',
    'no-results' => 'No results found.',

];
