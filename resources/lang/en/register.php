<?php

return [

    'register' => 'Register',
    'name'     => 'First Name',
    'last-name' => 'Last Name',
    'email'    => 'Email',
    'password' => 'Password',
    'confirm-password' => 'Confirm Password',
    'already-have-an-account' => 'Already Have An Account?',
    'tax-exempt' => 'Tax Exempt Proof (PDF Files Only. Max 5 Mb.)',

];
