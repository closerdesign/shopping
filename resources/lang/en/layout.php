<?php

return [

    //Cart
    'cart-head' => 'Your Cart',
    'no-pickup' => 'You have not yet selected a date and time for pickup. Please
                    <a class="alert-danger-link" data-toggle="modal" data-target="#pickupModal" onclick="hideMC()">click here to reserve one</a>.',
    'empty-cart' => 'Your cart is empty.',
    'add-custom-msg' => 'Can\'t find something you\'re sure we carry?',
    'add-custom-btn' => 'Add Custom Item',
    'item' => 'item',
    'tax' => 'Estimated Tax',
    'weight' => 'Some of these items may need to be weighed. You\'ll pay for the final weight.',
    'minimum' => '$50 minimum',
    'total' => 'Total',
    'checkout' => 'Checkout',
    'account' => 'My Account',

    //Account
    'save-time' => 'Save time and build your new order based on previous orders!',
    'sign-in' => 'Sign In',
    'create' => 'Don\'t have an account yet?<br />
                 <a style="text-decoration: underline" href="/register">Create Yours Now! It\'s FREE!</a>',
    'prev-orders' => 'Previous Orders',
    'favorites' => 'My Favorites',
    'clear-cart' => 'Clear Cart',
    'change-pass' => 'Change Password',
    'personal-info' => 'Personal Info',
    'name' => 'Name',

    //Top Bar
    'department' => 'Shop By Department',
    'search' => 'Search',
    'product' => 'Search product',

    //Nav
    'logout' => 'Log Out',
    'reserve' => '[Select pickup time/date at checkout]',
    'pickup' => 'Pickup',
    'delivery' => 'Delivery To',

    //Sidebar
    'close-menu' => 'Close Menu',

    //Footer
    'howitworks' => 'How It Works',
    'terms' => 'Terms of Use',
    'contact' => 'Contact Us',
    'copyright' => '&copy; All rights reserved.',

];
