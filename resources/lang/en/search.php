<?php

return [

    'header' => 'Product Search',
    'message' => 'Search products by name, brand or UPC',
    'search' => 'Search',
    'results' => 'results found for',
    'showing' => 'Showing',
    'to' => 'to',
    'page' => 'Page',
    'no-results' => 'No results found.',
    'custom' => 'Add Custom Item',

];
