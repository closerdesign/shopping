<?php

return [

    'location' => 'Which location would you prefer?',
    'delivery' => 'If you are ordering for delivery to J Marshall Square, Park Harvey Apartments, The Montgomery or The Classen please select \'The Village\'.',
    'continue' => 'Continue',
    'howitworks' => 'How does it work?',

];
