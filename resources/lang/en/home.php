<?php

return [

    'skip'          => 'Skip the Line. Order Online.',
    'order'         => 'Order your groceries for pick-up and we will have them waiting for you when you arrive!',
    'updates-title' => 'We would appreciate your feedback',
    'deadline'      => '',
    'feedback'      =>  '<p>We are so excited to continue to bring you new features, and to continue to evolve our Online Shopping 
                        experience to better fit your needs. We greatly appreciate your feedback, if you have any other improvement 
                        suggestions, please let us know by using the 
                        <a href="/contact">Contact form</a>.</p>',
    'christmas'     => '<b>PICKUP UNAVAILABLE DEC. 23rd - 25th</b><br />
                        Grocery pickup will not be available on Saturday, Dec. 23rd, Sunday, Dec. 24th and Monday, Dec. 25th.
                        The regular pickup schedule will resume on Tuesday, Dec. 26th. We are sorry for any inconvenience.
                        Have a Merry Christmas!',

];
