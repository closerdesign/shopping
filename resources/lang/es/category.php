<?php

return [

    'showing' => 'Mostrando',
    'to' => 'a',
    'of' => 'de',
    'total' => 'productos en total.',
    'no-results' => 'No se encontraron registros.',

];
