<?php

return [

    //Cart
    'cart-head' => 'Tu Pedido',
    'no-pickup' => 'No has elegido una fecha y hora para recoger tu pedido. Por favor 
                    <a class="alert-danger-link" data-toggle="modal" data-target="#pickupModal" onclick="hideMC()">Haz Click Aquí Para Reservar</a>.',
    'empty-cart' => 'Tu pedido esta vacío.',
    'add-custom-msg' => 'No puedes encontrar algo? Utiliza nuestra entrada personalizada de productos',
    'add-custom-btn' => 'Agregar Producto',
    'item' => 'producto',
    'tax' => 'Impuestos Estimados',
    'weight' => 'Algunos de estos productos podrían necesitar ser pesados. Te cobraremos únicamente por su peso final.',
    'minimum' => 'Mínimo $50',
    'total' => 'Total',
    'checkout' => 'Finalizar Pedido',
    'account' => 'Mi Cuenta',

    //Account
    'save-time' => 'Ahorra tiempo creando tu orden con los mismos productos de una orden anterior!',
    'sign-in' => 'Acceso de Usuarios',
    'create' => 'Aún no tienes una cuenta?<br />
                 <a style="text-decoration: underline" href="/register">Créala ahora mismo! Es GRATÍS!</a>',
    'prev-orders' => 'Historial de Órdenes',
    'favorites' => 'Favoritos',
    'clear-cart' => 'Clear Cart',
    'change-pass' => 'Cambiar Password',
    'personal-info' => 'Personal Info',
    'name' => 'Nombre',

    //Top Bar
    'department' => 'Buscar Por Departamento',
    'search' => 'Buscar',
    'product' => 'Buscar Producto',

    //Nav
    'logout' => 'Cerrar Sesión',
    'reserve' => '[Select pickup time/date at checkout]',
    'pickup' => 'Recoger en',
    'delivery' => 'Enviar a:',

    //Sidebar
    'close-menu' => 'Cerrar Menú',

    //Footer
    'howitworks' => '¿Cómo Funciona?',
    'terms' => 'Términos y Condiciones',
    'contact' => 'Contáctenos',
    'copyright' => '&copy; Todos los derechos reservados.',

];
