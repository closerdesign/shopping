<?php

return [

    'header' => 'Búsqueda de Productos',
    'message' => 'Busca tus productos por Nombre, Marca ó UPC',
    'search' => 'Buscar',
    'results' => 'resultados obtenidos para',
    'showing' => 'Mostrando',
    'to' => 'a',
    'page' => 'Página',
    'no-results' => 'No se encontró ningún resultado.',
    'custom' => 'Agregar Producto Personalizado',

];
