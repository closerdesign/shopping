<?php

return [

    'register' => 'Regístrate',
    'name'     => 'Nombre',
    'last-name' => 'Apellido',
    'email'    => 'Correo Electrónico',
    'password' => 'Contraseña',
    'confirm-password' => 'Confirmar Contraseña',
    'already-have-an-account' => '¿Ya tienes una cuenta?',
    'tax-exempt' => 'Prueba de Exención de Impuestos (Archivo PDF. Máximo 5 Mb.)',

];
