<?php

return [

    'sale-alert' => 'Debido a que este producto se encuentra en especial, dependiendo de la fecha y hora de recogida, el precio de venta podría ser ajustado.',
    'alcohol-alert' => '<p><b>GOVERNMENT WARNING</b>: (1) According to the Surgeon General, women should not drink
                        alcoholic beverages during pregnancy because of the risk of birth defects. (2) Consumption
                        of alcoholic beverages impairs your ability to drive a car or operate machinery, and may
                        cause health problems.</p>
                        <p>The sale of alcohol to minors is prohibited.</p>
                        <p>At delivery or pickup, you must show a valid photo ID and provide a signature confirming
                        that you are age 21 or over. Accepted forms of ID are: Driver\'s License, State-issued
                        Identification Cards, U.S. Passports, Military Identification Cards, U.S. Immigration
                        Cards, or Tribal IDs in specific states, or other similar government issued IDs that are
                        recognized within the state. Drivers will not deliver to anyone who appears to be intoxicated.
                        No discounts, coupons or tax-exempt sales may be applied to alcohol. An order of alcohol
                        totaling 20 gallons or more will not be allowed.</p>',
    'meat-alert' => '<p>Uptown Grocery Co. will season + grill your meat for free with a $100 minimum! Just follow these simple steps:</p>
                     <p><ol><li><b>Select</b> your items and <b>add</b> them to your virtual cart- don’t forget your side items!</li>
                     <li>Let us know the seasoning of your choice in the comments of each item. <b>Choose from the following dry seasonings:</b> lemon pepper, cajun, barbeque, chipotle, garlic and herb, tuscan herb, montana steak seasoning, chicago steak seasoning, and salt + pepper.</li>
                     <li>Be sure to <b>schedule your pick up 8 hours in advance</b> for a guaranteed pick up time! Your pick time is subject to be changed based the current grill volume.</li></ol></p>',

];
