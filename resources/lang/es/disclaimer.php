<?php

return [

    'disclaimer' => '<h5>Disclaimer</h5>
        <p class="disclaimer" style="text-align: justify;"><small>Prices subject to change without notice. Items subject to availability.
            We reserve the right to limit quantities. Not liable for typographical or pictorial errors. While we strive
            to obtain accurate product information, we cannot guarantee or ensure the accuracy,completeness or timeliness
            of any product information. We recommend that you do not solely rely on the information presented on this
            website and that you always read labels, warnings, and directions and other information provided with the product
            before using or consuming a product. For additional information about a product, please contact the manufacturer.
            Buy For Less assumes no liability for inaccuracies or misstatements about products. Information and statements
            regarding dietary supplements have not been evaluated by the Food and Drug Administration and are not intended
            to diagnose, treat, cure or prevent any disease or health condition.
            Content on this website is for reference purposes only and is not intended to substitute for advice given by a
            physician, pharmacist or other licensed healthcare professional. You should not use this information as
            self-diagnosis or for treating a health problem or disease. Contact your health-care provider immediately
            if you suspect that you have a medical problem.</small></p>',

    'disclaimer' => '<h5>Términos y Condiciones</h5>
        <p class="disclaimer" style="text-align: justify;"><small>
            Los precios están sujetos a cambios sin previo aviso. Los productos están sujetos a disponibilidad.
            Nos reservamos el derecho de limitar las cantidades. No nos hacemos responsables por errores tipográficos o en las imágenes. Hacemos nuestro mejor intento
            para obtener información de producto precisa, pero no podemos garantizar el nivel de precisión de la misma, el nivel de complexión o el tiempo
            de cada información obtenida. Recomendamos que no se confie únicamente en la información presentada en este sitio y que siempre revise las etiquetas, alertas,
            instrucciones y/o cualquier otra información suministrada antes de consumir el producto. Para información detallada acerca del producto, por favor contacte al fabricante.
            Nuestras marcas no asumen responsabilidad por imprecisiones o falsedad acerca de los productos. La información y/o cualquier aseveración
            acerca de suplementos dietarios que no han sido evaluafos por el <i>Food And Drugs Administration</i> y no están orientados
            a diagnosticar, tratar, curar o prevenir enfermedades y/o condiciones médicas.
            El contenido de este sitio tiene el propósito de referenciar y no sustituye de ninguna manera cualquier clase de acompañamiento
            brindado por un médico, farmaceuta y/o cualquier persona con licenciamiento médico profesional. Usted no debe usar esta información como
            auto diagnóstico o para el tratamiento de alguna enfermedad o padecimiento. Contacte su especialista en atención médida de manera inmediata
            si sospecha que tiene alguna condición médica.
        </small></p>',

];
