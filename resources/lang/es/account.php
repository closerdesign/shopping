<?php

return [

    'name' => 'Nombre',
    'last-name' => 'Apellido',
    'email' => 'Correo Electrónico',
    'billing-address' => 'Dirección de facturación',
    'city' => 'Ciudad',
    'zip-code' => 'Código Postal',
    'phone' => 'Teléfono',
    'preferred-store' => 'Tienda Preferida',
    'complete-personal-info' => 'Para poder seguir usando nuestro servicio debes actualizar tu información personal.',
    'loyalty-member-id' => 'Codigo de la Cliente en la Aplicación Móvil',

];
