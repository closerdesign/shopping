<?php

return [

    'header' => 'Contacto',
    'name' => 'Nombre',
    'email' => 'Email',
    'phone' => 'Teléfono',
    'contact' => 'Medio de Contacto Preferido',
    'store' => 'Tienda',
    'message' => 'Mensaje',
    'send' => 'Enviar Mensaje',
    'feedback' => 'Feedback',
    'experience' => '¿Cómo fue tu experiencia?',
    'excellent' => 'Excelente',
    'good' => 'Buena',
    'fair' => 'Regular',
    'poor' => 'Mala',
    'suggestions' => 'Sugerencia / Mensaje',

];
