<?php

return [

    'skip'          => 'Evita la líneas, ordenando en línea.',
    'order'         => 'Realiza tus compras en línea y recógelas. Estaremos esperando por ti cuando vengas por ellas.',
    'updates-title' => 'Apreciamos tu opinión',
    'deadline'      => '',
    'feedback'      => '<p>Estamos muy emocionados de poder seguir brindando nuevas funcionalidades y seguir evolucionando nuestra experiencia de compra en línea 
                        para ofrecerle un servicio superior. Agradecemos nos comparta sus opiniones. Si considera que hay alguna mejora que le gustaría que integraramos en nuestra plataforma 
                        por favor hagánoslo saber a través de nuestro 
                        <a href="/contact">Formulario de Contacto</a>.</p>',
    'christmas'     => '<b>PICKUP UNAVAILABLE DEC. 23rd - 25th</b><br />
                        Grocery pickup will not be available on Saturday, Dec. 23rd, Sunday, Dec. 24th and Monday, Dec. 25th.
                        The regular pickup schedule will resume on Tuesday, Dec. 26th. We are sorry for any inconvenience.
                        Have a Merry Christmas!',

];
