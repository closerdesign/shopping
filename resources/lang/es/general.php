<?php

return [

    'lang-tag' => 'English',
    'lang-code' => 'en',

    'envoy-error' => 'Lo sentimos, se presentó un error con el sistema de entregas. Por favor intenta de nuevo.',

    'select' => 'Seleccione...',

    'please-enter-your-credentials' => 'Por Favor Ingresa Tus Credenciales',
];
