<?php

return [

    'order' => 'Número de Orden: ',
    'name' => 'Nombre',
    'date' => 'Fecha Recogida',
    'time' => 'Hora Recogida',
    'total' => 'Total de la Orden',
    'subs' => 'Preferencia de Sustituciones',
    'comments' => 'Comentarios',
    'addtoorder' => 'Agrega Una Nota A Tu Orden',
    'message' => 'Mensaje',
    'send' => 'Enviar',

];
