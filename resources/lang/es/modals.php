<?php

return [

    'change-store' => 'Cambiar Tienda',
    'session' => 'Cambiar de tienda requerirá cerrar tu sesión y limpiar tu pedido. Quieres continuar?',
    'cancel' => 'Cancelar',
    'confirm' => 'Confirmar',
    'select-pickup' => 'Seleccione una fecha / hora de recogida',
    'apt' => 'Órdenes para entrega en conjuntos residenciales ÚNICAMENTE',
    'apt-name' => 'Nombre del Complejo',
    'date' => 'Fecha de Recogida',
    'time' => 'Hora de Recogida',
    'partner-msg' => '<p>Please Note: All deliveries are made Thursday, between 1 and 5 PM.</p> <p>Nuestras tiendas hacen entrega a través de acuerdos con algunos complejos residenciales! Quieres que tu complejo sea una de nuestras opciones? 
                      Por favor <a href="/contact">contáctanos</a> y háznoslo saber!</p>',
    'save' => 'Save',
    'howitworks-list' => '<li><b>Selecciona una tienda</b></li>
                          <li><b>Realiza tu pedido</b><br /><small><span class="text-info">Mínimo $50</span> para nuestras órdenes en línea.</small></li>
                          <li><b>Selecciona una fecha y hora de recogida</b><br /><small>Puedes hacerlo al finalizar tu pedido. Solo agendados dos recogidas cada media hora en nuestras tiendas.</small></li>
                          <li><b>Realiza tu pedido y paga</b><br /><small>Puedes pagar con tarjeta de crédito o tu cuenta de PayPal</small></li>
                          <li><b>Preséntate en tu tienda seleccionada a la fecha y hora indicadas</b><br /><small>Cuando llegues, por favor parquea en frente de la puerta para Órdenes en Línea, en donde verás un letrero de \'STOP\'. Llama al número en el letrero y nuestro equipo llevará tu pedido hasta tu vehículo!</small></li>',
    'gotit' => 'Listo!',
    'custom' => 'Agregar Producto Personalizado',
    'item' => 'Nombre del Producto',
    'price' => 'Precio Estimado',
    'guess' => 'Por favor indícanos el precio aproximado del producto. Podrás agregar comentarios adicionales cuando estés finalizando tu pedido.',
    'addtocart' => 'Comprar',

];
