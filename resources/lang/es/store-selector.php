<?php

return [

    'location' => 'En cual tienda quieres hacer tu pedido?',
    'delivery' => 'Si estás ordenando para entrega en J Marshall Square, Park Harcey Apartments, The Montgomery ó The Classen por favor selecciona \'The Village\'.',
    'continue' => 'Continuar',
    'howitworks' => '¿Cómo Funciona?',

];
