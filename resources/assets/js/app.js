
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.axios = require('axios');
window.numeral = require('numeral');
window.vuex = require('vuex');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component('cart-item-component', require('./components/CartItemComponent.vue').default);
Vue.component('credit-card-component', require('./components/CreditCardComponent.vue').default);
Vue.component('product-component', require('./components/ProductComponent.vue').default);
Vue.component('custom-item-component', require('./components/CustomItemComponent.vue').default);

let store = new vuex.Store({
    state: {
        favorites: [],
        cart: [],
        subtotal: [],
    },
    actions: {
        getFavorites({commit, state}){
            fetch('/user/favorites')
                .then(response => response.json())
                .then(data => {
                    commit('setFavorites', data)
                })
        },
        getCart({commit, state}){
            fetch('/shopping-cart/content')
                .then(response => response.json())
                .then(data => {
                    commit('setCart', data)
                })
        },
        getSubtotal({commit,state}){
            fetch('/shopping-cart/subtotal')
                .then(response => response.json())
                .then(data => {
                    commit('setSubtotal',data)
                })
        },
        syncFavorites({commit,state})
        {
            axios.post('/favorites/sync', {
                upcs: state.favorites
            }).then(response=>response.json())
                .then(data => {
                    console.log(data);
                });

        }
    },
    mutations: {
        setFavorites (state, favorites){
            state.favorites = favorites
        },
        setCart (state, cart){
            state.cart = cart
        },
        setSubtotal(state, subtotal)
        {
            state.subtotal = subtotal
        },
        toggleFavorites(state, upc)
        {
            if(state.favorites.includes(upc.upc))
            {
                const index = state.favorites.indexOf(upc.upc);
                state.favorites.splice(index, 1);
            }else{
                state.favorites.push(upc.upc);
            }
            store.dispatch('syncFavorites');
        }
    },
    getters: {
        cart(state){
            return state.cart
        },
        favorites(state){
            return state.favorites
        },
        isFavorite(state) {
            return (upc) => {
                return state.favorites.includes(upc);
            }
        },
        isInCart(state){
            return (upc) => {
                return state.cart.find(i => i.id === upc);
            }
        }
    }
});

import {
    mapGetters
} from 'vuex'

const app = new Vue({

    el: '#app',

    data: function(){
        return {
            categoriesWrapper: false,
            accountWrapper: false,
            mobileLogo: false,
        }
    },

    computed: {
          ...mapGetters([
                'cart'
          ]),

        subtotal(){
              return this.$store.state.subtotal.subtotal
        },

        productCount(){
            return this.$store.state.subtotal.count
        }
    },

    methods: {

        onScroll () {
            const currentScrollPosition = window.pageYOffset || document.documentElement.scrollTop

            if( currentScrollPosition > 170 )
            {
                this.mobileLogo = true;
            }
            this.mobileLogo = false
        },

        showCustomItemModal () {
            this.$refs.customItem.display();
        },

    },

    mounted() {
        this.$store.dispatch('getCart');
        this.$store.dispatch('getSubtotal');
        this.$store.dispatch('getFavorites');
        window.addEventListener('scroll', this.onScroll);
    },

    beforeDestroy() {
        window.removeEventListener('scroll', this.onScroll);
    },

    store: store,

});

$(document).ready(function(){

    $('#loader').fadeOut();

    $('a').click(function(event){
        // event.preventDefault();
        var link = $(this).attr('href');
        var popup = $(this).attr('type');
        var target = $(this).attr('target');

        if ( link !== undefined && link !== null ) {
            if ( link.indexOf('#') !== -1 || ( popup === 'img-popup' ) || ( target === '_blank' ) ){}
            else{
                $('#loader').fadeIn();
            }
        }
    });

    $('#departments').change(function(){
        var department = $(this).val();
        window.location.href = '/department/' + department;
    });

    //  Date Picker

    var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);

    var min = 0;

    if( new Date().getHours() > 15 )
    {
        min = 1;
    }

    var today = new Date();

    var launch = Date.parse('October 2nd 2017');

    if( today < launch )
    {
        min = new Date(2017,9,2);
    }

    var store = $('#store').val();

    $('#pickup_date_modal').change(function(){
        var pickup_date = $(this).val();
        $('#pickup-loading').slideDown();
        $.get('/shopping/pickup-times/' + pickup_date + '/' + store, function(data){
            $("#pickup_time_modal").html(data);
            $('#pickup-loading').slideUp();
        });
    });


    $('#delivery_date_modal').change(function(){
        var delivery_date = $(this).val();
        $.get('/shopping/delivery-times/' + delivery_date + '/' + store, function(data){
            $("#delivery_time_modal").html(data);
        });
    });


    // Tooltips and popovers

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(function (){
        $('[data-toggle="popover"]').popover();
    });

    // Scroll to top affix

    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('#top-link-block').fadeIn();
        } else {
            $('#top-link-block').fadeOut();
        }
    });

    $('#top-link-block').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    // Is Complex check on pickup modal

    $('#is_complex').click(function() {
        if( $(this).is(':checked')) {
            $("#pickup_datetime").fadeOut( function() {
                $("#complex").fadeIn();
            });
        } else {
            $("#complex").fadeOut( function() {
                $("#pickup_datetime").fadeIn();
            });
        }
    });

});

// Mobile Cart Triggers

var mcOut = false;

function hideMC() {
    $('#mobile-cart').animate({
        right: '-250px'
    },300);

    mcOut = false;
}

function showMC() {
    $('#mobile-cart').animate({
        right: 0
    },300);

    mcOut = true;
}
$(document).on('click', "#mobile-cart-trigger", function () {
    if (mcOut){
        hideMC();
    }else{
        showMC();
    }
});

$(document).on('click touchstart', "#mobile-cart-head", function () {
    hideMC();
});

$(document).on('click', "#checkout-link", function () {
    $.get('/total-top', function(data){

        if (data < 30) {
            $('#checkout-link').popover('toggle');
        }
        else {
            window.location.href = '/checkout';
        }

    });

});

// Payment Form Fields

$('.payment-form-field').change(function(){

    var data = $(this).val();
    var field_name = $(this).attr('id');
    var stripe_field = '#Stripe_' + field_name;
    var paypal_field = '#Paypal_' + field_name;

    $(stripe_field).val(data);
    $(paypal_field).val(data);

});

$('#tax_exempt').on('change', function(){

    var data = $(this).val();
    $('#Stripe_tax_exempt').val(data);
    $('#Paypal_tax_exempt').val(data);

});