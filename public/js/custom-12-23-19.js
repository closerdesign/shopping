$(document).ready(function(){

    $('#loader').fadeOut();

    $('a').click(function(event){
        // event.preventDefault();
        var link = $(this).attr('href');
        var popup = $(this).attr('type');
        var target = $(this).attr('target');

        if ( link !== undefined && link !== null ) {
            if ( link.indexOf('#') !== -1 || ( popup === 'img-popup' ) || ( target === '_blank' ) ){}
            else{
                $('#loader').fadeIn();
            }
        }
    });

    $('form').submit(function()
    {
        if($(this).attr('loader') != 'false')
        {
            $('#loader').fadeIn();
        }
    });

    $('#departments').change(function(){
        var department = $(this).val();
        window.location.href = '/department/' + department;
    });

    //  Date Picker

    var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);

    var min = 0;

    if( new Date().getHours() > 15 )
    {
        min = 1;
    }

    var today = new Date();

    var launch = Date.parse('October 2nd 2017');

    if( today < launch )
    {
        min = new Date(2017,9,2);
    }

    var store = $('#store').val();
   // var past_date = yesterday;

    if (store == 1006 || store == 2701){
        var past_date = today;
    }
    else{
        var past_date = yesterday;
    }

    var max = new Date();
    max.setDate(max.getDate() + 14);

    $('.datepicker_delivery').pickadate({
        // min: new Date(2017,9,2),
        min: min,
        max: max,
        container: '#app',
        format: 'yyyy-mm-dd',
        disable: [
            { from: [0,0,0], to: past_date },
            { from:  min, to: max },
            new Date(2019,11,25),
            new Date(2019,11,24)
        ]
    });


    $('.datepicker').pickadate({
        // min: new Date(2017,9,2),
        min: min,
        container: '#app',
        format: 'yyyy-mm-dd',
        disable: [
            { from: [0,0,0], to: past_date },
            new Date(2019,10,27),
            new Date(2019,10,28),
            new Date(2019,11,25),
            new Date(2019,11,24)
        ]
    });

    $('#pickup_date_modal').change(function(){
        var pickup_date = $(this).val();
        $('#pickup-loading').slideDown();
        $.get('/shopping/pickup-times/' + pickup_date + '/' + store, function(data){
            $("#pickup_time_modal").html(data);
            $('#pickup-loading').slideUp();
        });
    });


    $('#delivery_date_modal').change(function(){
        var delivery_date = $(this).val();
        $.get('/shopping/delivery-times/' + delivery_date + '/' + store, function(data){
            $("#delivery_time_modal").html(data);
        });
    });


    // Tooltips and popovers

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(function (){
        $('[data-toggle="popover"]').popover();
    });

    // Scroll to top affix

    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('#top-link-block').fadeIn();
        } else {
            $('#top-link-block').fadeOut();
        }
    });

    $('#top-link-block').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    // Is Complex check on pickup modal

    $('#is_complex').click(function() {
        if( $(this).is(':checked')) {
            $("#pickup_datetime").fadeOut( function() {
                $("#complex").fadeIn();
            });
        } else {
            $("#complex").fadeOut( function() {
                $("#pickup_datetime").fadeIn();
            });
        }
    });

    // Img Popups from product page

    $('.image-popup-vertical-fit').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-img-mobile',
        image: {
            verticalFit: true
        }

    });

    $('.image-popup-fit-width').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        image: {
            verticalFit: false
        }
    });

    $('.image-popup-no-margins').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom',
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300
        }
    });

});

// Shop By Department Menu Triggers

$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#app").toggleClass("toggled");
});
$("#top-menu-toggle").click(function(e) {
    e.preventDefault();
    $("#app").toggleClass("toggled");
});
$("#menu-close").click(function(e) {
    e.preventDefault();
    $("#app").toggleClass("toggled");
});

// Mobile Cart Triggers

var mcOut = false;

function hideMC() {
    $('#mobile-cart').animate({
        right: '-250px'
    },300);

    mcOut = false;
}

function showMC() {
    $('#mobile-cart').animate({
        right: 0
    },300);

    mcOut = true;
}
$(document).on('click', "#mobile-cart-trigger", function () {
    if (mcOut){
        hideMC();
    }else{
        showMC();
    }
});

$(document).on('click touchstart', "#mobile-cart-head", function () {
    hideMC();
});

$(document).on('click', "#checkout-link", function () {
    $.get('/total-top', function(data){

        if (data < 30) {
            $('#checkout-link').popover('toggle');
        }
        else {
            window.location.href = '/checkout';
        }

    });

});

// Payment Form Fields

$('.payment-form-field').change(function(){

    var data = $(this).val();
    var field_name = $(this).attr('id');
    var stripe_field = '#Stripe_' + field_name;
    var paypal_field = '#Paypal_' + field_name;

    $(stripe_field).val(data);
    $(paypal_field).val(data);

});

$('#tax_exempt').on('change', function(){

    var data = $(this).val();
    $('#Stripe_tax_exempt').val(data);
    $('#Paypal_tax_exempt').val(data);

});