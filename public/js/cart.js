function refresh_all() {
    $.get('/cart/refresh-top', function(data){
        $('#nav-container').html(data);
    });
    $.get('/cart/refresh-sidebar', function(data){
        $('#very-top').html(data);
    });
    if ($('#cart-container').length > 0) {
        $.get('/cart/refresh', function(cart){
            $('#cart-container').html(cart);
        });
    }
    if ($('#edit').length > 0) {
        $.get('cart/refresh-checkout', function(data){
            $('#edit').html(data);
        });
        $.get('/total-top', function(total){
            if (total < 30) {
                location.reload();
            } else {
                $('#checkout-total').text(total);
            }
        });
        $.get('/checkout-tax', function(tax){
            $('#checkout-tax').text(tax);
        });
    }
}

// Add Items

$('.add-item').submit(function(event){
    event.preventDefault();
    var form_data = $(this).serialize();
    var id = $(this).attr('id');
    $('#loader').fadeIn();
    $.ajax({
        url : '/cart/add-item',
        type: 'POST',
        data : form_data
    }).done(function(response){
        refresh_all();
        $('#loader').fadeOut();
        // Notify('Product Added!', null, null, 'success');
        if(id.substring(0, 9) === 'add-modal') {
            var modal = '#' + id.substring(4, id.length);
            $(modal).modal('toggle');
        }
    });
});

// Delete Items

function deleteItem(id) {
    // hideMC();
    $('#item' + id).fadeOut();
    $('#loader').fadeIn();
    $.get('/cart/delete/' + id, function(data){
        refresh_all();
        $('#loader').fadeOut();
        Notify('Product Removed!', null, null, 'success');
    });
}

// Comments

$(document).on('focusout', '.comments', function() {
    var item = $(this).attr('item');
    var comment = encodeURIComponent($(this).val());
    comment = comment.trim();
    if(comment !== "")
    {
        $.get('/cart/comments/' + item + '/' + comment, function(data){
            refresh_all();
            hideMC();
            Notify('Comment Saved!', null, null, 'success');
        });
    }
});

// Quantity

$('body').on('click', '.minusOne', function(){
    if ($(this).parent().siblings('input').val() > 1) {
        $(this).parent().siblings('input').val(parseInt($(this).parent().siblings('input').val()) - 1);
        if ($(this).hasClass('ajaxQty')) {
            var qty = $(this).parent().siblings('input').val();
            var id = $(this).parent().siblings('input').attr('rowId');
            updateQty(qty, id);
        }
    }
});

$('body').on('click', '.plusOne', function(){
    $(this).parent().siblings('input').val(parseInt($(this).parent().siblings('input').val()) + 1);
    if ($(this).hasClass('ajaxQty')) {
        var qty = $(this).parent().siblings('input').val();
        var id = $(this).parent().siblings('input').attr('rowId');
        updateQty(qty, id);
    }
});

$('body').on('click', '.minusOne-lb', function(){
    if ($(this).parent().siblings('input').val() > .5) {
        $(this).parent().siblings('input').val(parseFloat($(this).parent().siblings('input').val()) - .5);
        if ($(this).hasClass('ajaxQty')) {
            var qty = $(this).parent().siblings('input').val();
            var id = $(this).parent().siblings('input').attr('rowId');
            updateQty(qty, id);
        }
    }
});

$('body').on('click', '.plusOne-lb', function(){
    $(this).parent().siblings('input').val(parseFloat($(this).parent().siblings('input').val()) + .5);
    if ($(this).hasClass('ajaxQty')) {
        var qty = $(this).parent().siblings('input').val();
        var id = $(this).parent().siblings('input').attr('rowId');
        updateQty(qty, id);
    }
});

function updateQty(qty, id) {
    $('#loader').fadeIn();
    $.get('/cart/update-qty/' + id + '/' + qty, function(data){
        refresh_all();
        $('#loader').fadeOut();
    });
}

$('.qty-val').change( function() {
    var cur = $(this).val();
    if (cur.trim() === '') {
        $(this).val('1');
    }
});

// Favorites

$('.add-fav').click(function(){
    var id = $(this).attr('id');
    id = id.split('-')[1];
    $.get('/favorites/' + id, function(data){
        $('#fav-' + id).html('<i class="fa ' + data + '"></i>');
        $('#modalfav-' + id).html('<i class="fa ' + data + '"></i>');
    });

});

// Substitutions

$(document).on('click', '#disallow-substitutions', function() {
    var e = confirm('Are you sure?');
    if (e === true) {
        $.get('/cart/disallow-substitutions', function(data){
            $('.allow_sub').removeAttr('checked');
            Notify('Substitutions Disallowed', null, null, 'success');
            refresh_all();
        });
    }
});

// So functionality works after ajax refresh

$(document).ajaxComplete(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});