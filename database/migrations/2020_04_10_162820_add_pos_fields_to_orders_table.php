<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPosFieldsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('subtotal', 8, 2)->nullable()->after('final_amount');
            $table->decimal('total', 8, 2)->nullable()->after('subtotal');
            $table->decimal('tax', 8, 2)->nullable()->after('total');
            $table->string('pos_id')->nullable()->after('tax');
            $table->string('pos_received')->nullable()->after('pos_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('subtotal');
            $table->dropColumn('total');
            $table->dropColumn('tax');
            $table->dropColumn('pos_id');
            $table->dropColumn('pos_received');
        });
    }
}
