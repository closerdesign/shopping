<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeliveryFieldsToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('delivery_id')->nullable();
            $table->string('delivery_address')->nullable();
            $table->string('delivery_city')->nullable();
            $table->string('delivery_zip_code')->nullable();
            $table->string('delivery_first_name')->nullable();
            $table->string('delivery_last_name')->nullable();
            $table->string('delivery_phone')->nullable();
            $table->longText('delivery_instructions')->nullable();
            $table->dateTime('delivery_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('delivery_id');
            $table->dropColumn('delivery_address');
            $table->dropColumn('delivery_city');
            $table->dropColumn('delivery_zip_code');
            $table->dropColumn('delivery_first_name');
            $table->dropColumn('delivery_last_name');
            $table->dropColumn('delivery_phone');
            $table->dropColumn('delivery_instructions');
            $table->dropColumn('delivery_time');
        });
    }
}
