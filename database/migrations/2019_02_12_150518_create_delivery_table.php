<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->string('first_name_delivery');
            $table->string('last_name_delivery');
            $table->string('delivery_address');
            $table->string('phone_delivery');
            $table->string('zip_code_delivery');
            $table->string('delivery_instructions');
            $table->string('city_delivery');
            $table->string('state_delivery');
            $table->dateTime('delivery_time');
            $table->integer('time_slot_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
