<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->date('pickup_date');
            $table->time('pickup_time');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->longText('comments');
            $table->string('payment_method');
            $table->decimal('total_order', 10, 2);
            $table->decimal('final_amount', 10, 2)->nullable();
            $table->string('status')->default('PLACED');
            $table->string('paypal_token')->nullable();
            $table->string('paypal_payer_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
