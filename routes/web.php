<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::post('favorites/sync', 'FavoritesController@sync');

Route::get('shopping-cart/content', 'ShoppingCartController@content');
Route::get('shopping-cart/subtotal', 'ShoppingCartController@subtotal');
Route::post('shopping-cart/update', 'ShoppingCartController@update');

/*
 *  Cart Item
 */

Route::group(['prefix' => 'cart-item'], function () {
    Route::get('item-by-id/{id}', 'CartItemController@item_by_id');
});

/**
 *  Order
 */

Route::group(['prefix' => 'order'], function(){
    Route::get('duplicate/{order}', 'AdminController@duplicate')->middleware('admin');
});

/*
 * POS
 */

Route::group(['prefix' => 'pos'], function () {
    Route::get('add-order-attended/{id}/{type}', 'PosController@add_order');
    Route::get('receipt/{order}', 'PosController@receipt');
});

/*
 *  Payment
 */

Route::group(['prefix' => 'payment'], function(){
    Route::post('in-store/{order}', 'PaymentsController@instore_payment')->name('pay-in-store');
});

Route::get('user/favorites', 'UserController@favorites');

Route::group(['prefix' => 'picking'], function () {
    Route::resource('item', 'ItemsController');

    Route::get('add-item/{order}/{upc}', 'PickingController@add_item')->name('add-item');
    Route::get('add-substitute/{order}/{old}/{new}', 'PickingController@add_substitute')->name('add-substitute');
    Route::get('order/{order}', 'PickingController@picking_items')->name('picking-order');
    Route::get('price-check', 'PickingController@price_check')->name('price-check');
    Route::get('substitute/{order}/{upc}', 'PickingController@substitute')->name('substitute');

    Route::post('item/is-out-of-stock/{id}', 'PickingController@is_out_of_stock')->name('is-out-of-stock');
    Route::post('picked/{order}', 'PickingController@picked');
});

Route::post('message/reply', 'NexmoController@reply');

Route::get('', 'ShoppingController@index')->name('home');

Route::get('lang/{lang}', 'LanguageController@language');

Route::get('delivery/update_delivery_status/{id}/{status}', 'DeliveryController@update_delivery_status');

Route::resource('category', 'CategoriesController');
Route::resource('department', 'DepartmentsController');

Route::post('order/cancel-order/{id}', 'OrdersController@cancel_order')->name('cancel-order');
Route::get('order/score/{order}/{score}', 'OrdersController@score_page')->name('order.score-page');
Route::post('order/score-comments/{order}', 'OrdersController@score_comments')->name('order.score-comments');

Route::resource('product', 'ProductsController');

Route::get('cart/payment/{order}/{status}', 'CartController@response');
Route::get('cart/substitutions/{row}/{value}', 'CartController@substitutions');
Route::get('cart/disallow-substitutions', 'CartController@disallow_substitutions');
Route::get('cart/refresh', 'CartController@refresh');
Route::get('cart/refresh-top', 'CartController@refresh_top');
Route::get('cart/refresh-checkout', 'CartController@refresh_checkout');
Route::get('cart/refresh-sidebar', 'CartController@refresh_sidebar');

Route::post('cart-item/update/{id}', 'CartItemController@update');

Route::get('checkout', 'CartController@checkout')->middleware('personal-info')->name('checkout');

Route::get('cart/content', 'CartController@content');
Route::get('/cart/delete/{id}', 'CartController@delete');
Route::get('cart/comments/{item}/{comment}', 'CartController@comments');
Route::post('cart/add-item', 'CartController@add_item');
Route::get('cart/update-qty/{id}/{qty}', 'CartController@update_qty');
Route::post('cart/payment', 'CartController@payment');
Route::post('cart/payment-do/{id}', 'CartController@payment_do');
Route::post('cart/restore', 'CartController@restore');
Route::post('cart/cartstore', 'CartController@cartstore');
Route::post('cart/set_delivery_time', 'CartController@set_delivery_time');
Route::get('cart/delivery_times', 'CartController@delivery_times');
Route::get('cart/delivery_zip/{zip_code}', 'CartController@delivery_zip');
Route::get('cart/delivery_eligible/{zip}', 'CartController@delivery_eligible');
Route::post('cart/delivery_tip', 'CartController@add_delivery_tip');
Route::get('cart/update_delivery_status/{id}/{status}', 'CartController@update_delivery_status');
Route::get('clear-cart', 'CartController@clear');
Route::resource('cart', 'CartController');

Route::get('envoy/send/{id}', 'EnvoyController@send');

Route::get('store-selector', 'StoresController@store_selector');
Route::post('store-select', 'StoresController@store_select');

Route::get('feedback', 'ShoppingController@feedback');
Route::post('feedback-form', 'ShoppingController@feedback_form');
Route::get('contact', 'ShoppingController@contact');
Route::post('contact-form', 'ShoppingController@contact_form');
Route::get('notes/{id}', 'ShoppingController@notes')->name('add-note');
Route::post('notes-form/{id}', 'ShoppingController@notes_form');
Route::get('thankyou/{id}', 'ShoppingController@thankyou');

Route::get('favorites/add/{upc}', 'FavoritesController@add');
Route::get('favorites/{upc}', 'FavoritesController@toggle');

Route::group(['prefix' => 'shopping'], function () {
    Route::get('category/{id}', 'ShoppingController@category');
    Route::get('product/{id}', 'ShoppingController@product');
    Route::get('search', 'ShoppingController@search');
    Route::get('search_aws', 'ShoppingController@search_aws');
    Route::post('autocomplete', 'ShoppingController@autocomplete');
    Route::get('reset-location', 'ShoppingController@reset_location');
    Route::get('pickup-times/{date}/{location}', 'ShoppingController@pickup_times');
    Route::get('delivery-times/{date}/{location}', 'ShoppingController@delivery_times');
    Route::post('pickup-settings', 'ShoppingController@pickup_settings');
    Route::get('pickup-reset', 'ShoppingController@pickup_reset');
    Route::get('delivery-reset', 'ShoppingController@delivery_reset');
});

Route::group(['prefix' => 'admin', 'middleware' => 'approved'], function () {
    Route::get('', 'AdminController@index')->name('admin');
    Route::get('index-data', 'AdminController@index_data');
    Route::get('login-using-id/{id}', 'AdminController@login_using_id');
    Route::get('order/summary/{images}/{id}', 'OrdersController@summary');
    Route::get('order/packaging/{id}', 'OrdersController@packaging');
    Route::get('order/report/{id}/{store}', 'OrdersController@report');
    Route::get('order/out-of-stock/{id}/{store}/{user}', 'OrdersController@out_of_stock');
    Route::get('order/call-notification/{id}', 'OrdersController@call_notification');
    Route::get('order/text-notification/{id}', 'OrdersController@text_notification');
    Route::get('order/over-total/{id}', 'OrdersController@over_total');
    Route::get('order/guest-check/{id}', 'OrdersController@guest_check');
    Route::get('order/excel-full-report', 'AdminController@excel_full_report');
    Route::post('order/signature/{id}', 'OrdersController@save_signature');
    Route::post('order/picked/{id}', 'OrdersController@picked');
    Route::post('order/picker-comment/{id}/{comment}', 'OrdersController@picker_comment');
    Route::resource('order', 'OrdersController');
    Route::get('improvement-request', 'AdminController@improvement_request');
    Route::post('improvement-request-form', 'AdminController@improvement_request_form');
    Route::get('reminder', 'OrdersController@reminder');
    Route::get('top-sellers-report', 'AdminController@top_sellers_report');
    Route::get('top-sellers-report-excel', 'AdminController@top_sellers_report_excel');
    Route::get('thanx', 'AdminController@thanx');
    Route::get('top-customers', 'AdminController@top_online_shoppers');
    Route::post('customer-orders', 'AdminController@customer_orders');

    Route::resource('reports', 'ReportsController');
});

Route::group(['prefix' => 'account'], function () {
    Route::get('my-account', 'AccountsController@my_account');
    Route::get('order-details/{order}', 'AccountsController@order_details');
    Route::get('previous-orders', 'AccountsController@previous_orders');
    Route::get('reorder-items/{order}', 'AccountsController@reorder_items');
    Route::get('favorites', 'AccountsController@favorites');
    Route::get('change-password', 'AccountsController@change_password');
    Route::post('update-pass', 'AccountsController@update_pass');
    Route::get('personal-info', 'AccountsController@personal_info')->name('personal-info');
    Route::post('update-user', 'AccountsController@update_user');
    Route::get('loyalty', 'AccountsController@loyalty_index');
    Route::post('loyalty-login', 'AccountsController@loyalty');
});

Route::get('terms', 'ShoppingController@terms');

Route::get('total-top', function () {
    return \Gloudemans\Shoppingcart\Facades\Cart::total();
});

Route::get('checkout-tax', function () {
    $x = (\Gloudemans\Shoppingcart\Facades\Cart::total() - \Gloudemans\Shoppingcart\Facades\Cart::subtotal());

    return number_format($x, 2);
});

Route::group(['prefix' => 'stripe'], function(){
    Route::post('create-payment-intent', 'StripeController@create_payment_intent')->name('create-payment-intent');
});

Route::group(['middleware' => 'guest'], function(){

    Route::get('/sign-in/facebook', 'Auth\LoginController@facebook');
    Route::get('/sign-in/facebook/redirect', 'Auth\LoginController@facebook_redirect');

});

Route::resource('custom-item', 'CustomItemController');
