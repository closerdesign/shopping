<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::get('favorites/add/{upc}', 'FavoritesController@add');
Route::get('monthly-reports', 'OrdersController@month_report');
Route::get('order-reports', 'ReportsController@all_by_array');

Route::get('orders', function(){
    $orders = \App\Order::where('pickup_date', '>=', date('Y-m-d', strtotime('-90 days')))->get();
    return response($orders, 200);
});